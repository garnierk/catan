package up.catansColons.ui.controllers;

import up.catansColons.ui.uiG.views.objects.RessourcesRelatedGraphics;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Vue et partie de controlleur de l'echange
 */
public interface Exchange extends RessourcesRelatedGraphics {

    Color NEG = new Color(178, 8, 8);
    Color POS = new Color(76, 178, 8);

    default Map<Character, Integer> initInOut() {
        Map<Character, Integer> map = new HashMap<>();
        RESSOURCES_NAME.forEach((c, ignored) -> map.put(c, 0));
        return map;
    }

    Map<Character, Integer> getOut();

    void addCota(int i);

    void repaint();

    Map<Character, Integer> getIn();

    void reset();

    Map<Character, Integer> initRatio();

    int getRatio(char code);

    void removeCota(int i);
}
