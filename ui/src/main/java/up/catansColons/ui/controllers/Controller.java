package up.catansColons.ui.controllers;

import up.catansColons.board.composant.Tile;
import up.catansColons.player.Joueur;
import up.catansColons.ui.models.Model;

/**
 * Represente les controlleur de l'interface utilisateur
 */
public interface Controller {

    /**
     * Affiche la victoire du joueur
     *
     * @param current represente le joueur gagnant
     */
    void victory(Joueur current);

    /**
     * Joue le premier tour
     */
    void playFirstRound();

    /**
     * utilise la carte de construction
     */
    void constructionCard();

    /**
     * lance la fin de tour
     */
    void endRound();

    /**
     * Lance le message de debut de tour
     */
    void roundStartDialog();

    /**
     * Lance le second tour
     */
    void playSecondRound();

    /**
     * Joue un tour lambda
     */
    void playRound();

    /**
     * Fait prendre ua joueur une carte
     */
    void takeCard();

    /**
     * Utilise une carte
     */
    default void useCard() {
        useCard(' ');
    }

    /**
     * Utilise une carte
     */
    void useCard(char c);

    /**
     * Utilisation d'une carte chevalier
     */
    void knightCard();

    /**
     * Utilise une carte decouverte
     */
    void harvestCard();

    /**
     * Actualise la vue
     */
    void repaint();


    Model getModel();

    /**
     * Deplace le voleur
     *
     * @param tile represente le voleur a deplacer
     */
    void moveThief(Tile tile);

    /**
     * Affiche le message de construction impossible
     */
    void impossibleConstructionMessage();

    /**
     * Affiche le debut de tour
     */
    void startRound();

    /**
     * Indique au joueur qu'il doit se separer de la moitie de ses ressources
     *
     * @param p represente le joueur qui doit se separer de ses ressources
     */
    void overLimitResources(Joueur p);

    /**
     * Lance le de
     */
    void launchDice();

    /**
     * Realise l'echange
     */
    void exchange();
}
