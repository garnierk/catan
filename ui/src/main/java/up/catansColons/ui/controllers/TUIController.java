package up.catansColons.ui.controllers;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.game.Config;
import up.catansColons.player.Joueur;
import up.catansColons.ui.models.Model;
import up.catansColons.ui.uiT.Vue;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TUIController implements Controller {

    private final Vue view;
    private final int MAJ_CHAR_POINT = 65;
    private final int DIGIT_CHAR_POINT = 48;
    private final Scanner entry;
    public boolean end;
    public boolean second;
    public boolean first;
    public boolean cardTaken;
    private boolean diceLaunched;
    private boolean thief;
    private boolean isAbleToConstruct;
    private boolean hasCard;
    private boolean takeCard;
    private boolean cardUsed;
    private Model model;
    private int counter;

    public TUIController(Vue view) {
        this.view = view;
        diceLaunched = false;
        entry = new Scanner(System.in);
        counter = 0;
    }

    public static void quitGame() {
        System.out.println("\n \n Un jeu cree par");
        System.out.println("Garnier Kevin");
        System.out.println("Ripasarti Adriano \n");
        System.out.println("Fermeture du jeu, Merci d'avoir joue !");
        System.exit(0);
    }

    public boolean isDiceLaunched() {
        return diceLaunched;
    }

    public boolean isThief() {
        return thief;
    }

    public boolean isAbleToConstruct() {
        return isAbleToConstruct;
    }

    public boolean isHasCard() {
        return hasCard;
    }

    public boolean isTakeCard() {
        return takeCard;
    }

    public boolean isEnd() {
        return end;
    }

    public boolean isSecond() {
        return second;
    }

    public boolean isFirst() {
        return first;
    }

    public boolean isCardTaken() {
        return cardTaken;
    }

    public boolean isCardUsed() {
        return cardUsed;
    }

    public void readPseudo() {
        view.demandePseudo();
        String s = entry.next();
        this.model = new Model();
        this.view.setModel(this.model);
        model.addPlayer(s);
        this.model.setProfil(s);
        readAction1();
    }

    public void readAction1() {
        this.view.demandeAction1();
        Scanner entry = new Scanner(System.in);
        switch (entry.next()) {
            case "load":
                break;
            case "new":
                readAction2();
                break;
            case "profile":
                this.view.seeProfil();
                readAction1();
                break;
            case "quit":
                quitGame();
                break;
            default:
                readAction1();
        }
    }

    public void readAction2() {
        view.demandeAction2();
        switch (entry.next()) {
            case "playAgainst3Computer":
                playAgainstComputer(4);
                break;
            case "playAgainst2Computer":
                playAgainstComputer(3);
                break;
            case "return":
                readAction1();
                break;
            case "reload":
                reloadConfigRequest();
                break;
            case "new":
                readAction3();
                break;
            case "quit":
                quitGame();
                break;
            default:
                readAction2();
        }
    }

    public void reloadConfigRequest() {
        if (this.model.getProfil().getConfigSave().size() != 0) {
            this.view.demandeReload();
            this.view.setModel(model);
            readAction3();
        } else {
            this.view.affiche("Erreur ! Il n'y a pas de configuration existante.");
            readAction2();
        }
    }

    public void playAgainstComputer(int n) {
        this.completeConfig(n);
        this.model.setnbPlayer(n);
        this.model.setConfig(model.getConfig());
        this.view.setModel(model);
        this.view.affiche("\n" + "Debut de la partie !");
        this.view.miseAJour();
    }

    public void readAction3() {
        view.demandeAction3();
        switch (entry.next()) {
            case "see":
                this.view.seeConfig();
                readAction3();
                break;
            case "return":
                readAction2();
                break;
            case "make":
                readConfig();
                readSave();
                readAction3();
                break;
            case "play":
                playGameRequest();
                break;
            case "quit":
                quitGame();
                break;
            default:
                readAction3();
        }
    }

    public void playGameRequest() {
        if (!this.model.getConfig().estComplete()) readIA();
        this.model.setConfig(model.getConfig());
        model.setController(this);
        this.view.setModel(model);
        model.initGame();
        this.view.affiche("\n" + "Debut de la partie !");
        model.playFirstRound();
    }

    public void completeConfig(int nb) {
        int nbPlayer = this.model.getConfig().getPlayerList().size();
        for (int i = nbPlayer; i < nb; i++) {
            this.model.addIA("IA" + i);
        }
    }

    public void readIA() {
        view.demandeIA();
        try {
            int nb = Integer.parseInt(entry.next("[34]"));
            this.completeConfig(nb);
        } catch (InputMismatchException e) {
            entry.nextLine();
            System.out.println("entree incorecte");
            view.demandeIA();
        }
    }

    @Override
    public void playSecondRound() {
        second = true;
        model.getBoard().print();
        roundStartDialog();
        view.askColonyEmplacement();
        endRound();
    }

    @Override
    public void playFirstRound() {
        first = true;
        model.getBoard().print();
        roundStartDialog();
        this.view.askColonyEmplacement();
        endRound();
    }

    public void readSave() {
        view.demandeSave();
        try {
            entry.next("s");
            System.out.print("name :");
            String s = entry.nextLine();
            Config save = this.model.getConfig();
            save.setName(s);
            this.model.addSave(save);
        } catch (InputMismatchException ignored) {
        }

    }

    public void readConfig() {
        int nbHuman;
        nbHuman = readInt();
        for (int i = 0; i < nbHuman - 1; i++) {
            System.out.println("Joueur" + i);
            System.out.print("name :");
            String name = readString();
            model.addPlayer(name);
        }
    }

    public void readReload() {
        view.demandeReload();
        String s = entry.next();
        for (Config c : this.model.getProfil().getConfigSave()) {
            if (c.getNameConfig().equals(s)) {
                this.model.setConfig(c);
            }
        }
    }

    public String readString() {
        return entry.nextLine();
    }

    public int readInt() {
        Scanner sc = new Scanner(System.in);
        try {
            return Integer.parseInt(sc.next("\\d+"));
        } catch (InputMismatchException e) {
            entry.skip("\\.");
            System.out.println("Veuillez entrer un entier");
            return readInt();
        }
    }

    public int[] readTileCoordinate() {
        System.out.println("Veuillez indiquer les coordonnees de la Tuile");
        try {
            String s = entry.next("\\p{Alpha}\\d");
            return new int[]{Character.toUpperCase(s.charAt(0)) - MAJ_CHAR_POINT, s.charAt(1) - DIGIT_CHAR_POINT - 1};
        } catch (InputMismatchException e) {
            entry.nextLine();
            System.out.println(
                    "Les coordonee sont ceux affiches sur la plateau et doivent etre de la forme \n" +
                            "lettreChiffre"
            );
            return readTileCoordinate();
        }
    }

    public int[] readVertexCoordinate() {
        System.out.println("Veuillez indiquer les coordonnees du coin (exemple 00 pour le coin gauche haut");
        try {
            String s = entry.next("[01][01]");
            return new int[]{s.charAt(0) - DIGIT_CHAR_POINT, s.charAt(1) - DIGIT_CHAR_POINT};
        } catch (InputMismatchException e) {
            entry.nextLine();
            System.out.println("Les coordonnes x et y sont comprises entre 0 et 1 inclus");
            return readVertexCoordinate();
        }
    }

    public Edge readEdgeCoordinate(Vertex v) {
        System.out.println("Veuillez indiquer les coordonnees de l'arrete (h,g,d,b");
        try {
            return model.getEdge(v, entry.next("[hgdb]").charAt(0));
        } catch (InputMismatchException e) {
            entry.nextLine();
            System.out.println("Entree invalide");
            return readEdgeCoordinate(v);
        }
    }

    @Override
    public void victory(Joueur current) {
        System.out.println(current.getPseudo() + " a gagne la partie \n \n");
        System.out.println("Un jeu cree par");
        System.out.println("Garnier Kevin");
        System.out.println("Ripasarti Adriano \n");
        System.out.println("Fermeture du jeu, Merci d'avoir joue !");
        System.exit(0);
    }

    @Override
    public void endRound() {
        model.changeTurn();
    }

    @Override
    public void roundStartDialog() {
        this.view.affiche("Tour de " + this.model.getCurrent().getPseudo());
    }

    @Override
    public void impossibleConstructionMessage() {
        view.affiche("Vous ne pouvez pas construire ici");
    }

    public void missingRessourcesMessage() {
        view.affiche("Vous n'avez pas les ressources requises pour cette action");
    }

    @Override
    public void overLimitResources(Joueur p) {
        p.affiche();
        view.askOverLimit(p.getRessourcesNumber() / 2);
        new TUIOverLimit(p, entry);
    }

    public void firstBuild(Tile t, Vertex v, Edge e) {
        Joueur current = model.getCurrent();
        if (!current.placeFirstColony(t, v)) {
            impossibleConstructionMessage();
            view.askColonyEmplacement();
            view.miseAJour();
        } else {
            if (!current.placeFirstRoad(t, v, e)) {
                impossibleConstructionMessage();
                view.miseAJour();
                view.askColonyEmplacement();
            }
        }
        if (second) model.getCurrent().harvest(v.getNextTiles());
    }

    @Override
    public void playRound() {
        diceLaunched = false;
        end = false;
        thief = false;
        takeCard = (getModel().getCurrent().canTakeCard() && !model.isCardStackEmpty());
        isAbleToConstruct = (getModel().getCurrent().isAbleToConstruct().size() > 0);
        hasCard = getModel().getCurrent().hasDevCard();
        cardTaken = false;
        cardUsed = false;
        startRound();
        endRound();
    }

    @Override
    public void startRound() {
        readRoundAction();
    }

    @Override
    public void repaint() {
    }

    public void readRoundAction() {
        roundStartDialog();
        view.printRoundOption();
        Scanner entry = new Scanner(System.in);
        switch (entry.next()) {
            case "dice":
                launchDice();
                break;
            case "exchange":
                exchange();
                break;
            case "take":
                takeCard();
                break;
            case "use":
                useCard();
                break;
            case "build":
                askBuild();
                break;
            case "thief":
                thief();
                break;
            case "end":
                endRound();
                break;
            case "quit":
                quitGame();
                break;
            default:
                view.printRoundOption();
        }
    }

    private void askBuild() {
        view.askBuildType();
        Scanner entry = new Scanner(System.in);
        switch (entry.next()) {
            case "colony":
                view.askColonyEmplacement();
                break;
            case "road":
                view.askRoadEmplacement();
                break;
            case "city":
                view.askCityEmplacement();
                break;
            case "return":
                view.printRoundOption();
                break;
            case "quit":
                quitGame();
            default:
                askBuild();
        }
    }

    @Override
    public void takeCard() {
        if (!takeCard) {
            missingRessourcesMessage();
            view.printRoundOption();
            return;
        }
        model.getCurrent().prendreDeveloppement();
        cardTaken = true;
        takeCard = false;
        hasCard = !cardUsed;
        readRoundAction();
    }

    @Override
    public void launchDice() {
        if (diceLaunched) {
            readRoundAction();
            return;
        }
        Joueur current = model.getCurrent();
        diceLaunched = true;
        int d = model.launchDice();
        thief = (d == 7);
        end = (d != 7);
        takeCard = (current.canTakeCard() && !model.isCardStackEmpty() && !cardTaken);
        isAbleToConstruct = (current.isAbleToConstruct().size() > 0);
        readRoundAction();
    }

    @Override
    public void exchange() {
        Joueur current = getModel().getCurrent();
        new TUIExchange(current, entry).exchange();
        isAbleToConstruct = (current.isAbleToConstruct().size() > 0);
        takeCard = (current.canTakeCard() && !model.isCardStackEmpty()) && !cardTaken;
        readRoundAction();
    }

    @Override
    public void useCard(char c) {
        view.showCards();
        switch (entry.next()) {
            case "knight":
                knightCard();
                break;
            case "build":
                constructionCard();
                break;
            case "discovery":
                harvestCard();
                break;
            case "quit":
                TUIController.quitGame();
                break;
            case "return":
                return;
            default:
                useCard(c);
        }
    }

    @Override
    public void knightCard() {
        view.miseAJour();
        view.thief();
        moveThief(model.getBoard().getTile(readTileCoordinate()));
        model.getCurrent().useCard('C');
        model.setHighestKnight();
        cardUsed = true;
    }

    @Override
    public void constructionCard() {
        if (counter >= 2) {
            counter = 0;
            model.getCurrent().useCard('R');
            cardUsed = true;
            return;
        }
        view.constructionCard();
        view.miseAJour();
        Tile t = model.getBoard().getTile(readTileCoordinate());
        Vertex v = t.getVertex(readVertexCoordinate());
        Edge e = readEdgeCoordinate(v);
        if (!model.getCurrent().useConstructionCard(e)) {
            impossibleConstructionMessage();
        }
        if (stopUsingCard('R')) {
            counter = 0;
            model.getCurrent().useCard('R');
            cardUsed = true;
            return;
        }
        counter++;
        constructionCard();
    }

    public void harvestCard() {
        if (counter >= 2) {
            model.getCurrent().useCard('D');
            counter = 0;
            cardUsed = true;
            return;
        }
        view.harvestCard();
        view.miseAJour();
        Tile t = model.getBoard().getTile(readTileCoordinate());
        model.getCurrent().harvest(t);
        counter++;
        if (stopUsingCard('D')) {
            model.getCurrent().useCard('D');
            counter = 0;
            cardUsed = true;
            return;
        }
        counter++;
        constructionCard();
    }

    private boolean stopUsingCard(char c) {
        view.affiche("Voule vous arreter d'utiliser la carte [O/N]");
        try {
            switch (entry.next("[ONon]").charAt(0)) {
                case 'O':
                case 'o':
                    model.getCurrent().useCard(c);
                    return true;
                case 'N':
                case 'n':
                    return false;
                default:
                    return stopUsingCard(c);
            }
        } catch (InputMismatchException e) {
            return stopUsingCard(c);
        }
    }

    public void build(char c, Tile t, Vertex v, Edge e) {
        boolean isBuild = model.getCurrent().construirePiece(c, t, v, e);
        if (!isBuild) {
            impossibleConstructionMessage();
            readRoundAction();
        }
        model.setLongestRoad();
        isAbleToConstruct = (model.getCurrent().isAbleToConstruct().size() > 0);
        readRoundAction();
    }

    private void thief() {
        view.miseAJour();
        view.thief();
        moveThief(model.getBoard().getTile(readTileCoordinate()));
        end = true;
        readRoundAction();
    }

    public void moveThief(Tile t) {
        if (t == model.getBoard().getThief().getTile()) {
            view.affiche("Vous ne pouvez pas placer le voleur sur la même case");
            thief();
        } else {
            model.getThief().move(t);
            thief = false;
        }
    }

    public void tryBuild(char c, Tile t, Vertex v, Edge e) {
        if (first || second) firstBuild(t, v, e);
        else build(c, t, v, e);
    }

    @Override
    public Model getModel() {
        return model;
    }
}
