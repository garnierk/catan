package up.catansColons.ui.controllers;

import up.catansColons.player.Joueur;

import javax.swing.*;

public class GUIExchangeController implements ExchangeController {
    private final Joueur player;
    private final Exchange exchangePanel;
    private final JFrame window;

    public GUIExchangeController(Joueur player, Exchange exchangePanel, JFrame window) {
        this.player = player;
        this.exchangePanel = exchangePanel;
        this.window = window;
    }

    @Override
    public void addPlayerRessources(char code) {
        if (player.getRessourceNumber(code) - exchangePanel.getOut().get(code) <= 0) return;
        exchangePanel.getOut().computeIfPresent(code, (c, i) -> i + 1);
        exchangePanel.addCota(1);
        exchangePanel.repaint();
    }

    @Override
    public void accept() {
        exchangePanel.getIn().forEach(player::prendreRessource);
        exchangePanel.getOut().forEach(player::enleveRessource);
        exchangePanel.reset();
        exchangePanel.repaint();
        window.repaint();

    }

    @Override
    public void removeExchange(char code) {
        if (exchangePanel.getIn().get(code) <= 0) return;
        exchangePanel.getIn().computeIfPresent(code, (c, i) -> i - 1);
        exchangePanel.addCota(exchangePanel.getRatio(code));
        exchangePanel.repaint();
    }

    @Override
    public void removePlayerRessource(char code) {
        if (exchangePanel.getOut().get(code) <= 0) return;
        exchangePanel.getOut().computeIfPresent(code, (c, i) -> i - 1);
        exchangePanel.removeCota(1);
        exchangePanel.repaint();
    }

    @Override
    public void addExchange(char code) {
        exchangePanel.getIn().computeIfPresent(code, (c, i) -> i + 1);
        exchangePanel.removeCota(exchangePanel.getRatio(code));
        exchangePanel.repaint();
    }
}
