package up.catansColons.ui.controllers;

import up.catansColons.player.Joueur;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class TUIOverLimit implements Exchange, ExchangeController {
    private final Scanner entry;
    private final Joueur player;
    private final Map<Character, Integer> ratio;
    private Map<Character, Integer> out;
    private Map<Character, Integer> in;
    private int cota;

    public TUIOverLimit(Joueur player, Scanner entry) {
        this.entry = entry;
        out = initInOut();
        in = initInOut();
        ratio = initRatio();
        cota = player.getRessourcesNumber() / 2;
        this.player = player;
    }

    @Override
    public Map<Character, Integer> getOut() {
        return out;
    }

    @Override
    public void addCota(int i) {
        cota -= i;
    }

    @Override
    public void repaint() {
        printCentralMessage();
        RESSOURCES_NAME.forEach((c, s) -> System.out.println(s + " (" + (player.getRessourceNumber(c) + in.get(c) - out.get(c) + ")")));
    }

    private void printCentralMessage() {
        if (cota > 0) {
            System.out.println("Veuillez ajouter " + Math.abs(cota) + " ressource(s)");
        } else if (cota < 0) {
            System.out.println("Veuillez retirer " + Math.abs(cota) + " ressources(s)");
        } else {
            System.out.println("Vous avez donné suffisament de ressource");
        }
    }

    @Override
    public Map<Character, Integer> getIn() {
        return in;
    }

    @Override
    public void reset() {
        cota = player.getRessourcesNumber() / 2;
        out = initInOut();
        in = initInOut();
    }

    @Override
    public Map<Character, Integer> initRatio() {
        Map<Character, Integer> map = new HashMap<>();
        out.forEach((code, ignored) -> map.put(code, 1));
        return map;
    }

    @Override
    public int getRatio(char code) {
        return ratio.get(code);
    }

    @Override
    public void removeCota(int i) {
        cota += i;
    }

    @Override
    public void addPlayerRessources(char code) {
        if (player.getRessourceNumber(code) - out.get(code) <= 0) return;
        out.computeIfPresent(code, (c, i) -> i + 1);
        addCota(1);
        repaint();
    }

    @Override
    public void accept() {
        in.forEach(player::prendreRessource);
        out.forEach(player::enleveRessource);
        reset();
        repaint();
    }

    @Override
    public void removeExchange(char code) {
        if (in.get(code) <= 0) return;
        in.computeIfPresent(code, (c, i) -> i - 1);
        addCota(getRatio(code));
        repaint();
    }

    @Override
    public void removePlayerRessource(char code) {

    }

    @Override
    public void addExchange(char code) {
    }

    public void overLimit() {
        boolean done = false;
        while (!done) {
            System.out.println("Entrez d'abord le detenteur de ressource, puis le sens et enfin la ressource");
            switch (entry.next()) {
                case "player":
                    chooseRessourcesDir();
                    break;
                case "accept":
                    if (cota == 0) {
                        accept();
                        done = true;
                    }
            }
        }
    }

    private void chooseRessourcesDir() {
        String s = entry.next().toLowerCase(Locale.ROOT);
        if (s.equals("add")) {
            addRessource();
        } else if (s.equals("remove")) {
            removeRessource();
        }
    }

    private void removeRessource() {
        switch (entry.next().toLowerCase(Locale.ROOT)) {
            case "pierre":
                removePlayerRessource('p');
                break;
            case "laine":
                removePlayerRessource('l');
                break;
            case "bois":
                removePlayerRessource('w');
                break;
            case "argile":
                removePlayerRessource('a');
                break;
            case "ble":
                removePlayerRessource('b');
        }
    }

    private void addRessource() {
        switch (entry.next().toLowerCase(Locale.ROOT)) {
            case "pierre":
                addPlayerRessources('p');
                break;
            case "laine":
                addPlayerRessources('l');
                break;
            case "bois":
                addPlayerRessources('w');
                break;
            case "argile":
                addPlayerRessources('a');
                break;
            case "ble":
                addPlayerRessources('b');
        }
    }

}
