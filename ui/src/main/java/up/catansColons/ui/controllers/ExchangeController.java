package up.catansColons.ui.controllers;

/**
 * Controlleur de l'echange
 */
public interface ExchangeController {
    void addPlayerRessources(char code);

    void accept();

    void removeExchange(char code);

    void removePlayerRessource(char code);

    void addExchange(char code);
}
