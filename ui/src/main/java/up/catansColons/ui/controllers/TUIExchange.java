package up.catansColons.ui.controllers;

import up.catansColons.board.composant.Port;
import up.catansColons.cards.CarteRessource;
import up.catansColons.player.Joueur;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class TUIExchange implements Exchange, ExchangeController {
    private final Scanner entry;
    private final Joueur player;
    private final Map<Character, Integer> ratio;
    private Map<Character, Integer> out;
    private Map<Character, Integer> in;
    private int cota;

    public TUIExchange(Joueur current, Scanner entry) {
        player = current;
        this.entry = entry;
        out = initInOut();
        in = initInOut();
        ratio = initRatio();
        cota = 0;
    }

    @Override
    public Map<Character, Integer> getOut() {
        return out;
    }

    @Override
    public void addCota(int i) {
        cota += i;
    }

    @Override
    public void repaint() {
        ratio.forEach((r, i) -> System.out.println(RESSOURCES_NAME.get(r) + " (" + i + ":1)"));
        printCentralMessage();
        RESSOURCES_NAME.forEach((c, s) -> System.out.println(s + " (" + (player.getRessourceNumber(c) + in.get(c) - out.get(c) + ")")));
    }

    private void printCentralMessage() {
        if (cota < 0) {
            System.out.println("Veuillez ajouter " + Math.abs(cota) + " ressource(s)");
        } else if (cota > 0) {
            System.out.println("Vous donnez gracieusement " + cota + " ressources(s)");
        } else {
            System.out.println("Echange equitable");
        }
    }

    @Override
    public Map<Character, Integer> getIn() {
        return in;
    }

    @Override
    public void reset() {
        in = initInOut();
        out = initInOut();
        cota = 0;
    }

    @Override
    public int getRatio(char code) {
        return 0;
    }

    @Override
    public void removeCota(int i) {
        cota -= i;
    }

    public void exchange() {
        boolean done = false;
        while (!done) {
            player.affiche();
            System.out.println("Entrez d'abord le detenteur de ressource, puis le sens et enfin la ressource");
            switch (entry.next()) {
                case "bank":
                    chooseRessourcesDir(false);
                    break;
                case "player":
                    chooseRessourcesDir(true);
                    break;
                case "accept":
                    if (cota > 0) {
                        accept();
                        done = true;
                        break;
                    }
                case "return":
                    done = true;
                    break;
                case "quit":
                    TUIController.quitGame();
            }
        }
    }

    private void chooseRessourcesDir(boolean isPlayer) {
        switch (entry.next().toLowerCase(Locale.ROOT)) {
            case "add":
                addRessource(isPlayer);
                break;
            case "remove":
                removeRessource(isPlayer);
                break;
            case "return":
                return;
            case "quit":
                TUIController.quitGame();
                break;
            default:
                chooseRessourcesDir(isPlayer);
        }
    }

    private void removeRessource(boolean isPlayer) {
        switch (entry.next().toLowerCase(Locale.ROOT)) {
            case "pierre":
                if (isPlayer) removePlayerRessource('p');
                else removeExchange('p');
                break;
            case "laine":
                if (isPlayer) removePlayerRessource('l');
                else removeExchange('l');
                break;
            case "bois":
                if (isPlayer) removePlayerRessource('w');
                else removeExchange('w');
                break;
            case "argile":
                if (isPlayer) removePlayerRessource('a');
                else removeExchange('a');
                break;
            case "ble":
                if (isPlayer) removePlayerRessource('b');
                else removeExchange('b');
            case "return":
                return;
            case "quit":
                TUIController.quitGame();
                break;
            default:
                removeRessource(isPlayer);
        }
    }

    private void addRessource(boolean isPlayer) {
        switch (entry.next().toLowerCase(Locale.ROOT)) {
            case "pierre":
                if (isPlayer) addPlayerRessources('p');
                else addExchange('p');
                break;
            case "laine":
                if (isPlayer) addPlayerRessources('l');
                else addExchange('l');
                break;
            case "bois":
                if (isPlayer) addPlayerRessources('w');
                else addExchange('w');
                break;
            case "argile":
                if (isPlayer) addPlayerRessources('a');
                else addExchange('a');
                break;
            case "ble":
                if (isPlayer) addPlayerRessources('b');
                else addExchange('b');
            case "return":
                return;
            case "quit":
                TUIController.quitGame();
                break;
            default:
                removeRessource(isPlayer);
        }
    }


    @Override
    public void addPlayerRessources(char code) {
        if (player.getRessourceNumber(code) - out.get(code) <= 0) return;
        out.computeIfPresent(code, (c, i) -> i + 1);
        addCota(1);
        repaint();
    }

    @Override
    public void accept() {
        in.forEach(player::prendreRessource);
        out.forEach(player::enleveRessource);
        reset();
        repaint();
    }

    @Override
    public void removeExchange(char code) {
        if (in.get(code) <= 0) return;
        in.computeIfPresent(code, (c, i) -> i - 1);
        addCota(getRatio(code));
        repaint();
    }

    @Override
    public void removePlayerRessource(char code) {
        if (out.get(code) <= 0) return;
        out.computeIfPresent(code, (c, i) -> i - 1);
        removeCota(1);
        repaint();
    }

    @Override
    public void addExchange(char code) {
        in.computeIfPresent(code, (c, i) -> i + 1);
        removeCota(getRatio(code));
        repaint();
    }

    @Override
    public Map<Character, Integer> initRatio() {
        Map<Character, Integer> map = new HashMap<>();
        out.forEach((code, ignored) -> {
            Port port = player.getUsefulPort(new CarteRessource(code));
            int n = (port != null) ? port.getRatio() : 4;
            map.put(code, n);
        });
        return map;
    }
}
