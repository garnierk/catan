package up.catansColons.ui.controllers;

import up.catansColons.board.composant.Tile;
import up.catansColons.player.Joueur;
import up.catansColons.ui.models.Model;
import up.catansColons.ui.uiG.views.objects.*;
import up.catansColons.ui.uiG.views.objects.board.BoardElementGraphic;
import up.catansColons.ui.uiG.views.objects.board.EdgeGraphic;
import up.catansColons.ui.uiG.views.objects.board.TilePanel;
import up.catansColons.ui.uiG.views.objects.board.VertexGraphic;
import up.catansColons.ui.uiG.views.objects.player.ExchangePanel;
import up.catansColons.ui.uiG.views.scenes.GameScene;
import up.catansColons.ui.uiG.views.scenes.SettingsScene;

import javax.swing.*;

/**
 * Controlleur du jeu
 */
public class GameController implements Controller {

    private final Model model;
    private final GameScene scene;
    private final JFrame window;
    private ActionButtonPanel actionButtonPanel;
    private CustomDialog constructionMenu;
    private CustomDialog cards;
    private boolean isCardActive;
    private int counter;
    private boolean first;
    private boolean second;
    private boolean abort;

    public GameController(GameScene scene, Model model, JFrame window) {
        this.scene = scene;
        this.model = model;
        this.window = window;
        isCardActive = false;
        counter = 0;
        abort = false;
    }

    public void setActionButtonPanel(ActionButtonPanel actionButtonPanel) {
        this.actionButtonPanel = actionButtonPanel;
    }

    @Override
    public void launchDice() {
        int dice = model.launchDice();
        actionButtonPanel.getDiceButton().setEnabled(false);
        actionButtonPanel.getTakeDevCards().setEnabled(model.getCurrent().canTakeCard() && !model.isCardStackEmpty());
        actionButtonPanel.getConstruction().setEnabled(model.getCurrent().isAbleToConstruct().size() > 0);
        actionButtonPanel.getMoveThief().setEnabled(dice == 7);
        actionButtonPanel.getEndRoundButton().setEnabled(dice != 7);
        if (constructionMenu != null && constructionMenu.isVisible())
            constructionMenu.repaint(model.getCurrent().isAbleToConstruct().size() > 0);
        scene.repaint();
    }

    public void exchange() {
        new CustomDialog(window, new ExchangePanel(this, window), true).setVisible(true);
    }

    @Override
    public void takeCard() {
        model.getCurrent().prendreDeveloppement();
        actionButtonPanel.getTakeDevCards().setEnabled(model.getCurrent().canTakeCard() && !model.isCardStackEmpty());
        actionButtonPanel.getConstruction().setEnabled(model.getCurrent().isAbleToConstruct().size() > 0);
        scene.repaint();
        if (constructionMenu != null && constructionMenu.isVisible())
            constructionMenu.repaint(model.getCurrent().isAbleToConstruct().size() > 0);
    }

    @Override
    public void useCard(char c) {
        cards.setVisible(false);
        switch (c) {
            case 'C':
                this.knightCard();
                break;
            case 'R':
                this.constructionCard();
                break;
            case 'D':
                this.harvestCard();
                break;
        }
    }

    @Override
    public void knightCard() {
        isCardActive = false;
        setActiveTilePanel(true);
        actionButtonPanel.getUseDevCards().setEnabled(false);
        model.getCurrent().useCard('C');
        model.setHighestKnight();
    }

    @Override
    public void harvestCard() {
        isCardActive = true;
        actionButtonPanel.setVisible(false);
        if (counter >= 2) {
            actionButtonPanel.setVisible(true);
            setActiveTilePanel(false);
            model.getCurrent().useCard('D');
            isCardActive = false;
            counter = 0;
        } else {
            setActiveTilePanel(true);
        }
    }

    @Override
    public void repaint() {
        scene.repaint();
        actionButtonPanel.getConstruction().setEnabled(model.getCurrent().isAbleToConstruct().size() > 0);
        actionButtonPanel.getTakeDevCards().setEnabled(model.getCurrent().canTakeCard() && !model.isCardStackEmpty());
    }

    @Override
    public void constructionCard() {
        isCardActive = true;
        actionButtonPanel.setVisible(false);
        if (counter >= 2 || abort) {
            actionButtonPanel.setVisible(true);
            setActiveEdgeGraphics(false);
            model.getCurrent().useCard('R');
            isCardActive = false;
            counter = 0;
            abort = false;
        } else {
            setActiveEdgeGraphics(true);
        }
    }

    @Override
    public void endRound() {
        model.changeTurn();
    }

    @Override
    public void startRound() {
        roundStartDialog();
        actionButtonPanel.getDiceButton().setEnabled(true);
        actionButtonPanel.getExchangeButton().setEnabled(true);
        actionButtonPanel.getConstruction().setEnabled(model.getCurrent().isAbleToConstruct().size() > 0);
        actionButtonPanel.getMoveThief().setEnabled(false);
        actionButtonPanel.getEndRoundButton().setEnabled(false);
        actionButtonPanel.getTakeDevCards().setEnabled(model.getCurrent().canTakeCard() && !model.isCardStackEmpty());
        actionButtonPanel.getUseDevCards().setEnabled(model.getCurrent().hasDevCard());
        showBuildChoices('*');
        if (constructionMenu != null) constructionMenu.setVisible(false);
        second = false;
        first = false;
        isCardActive = false;
        constructionMenu = null;
        scene.repaint();
    }

    public void setActiveTilePanel(boolean active) {
        TilePanel.setActive(active);
        scene.repaint();
    }

    public void setActiveEdgeGraphics(boolean active) {
        EdgeGraphic.setActive(active);
        scene.repaint();
    }

    @Override
    public void overLimitResources(Joueur p) {
        if (p.getRessourcesNumber() <= 7) return;
        JDialog dialog = new CustomDialog(window, new OverLimitPanel(window, p), true);
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        dialog.setVisible(true);
    }

    public void showBuildChoices(char c) {
        switch (c) {
            case 'r':
                EdgeGraphic.setActive(true);
                VertexGraphic.setEmptyActive(false);
                VertexGraphic.setColonyActive(false);
                break;
            case 'c':
                VertexGraphic.setEmptyActive(true);
                VertexGraphic.setColonyActive(false);
                EdgeGraphic.setActive(false);
                break;
            case 't':
                VertexGraphic.setColonyActive(true);
                EdgeGraphic.setActive(false);
                VertexGraphic.setEmptyActive(false);
                break;

            default:
                EdgeGraphic.setActive(false);
                VertexGraphic.setEmptyActive(false);
                VertexGraphic.setColonyActive(false);
        }
        scene.repaint();


    }

    @Override
    public Model getModel() {
        return model;
    }

    public void showConstructionMenu() {
        if (constructionMenu != null) constructionMenu.setVisible(false);
        constructionMenu = new CustomDialog(window, new ConstructionPanel(model.getCurrent(), this), false);
        constructionMenu.setVisible(model.getCurrent().isAbleToConstruct().size() > 0);
    }

    public void build(char c, BoardElementGraphic graphic) {
        boolean isBuild = model.getCurrent().construirePiece(c, graphic.getTile(), graphic.getVertex(), graphic.getEdge());
        if (!isBuild) impossibleConstructionMessage();
        if (constructionMenu != null) constructionMenu.repaint();
        model.setLongestRoad();
        actionButtonPanel.getConstruction().setEnabled(model.getCurrent().isAbleToConstruct().size() > 0);
        constructionMenu.repaint(model.getCurrent().isAbleToConstruct().size() > 0);
        scene.repaint();
    }

    @Override
    public void moveThief(Tile tile) {
        model.getCurrent().moveThief(tile, model.getThief());
        actionButtonPanel.getMoveThief().setEnabled(false);
        scene.repaint();
        actionButtonPanel.getEndRoundButton().setEnabled(true);
    }

    public void showCards() {
        if (cards != null) cards.setVisible(false);
        cards = new CustomDialog(window, new DevCardsGraphicsPanel(model.getCurrent(), this), false);
        cards.setVisible(model.getCurrent().getDevCardsNumber() > 0);
    }

    public void tileAction(Tile tile) {
        if (!isCardActive) moveThief(tile);
        else {
            model.getCurrent().harvest(tile);
            actionButtonPanel.getUseDevCards().setEnabled(false);
            counter++;
            harvestCard();
        }
    }

    public void edgeAction(char c, BoardElementGraphic graphic) {
        if (first || second) {
            buildFirst(c, graphic);
            return;
        }
        if (!isCardActive) build(c, graphic);
        else {
            if (!model.getCurrent().useConstructionCard(graphic.getEdge())) {
                impossibleConstructionMessage();
                showAbort();
            } else {
                actionButtonPanel.getUseDevCards().setEnabled(false);
                counter++;
            }
            constructionCard();
        }
    }

    private void showAbort() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(new JLabel("Voulez vous arreter d'utiliser la carte ?"));
        JPanel buttons = new JPanel();
        JButton abort = new JButton("Oui");
        abort.addActionListener(e -> this.abort = true);
        JButton keep = new JButton("Non");
        keep.addActionListener(e -> this.abort = false);
        buttons.add(abort);
        buttons.add(keep);
        panel.add(buttons);
        new CustomDialog(window, panel, true);
    }

    public void vertexAction(char c, BoardElementGraphic graphic) {
        if (first || second) buildFirst(c, graphic);
        else build(c, graphic);
    }

    public void buildFirst(char c, BoardElementGraphic graphic) {
        if (c == 'c') buildFirstColony(graphic);
        else buildFirstRoad(graphic);
    }

    private void buildFirstColony(BoardElementGraphic graphic) {
        if (model.getCurrent().placeFirstColony(graphic.getTile(), graphic.getVertex())) {
            if (second) model.getCurrent().harvest(graphic.getVertex().getNextTiles());
            JOptionPane.showMessageDialog(window, "Veuillez placer une route");
            showBuildChoices('r');
            return;
        }
        impossibleConstructionMessage();
    }

    private void buildFirstRoad(BoardElementGraphic graphic) {
        if (model.getCurrent().placeFirstRoad(graphic.getTile(), graphic.getVertex(), graphic.getEdge())) {
            actionButtonPanel.getEndRoundButton().setEnabled(true);
            showBuildChoices('*');
            return;
        }
        impossibleConstructionMessage();
        JOptionPane.showMessageDialog(window, "Veuillez placer une route");
        showBuildChoices('r');
    }

    private void startFirst() {
        roundStartDialog();
        actionButtonPanel.getDiceButton().setEnabled(false);
        actionButtonPanel.getExchangeButton().setEnabled(false);
        actionButtonPanel.getConstruction().setEnabled(false);
        actionButtonPanel.getMoveThief().setEnabled(false);
        actionButtonPanel.getEndRoundButton().setEnabled(false);
        actionButtonPanel.getTakeDevCards().setEnabled(false);
        actionButtonPanel.getUseDevCards().setEnabled(false);
        if (constructionMenu != null) constructionMenu.setVisible(false);
        showBuildChoices('*');
        isCardActive = false;
        constructionMenu = null;
        if (first) first = false;
        if (second) second = false;
        scene.repaint();
    }

    @Override
    public void impossibleConstructionMessage() {
        JOptionPane.showMessageDialog(scene, "Vous ne pouvez pas construire ici", "Attention", JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void roundStartDialog() {
        JOptionPane.showMessageDialog(scene,
                "Au tour de " + model.getCurrent().getPseudo() + " de jouer");
    }

    @Override
    public void playSecondRound() {
        startFirst();
        second = true;
        JOptionPane.showMessageDialog(window, "Veuillez placer une colonie");
        showBuildChoices('c');
    }

    @Override
    public void playFirstRound() {
        startFirst();
        first = true;
        JOptionPane.showMessageDialog(window, "Veuillez placer une colonie");
        showBuildChoices('c');
    }

    @Override
    public void playRound() {
        startRound();
    }

    @Override
    public void victory(Joueur current) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(new JLabel(current.getPseudo() + " win!"));
        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons, BoxLayout.LINE_AXIS));
        JButton replay = new JButton("Rejouer");
        JButton quit = new JButton("Quitter");
        replay.addActionListener(e -> replay());
        quit.addActionListener(e -> quitGame());
        buttons.add(replay);
        buttons.add(quit);
        panel.add(buttons);
        new CustomDialog(window, panel, true).setVisible(true);
        actionButtonPanel.disableAll();
    }

    private void quitGame() {
        System.exit(0);
    }

    private void replay() {
        SettingsScene newScene = new SettingsScene(this.window);
        window.add(newScene);
        window.setContentPane(newScene);
        window.remove(scene);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }

}
