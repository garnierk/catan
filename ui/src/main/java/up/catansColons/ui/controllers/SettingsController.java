package up.catansColons.ui.controllers;

import javax.swing.*;

import up.catansColons.ui.uiG.views.scenes.SettingsScene;
import up.catansColons.game.Config;
import up.catansColons.ui.models.Model;
import up.catansColons.ui.uiG.views.scenes.GameScene;

public class SettingsController {

    private final Model model;
    private final SettingsScene scene;
    private final JFrame window;

    public SettingsController(SettingsScene scene, Model model, JFrame window) {
        this.scene = scene;
        this.model = model;
        this.window = window;
    }

    public void readPseudo() {
        this.model.setProfil(this.scene.getPseudo().getText());
        this.model.addPlayer(this.model.getProfil().getPseudo());
        this.scene.removeAll();
        this.scene.isAgainstPlayer();
    }

    public void startGame() {
        this.scene.removeAll();
        this.scene.afficheStart();
    }

    public void readOpponent1() {
    	if(this.model.getConfig().estComplete() || (this.model.getConfig().getPlayerList().size() == 2)) {
    		return;
    	}
        for (int i = 1; i < 4; i++) {
            this.model.addIA("IA" + i);
        }
        this.readGame();
    }

    public void readOpponent2() {
    	if(this.model.getConfig().estComplete()) {
    		return;
    	}
        for (int i = 1; i < 3; i++) {
            this.model.addIA("IA" + i);
        }
        this.readGame();
    }

    public void readLoad(JTextField text) {
        String saveName = this.readString(text);
        for (Config c : this.model.getProfil().getConfigSave()) {
            if (c.getNameConfig().equals(saveName)) {
                this.model.setConfig(c);
                for (int i = 0; i < this.model.getProfil().getConfigSave().size(); i++) {
                    System.out.println(this.model.getProfil().getConfigSave().get(i).getPlayerList().size());
                }
                this.readGame();
            }
        }
        this.scene.removeAll();
        this.scene.isAgainstPlayer();
    }

    public void readOpponent3() {
        this.scene.removeAll();
        this.scene.askConfig(1);
    }

    public String readString(JTextField text) {
        return text.getText();
    }

    public int readIntFromTextField(JTextField text) {
        return Integer.parseInt(text.getText());
    }

    public int readIntFromMenu(JComboBox<String> menu) {
        String s = menu.getItemAt(menu.getSelectedIndex());
        return Integer.parseInt(s);
    }

    public void readGame() {
        model.initGame();
        GameScene newScene = new GameScene(this.window, this.model);
        window.add(newScene);
        window.setContentPane(newScene);
        window.remove(scene);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        newScene.play();
    }

    public void saveConfig(JTextField text) {
        Config newConfig = this.model.getConfig();
        newConfig.setName(text.getText());
        this.model.addSave(newConfig);
    }

}
