package up.catansColons.ui.uiT;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.game.Config;
import up.catansColons.player.IA;
import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.TUIController;
import up.catansColons.ui.models.Model;

public class Vue {

    private Model model;
    private TUIController controller;

    public Vue() {
        this.controller = new TUIController(this);
        controller.readPseudo();
    }

    public void demandePseudo() {
        System.out.println("Quel est votre Pseudo ?");
    }

    public void demandeAction1() {
        System.out.println("Vous pouvez lancer une partie en ecrivant \"new\" ou \n" +
                "chargez une partie deja existante avec \"load\". \n" +
                " Vous pouvez aussi quittez le jeu en ecrivant \"quit\", \n" +
                "pour voir votre profil ecrivez \"profile\"");
    }

    public void demandeAction2() {
        System.out.println("Pour lancer une configuration deja existante ecrivez \"reload\", \n" +
                "pour creez une nouvelle configuration ecrivez \"new\" et \n"
                + " pour revenir en arriere utiliser \"return\"");
    }

    public void demandeAction3() {
        System.out.println("Pour configurer la partie ecrivez \"make\", pour lance la partie \n" +
                "utilisez \"play\" et pour revenir en arriere ecrivez \"return\". \n"
                + "Vous pouvez aussi utiliser \"see\" pour voir la configuration actuel.");
    }

    public void demandeIA() {
        System.out.println("Combien de joueur dans la Partie (3 ou 4)?");
    }

    public void askColonyEmplacement() {
        if (controller.first || controller.second) {
            System.out.println("Ou souhaitez vous placer la colony ainsi que sa route correspondante ?");
        } else {
            System.out.println("Veuillez indiquer l'emplacement de la future colonie");
        }

        Tile t = this.model.getBoard().getTile(controller.readTileCoordinate());
        Vertex v = t.getVertex(controller.readVertexCoordinate());
        Edge e = controller.readEdgeCoordinate(v);
        this.controller.tryBuild('c', t, v, e);
    }

    public void askCityEmplacement() {
        System.out.println("Veuillez indiquer l'emplacement de la future ville");
        Tile t = this.model.getBoard().getTile(controller.readTileCoordinate());
        Vertex v = t.getVertex(controller.readVertexCoordinate());
        this.controller.build('t', t, v, v.getFreeEdge());
    }

    public void askRoadEmplacement() {
        System.out.println("Veuillez indiquer l'emplacement de la future ville");
        Tile t = this.model.getBoard().getTile(controller.readTileCoordinate());
        Vertex v = t.getVertex(controller.readVertexCoordinate());
        Edge e = controller.readEdgeCoordinate(v);
        this.controller.build('r', t, v, e);
    }


    public void demandeReload() {
        System.out.println("Indiquez dans la liste suivante le nom d'une configuration a chargez");
        for (Config c : this.model.getProfil().getConfigSave()) {
            System.out.println(c.getNameConfig());
        }
    }

    public void demandeConfig() {
        System.out.println("Indiquez le nombre de joueur humain dans la partie et " +
                "pour chacun d'entre eux leur nom.");
    }

    public void demandeSave() {
        System.out.println("Pour sauvegarder la configuration ecrivez \"s\" puis donnez un nom a la sauvegarde.");
    }

    public void seeProfil() {
        System.out.println("Nom de l'Utilisateur :");
        System.out.println(this.model.getProfil().getPseudo());
        System.out.println("Liste des configuration sauvegarde : ");
        System.out.println();
        for (Config c : this.model.getProfil().getConfigSave()) {
            System.out.println(c.getNameConfig());
        }
    }

    public void seeConfig() {
        System.out.println("Config " + this.model.getConfig().getNameConfig());
        System.out.println();
        System.out.println("Owner : " + this.model.getConfig().getOwner().getPseudo());
        for (Joueur j : this.model.getConfig().getPlayerList()) {
            if (j instanceof IA) {
                System.out.println("IA " + j.getColor() + " : " + j.getPseudo());
            } else if (j instanceof Joueur) {
                System.out.println("Joueur " + j.getColor() + " : " + j.getPseudo());
            }
        }
    }

    public void affiche(String erreur) {
        System.out.println(erreur);
    }

    public void miseAJour() {
        model.getBoard().print();
        System.out.println("Valeur du de :" + model.getDice());
        model.getCurrent().affiche();
    }

    public void setController(TUIController c) {
        this.controller = c;
    }

    public void setModel(Model m) {
        this.model = m;
    }

    public void printRoundOption() {
        miseAJour();
        System.out.println("Ecrivez :");
        if (!controller.isDiceLaunched()) System.out.println("dice - pour lancer le de");
        System.out.println("exchange - pour echanger");
        if (controller.isTakeCard()) System.out.println("take - pour prendre une carte developpement");
        if (controller.isHasCard()) System.out.println("use - pour utiliser une carte developpement");
        if (controller.isAbleToConstruct()) System.out.println("build - pour placer une piece");
        if (controller.isThief()) System.out.println("thief - pour placer le voleur");
        if (controller.end) System.out.println("end - pour finir le tour");
    }

    public void showExchanges() {
        model.getCurrent().affiche();
    }

    public void showCards() {
        if (!controller.isHasCard()) {
            System.out.println("Vous n'avez pas de cartes. Entrez return pour revenir en arriere");
            return;
        }
        model.getCurrent().affiche();
    }

    public void askBuildType() {
        if (!controller.isAbleToConstruct()) {
            System.out.println("Vous n'avez pas les materiaux necessaire a la construction");
            System.out.println("Ecrivez return pour revenir en arriere");
            return;
        }
        System.out.println("Ecrivez :");
        if (model.getCurrent().construireColonie()) System.out.println("colony - placer une colonie");
        if (model.getCurrent().construireRoute()) System.out.println("road - construire une route");
        if (model.getCurrent().canBuildCity()) System.out.println("city - construire une ville");
    }

    public void askOverLimit(int i) {
        System.out.println("Veuillez vous separer de " + i + " ressource(s)");
    }

    public void thief() {
        System.out.println("Veuillez deplacer le voleur");
    }

    public void constructionCard() {
        System.out.println("Vous pouvez construire deux routes gratuitement");
    }

    public void harvestCard() {
        System.out.println("Vous pouvez recolter deux ressources de votre choix");
    }
}
