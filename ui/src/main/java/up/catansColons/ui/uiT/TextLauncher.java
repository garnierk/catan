package up.catansColons.ui.uiT;

import up.catansColons.ui.controllers.TUIController;

public class TextLauncher {

    public TextLauncher() {
        Vue view = new Vue();
        TUIController controleur = new TUIController(view);
        view.setController(controleur);
    }

}
