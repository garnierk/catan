package up.catansColons.ui.models;

import up.catansColons.board.Board;
import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.board.piece.Thief;
import up.catansColons.cards.CarteDeveloppement;
import up.catansColons.game.Config;
import up.catansColons.game.Game;
import up.catansColons.player.IA;
import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.Controller;
import up.catansColons.ui.controllers.SettingsController;

public class Model {
    private static final int VICTORY = 3;
    private final Board board;
    private int roundCounter;
    private Config config;
    private Joueur current;
    private int dice;
    private Controller controller;
    private SettingsController settingsController;
    private Game profil;
    private boolean finished;

    public Model() {
        board = new Board(5, 5);
        config = new Config();
        config.getPlayerList().forEach(Joueur::initPiece);
        dice = 0;
        roundCounter = 1;
        finished = false;
    }

    public void addPlayer(String name) {
        this.config.ajouteJoueur(name);
    }

    public void addIA(String name) {
        this.config.ajouteIA(name);
    }

    public void addSave(Config save) {
        this.profil.addSave(save);
    }

    public Game getProfil() {
        return this.profil;
    }

    public void setProfil(String pseudo) {
        this.profil = new Game(pseudo);
    }

    public Board getBoard() {
        return board;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config c) {
        config = c;
    }

    public Joueur getCurrent() {
        return current;
    }

    public void setCurrent(Joueur current) {
        this.current = current;
    }

    public void setCurrent(int id, boolean invert) {
        id += ((invert) ? -1 : 1);
        id = id % config.getNbJoueur();
        id = (id < 0) ? config.getNbJoueur() + id : id;
        this.current = getConfig().getPlayerList().get(id);
    }

    public boolean isCardStackEmpty() {
        return CarteDeveloppement.isEmpty();
    }

    public void changeTurn() {
        if (finished) return;
        if (current.getScore() >= VICTORY) {
            controller.victory(current);
            finished = true;
        }
        setCurrent(current.getID(), false);
        roundCounter++;
        if (roundCounter <= config.getNbJoueur()) playFirstRound();
        else if (roundCounter <= (config.getNbJoueur() * 2)) playSecondRound();
        else playRound();
    }

    public void playFirstRound() {
        if (current instanceof IA) {
            current.playFirstRound(board);
            changeTurn();
        } else {
            controller.playFirstRound();
        }

    }

    private void playSecondRound() {
        if (current instanceof IA) {
            current.playSecondRound(board);
            controller.endRound();
        } else {
            controller.playSecondRound();

        }
    }

    private void playRound() {
        if (current instanceof IA) {
            launchDice();
            current.playRound(board, dice);
            changeTurn();
        } else {
            controller.playRound();
        }
    }

    public int getDice() {
        return dice;
    }

    public int launchDice() {
        this.dice = Joueur.lanceDe();
        getConfig().getPlayerList().forEach(p -> p.harvestRessources(dice));
        controller.repaint();
        if (dice == 7) {
            getConfig().getPlayerList().forEach(p -> {
                if (p instanceof IA) p.overLimitRessources();
                else controller.overLimitResources(p);
            });
        }
        return dice;
    }

    public void setHighestKnight() {
        int n = 0;
        Joueur player = config.getPlayerList().get(0);
        for (Joueur p : config.getPlayerList()) {
            int k = p.getUsedKnightCards();
            if (k > n && k >= 3) {
                n = k;
                player = p;
            } else {
                p.setHighestKnight(false);
            }
        }
        player.setHighestKnight(n >= 3);
    }

    public void setLongestRoad() {
        int n = 0;
        Joueur player = config.getPlayerList().get(0);
        for (Joueur p : config.getPlayerList()) {
            int l = p.getLongestRoadSize();
            if (l > n && l >= 5) {
                n = l;
                player = p;
            } else {
                p.setLongestRoad(false);
            }
        }
        player.setLongestRoad(n >= 5);
    }

    public Thief getThief() {
        return getBoard().getThief();
    }

    public void setController(Controller controller) {
        if (controller != null) this.controller = controller;
    }

    public void setController(SettingsController controller) {
        if (controller != null) this.settingsController = controller;
    }

    public int getRoundCounter() {
        return roundCounter;
    }

    public void setnbPlayer(int nbPlayer) {
        this.config.setnbJoueur(nbPlayer);
    }

    public Edge getEdge(Vertex vertex, char pos) {
        Tile t = board.getTile(vertex.getTile(), pos);
        boolean horizontal = (pos == 'd' || pos == 'g');
        if (t == vertex.getTile()) {
            Edge e = vertex.getEdge(vertex.getVertex(vertex, pos));
            if (e.isHorizontal() == horizontal) {
                return e;
            }

        }
        for (var v : vertex.getVertices()) {
            if (t.isVertexPresent(v)) {
                Edge e = vertex.getEdge(v);
                if (e.isHorizontal() == horizontal) {
                    return e;
                }
            }
        }
        return null;
    }

    private int[] vertexPos(char pos) {
        switch (pos) {
            case 'g':
            case 'b':
                return new int[]{1, 0};
            case 'd':
                return new int[]{0, 1};
            default:
                return new int[]{0, 0};
        }
    }

    public void initGame() {
        if (config != null && config.estComplete()) {
            config.getPlayerList().forEach(Joueur::initPiece);
            CarteDeveloppement.initDevCardStack();
            current = config.getPlayerList().get(0);
        }
    }
}
