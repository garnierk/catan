package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Port;
import up.catansColons.board.composant.Tile;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import java.awt.*;

/**
 * Afichage d'un port
 */
public class PortPanel extends TilePanel implements BoardElementGraphic {


    private final Port tile;

    public PortPanel(Tile t, GameController controller) {
        super(t, controller);
        tile = (Port) t;
    }

    @Override
    public void initTile(GridBagConstraints constraints) {
        Port temp = (Port) super.getTile();
        add(new JLabel(temp.getRessourceOutput() + temp.getNumber()), constraints);
        constraints.gridy++;
        add(new JLabel("(" + temp.getPortRessourceOutput() + " " + temp.getRatio() + ":1)"), constraints);
        constraints.gridy++;
    }
}
