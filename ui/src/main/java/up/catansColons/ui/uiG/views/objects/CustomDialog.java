package up.catansColons.ui.uiG.views.objects;

import javax.swing.*;
import java.awt.*;

public class CustomDialog extends JDialog {
    private final Component child;

    public CustomDialog(Frame owner, Component child, boolean modal) {
        super(owner, modal);
        add(child);
        this.child = child;
        setLocationRelativeTo(owner);
        setMinimumSize(child.getPreferredSize());
        pack();
    }

    @Override
    public void paint(Graphics g) {
        revalidate();
        if (isEmpty()) setVisible(false);
        super.paint(g);
    }

    public boolean isEmpty() {
        return child == null || !child.isVisible();
    }

    public void repaint(boolean visible) {
        setVisible(visible);
        revalidate();
        super.repaint();
    }

}
