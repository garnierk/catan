package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.board.piece.City;
import up.catansColons.board.piece.Colony;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;

/**
 * Affichage des arretes sur le plateau
 *
 * @see JButton
 * @see BoardElementGraphic
 * @see Edge
 * @see GameController
 */

public class VertexGraphic extends JButton
        implements BoardElementGraphic {

    private static boolean emptyActive = false;
    private static boolean colonyActive = false;
    private final Vertex vertex;
    private final GameController controller;
    private char type;
    private Shape shape;


    public VertexGraphic(Vertex vertex, GameController gameController) {
        this.vertex = vertex;
        this.setBackground(EMPTY_STRUCTURE);
        setFocusable(false);
        setPreferredSize(setAllSize(getPreferredSize()));
        setContentAreaFilled(false);
        setForeground(Color.WHITE);
        this.controller = gameController;
        if (gameController != null) initAction();
        setUI(new MetalButtonUI() {
            @Override
            protected Color getDisabledTextColor() {
                return Color.WHITE;
            }
        });
    }

    public static void setEmptyActive(boolean emptyActive) {
        VertexGraphic.emptyActive = emptyActive;
    }

    public static void setColonyActive(boolean active) {
        VertexGraphic.colonyActive = active;
    }

    /**
     * Definit la taille du Jpanel
     *
     * @return la taille definit par BoardGraphic
     */
    @SuppressWarnings("all")
    private Dimension setAllSize(Dimension size) {
        size.width = Math.min(BoardElementGraphic.WIDTH, BoardElementGraphic.HEIGHT);
        size.height = size.width;
        return size;
    }

    /**
     * Initialise l'action quand le bouton est active
     */
    private void initAction() {
        addActionListener(e -> controller.vertexAction(type, this));
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (!vertex.isFree()) {
            type = 't';
            g.setColor(ID_TO_COLOR.get(vertex.getColony().getPlayerId()));
            String txt = (vertex.getColony() instanceof City) ? "v" : "c";
            setEnabled(colonyActive && txt.equals("c"));
            this.setText(txt);
        } else {
            setBackground(EMPTY_STRUCTURE);
            g.setColor(EMPTY_STRUCTURE);
            setText("");
            setEnabled(emptyActive);
            type = 'c';
        }
        Dimension size = getSize();
        g.fillRect(0, 0, size.width, size.height);
        super.paintComponent(g);
    }

    @Override
    protected void paintBorder(Graphics g) {
        if (emptyActive && vertex.isFree()) g.setColor(Color.YELLOW);
        else if (colonyActive && occupiedByColony()) g.setColor(Color.YELLOW);
        Dimension size = getSize();
        g.drawRect(0, 0, size.width, size.height);
        super.paintBorder(g);
    }

    private boolean occupiedByColony() {
        return (!vertex.isFree()) && (vertex.getColony().getClass() == Colony.class);
    }

    @Override
    public boolean contains(int x, int y) {
        if (shape == null || !shape.getBounds().equals(getBounds())) {
            shape = new Rectangle(0, 0, getWidth(), getHeight());
        }
        return shape.contains(x, y);
    }

    @Override
    public Tile getTile() {
        return vertex.getTile();
    }

    public Vertex getVertex() {
        return vertex;
    }

    @Override
    public Edge getEdge() {
        return null;
    }
}
