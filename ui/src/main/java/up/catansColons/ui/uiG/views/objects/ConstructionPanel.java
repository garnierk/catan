package up.catansColons.ui.uiG.views.objects;

import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class ConstructionPanel extends JPanel {
    private final GameController controller;
    private final Joueur player;
    private final CBPanel road;
    private final CBPanel city;
    private final CBPanel colony;

    public ConstructionPanel(Joueur player, GameController controller) {
        this.player = player;
        this.controller = controller;
        setLayout(new GridBagLayout());
        road = new CBPanel('r');
        colony = new CBPanel('c');
        city = new CBPanel('t');
        addComp();
    }

    private void addComp() {
        if (player.construireRoute() && player.getUnplacedRoad() != null) {
            add(road);
        } else {
            remove(road);
        }
        if (player.construireColonie() && player.getUnplacedColony() != null) {
            add(colony);
        } else {
            remove(colony);
        }
        if (player.canBuildCity()) {
            add(city);
        } else {
            remove(city);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        addComp();
        super.paintComponent(g);
    }

    private class ConstructionButton extends JButton {
        private ConstructionButton(char c) {
            super("Construire");
            addActionListener(e -> controller.showBuildChoices(c));
        }
    }

    private class CBPanel extends JPanel {
        public CBPanel(char c) {
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            Map<Character, String> TRAD_MAP = Joueur.setPieceCodeToPrintFr();
            add(new JLabel(TRAD_MAP.get(c)));
            add(new ConstructionButton(c));
        }
    }

}
