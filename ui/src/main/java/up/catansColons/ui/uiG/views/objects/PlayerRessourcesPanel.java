package up.catansColons.ui.uiG.views.objects;

import up.catansColons.ui.models.Model;
import up.catansColons.ui.uiG.views.objects.player.PlayerGraphicsElement;

import javax.swing.*;
import java.awt.*;

public class PlayerRessourcesPanel extends JPanel implements RessourcesRelatedGraphics {
    private final Model model;
    private final JLabel wood;
    private final JLabel rock;
    private final JLabel wool;
    private final JLabel wheat;
    private final JLabel clay;

    public PlayerRessourcesPanel(Model model) {
        this.model = model;
        wood = new JLabel(initText('w'));
        rock = new JLabel(initText('p'));
        wool = new JLabel(initText('l'));
        wheat = new JLabel(initText('b'));
        clay = new JLabel(initText('a'));
        wood.setForeground(Color.WHITE);
        rock.setForeground(Color.WHITE);
        wool.setForeground(Color.WHITE);
        wheat.setForeground(Color.WHITE);
        clay.setForeground(Color.WHITE);
        add(wood);
        add(clay);
        add(wool);
        add(wheat);
        add(rock);
    }

    private String initText(char c) {
        return RESSOURCES_NAME.get(c) + " (" + model.getCurrent().getRessourceNumber(c) + ")";
    }

    @Override
    protected void paintComponent(Graphics g) {
        wood.setText(initText('w'));
        wheat.setText(initText('b'));
        wool.setText(initText('l'));
        clay.setText(initText('a'));
        rock.setText(initText('p'));
        setBackground(PlayerGraphicsElement.PLAYER_COLOR.get(model.getCurrent().getID()));
        super.paintComponent(g);
    }
}
