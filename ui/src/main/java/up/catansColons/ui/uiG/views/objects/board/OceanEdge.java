package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

import javax.swing.*;
import java.awt.*;

/**
 * Represente l'equivalent d'une arrete pour l'ocean
 *
 * @see EdgeGraphic
 * @see BoardElementGraphic
 */
public class OceanEdge extends JPanel
        implements BoardElementGraphic {

    public OceanEdge(boolean horizontal) {
        int width = (horizontal) ? BoardElementGraphic.WIDTH : BoardElementGraphic.HEIGHT;
        int height = (horizontal) ? BoardElementGraphic.WIDTH : BoardElementGraphic.HEIGHT;
        setPreferredSize(new Dimension(width, height));
        setBackground(OCEAN);
    }

    @Override
    protected void paintBorder(Graphics g) {
        g.setColor(Color.BLACK);
        super.paintBorder(g);
    }

    @Override
    public Tile getTile() {
        return null;
    }

    @Override
    public Vertex getVertex() {
        return null;
    }

    @Override
    public Edge getEdge() {
        return null;
    }
}
