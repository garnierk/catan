package up.catansColons.ui.uiG.views.objects.player;

import up.catansColons.player.Joueur;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class AllPlayerPanel extends JPanel {

    public AllPlayerPanel(List<Joueur> playerList) {
        setLayout(new FlowLayout());
        playerList.forEach(player -> add(new PlayerPanel(player)));
    }

    public static class PlayerPanel extends JPanel implements PlayerGraphicsElement {

        private final Joueur player;
        private final Color color;


        public PlayerPanel(Joueur player) {
            this.player = player;
            this.color = Color.LIGHT_GRAY;
            setLayout(new BorderLayout());
            add(new PlayerHeader(player), BorderLayout.PAGE_START);
            initCentralPane();
            setBackground((color));
        }

        private void initCentralPane() {
            JPanel pane = new JPanel();
            pane.setLayout(new FlowLayout());
            pane.setBackground(color);
            pane.add(new RessourceCardsGraphic(player));
            pane.add(new DevCardsGraphics(player));
            pane.add(new LongestRoadPanel(player));
            pane.add(new KnightCardsPlayed(player));
            add(pane, BorderLayout.CENTER);
        }

        public static class PlayerHeader extends JPanel implements PlayerGraphicsElement {
            private final Joueur player;
            private final JLabel score;

            public PlayerHeader(Joueur player) {
                this.player = player;
                setLayout(new FlowLayout());
                setBackground(PLAYER_COLOR.get(player.getID()));
                score = new JLabel();
                score.setToolTipText("Score");
                score.setForeground(Color.WHITE);
                add(score);
                JLabel label = new JLabel(player.getPseudo());
                label.setForeground(Color.WHITE);
                add(label);
            }

            @Override
            protected void paintComponent(Graphics g) {
                score.setText(String.valueOf(player.getScore()));
                super.paintComponent(g);
            }
        }

        private static class RessourceCardsGraphic extends JPanel implements PlayerGraphicsElement {
            private final Joueur player;
            private final JLabel number;

            public RessourceCardsGraphic(Joueur player) {
                this.player = player;
                setBackground(PLAYER_COLOR.get(player.getID()));
                number = new JLabel("");
                number.setForeground(Color.WHITE);
                add(number);
                setToolTipText("Nombre de ressources");
            }

            @Override
            protected void paintComponent(Graphics g) {
                number.setText(String.valueOf(player.getRessourcesNumber()));
                super.paintComponent(g);
            }
        }

        private static class LongestRoadPanel extends JPanel implements PlayerGraphicsElement {

            private final Joueur player;
            private final JLabel number;

            public LongestRoadPanel(Joueur player) {
                this.player = player;
                setBackground(PLAYER_COLOR.get(player.getID()));
                number = new JLabel("");
                number.setForeground(Color.WHITE);
                add(number);
                setToolTipText("Longueur de la route la plus longue ");
            }

            @Override
            protected void paintComponent(Graphics g) {
                number.setText(String.valueOf(player.getLongestRoadSize()));
                super.paintComponent(g);
            }
        }

        private static class DevCardsGraphics extends JPanel implements PlayerGraphicsElement {

            private final Joueur player;
            private final JLabel number;

            public DevCardsGraphics(Joueur player) {
                this.player = player;
                setBackground(PLAYER_COLOR.get(player.getID()));
                number = new JLabel("");
                number.setForeground(Color.WHITE);
                add(number);
                setToolTipText("Nombre de carte Developpement");
            }

            @Override
            protected void paintComponent(Graphics g) {
                number.setText(String.valueOf(player.getDevCardsNumber()));
                super.paintComponent(g);
            }
        }

        public static class KnightCardsPlayed extends JPanel {
            private final Joueur player;
            private final JLabel number;

            public KnightCardsPlayed(Joueur player) {
                this.player = player;
                setBackground(PLAYER_COLOR.get(player.getID()));
                number = new JLabel("");
                number.setForeground(Color.WHITE);
                add(number);
                setToolTipText("Carte chevalier jouee(s)");
            }

            @Override
            protected void paintComponent(Graphics g) {
                number.setText(String.valueOf(player.getUsedKnightCards()));
                super.paintComponent(g);
            }
        }
    }
}
