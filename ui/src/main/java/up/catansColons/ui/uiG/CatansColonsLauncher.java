package up.catansColons.ui.uiG;

import up.catansColons.ui.uiG.views.scenes.Scene;
import up.catansColons.ui.uiG.views.scenes.SettingsScene;

import javax.swing.*;

/**
 * Lanceur du programme sour l'interface graphique
 */
public class CatansColonsLauncher extends JFrame {

    public CatansColonsLauncher() {
        super();
        Scene scene = new SettingsScene(this);
        setDefaultLookAndFeelDecorated(true);
        add(scene);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setTitle("Les Colons de Catane");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
