package up.catansColons.ui.uiG.views.objects;

import up.catansColons.cards.CarteRessource;
import up.catansColons.player.Joueur;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class CostCard extends JPanel {

    private static final Map<Character, String> CODE_TO_PIECE = setupCodeToPiece();


    public CostCard() {
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridy = 0;
        constraints.gridx = 0;
        add(new JLabel("RESSOURCES "), constraints);
        constraints.gridx++;
        add(new JLabel("REQUISES"), constraints);
        addNeededRessourcesLine('r', constraints);
        addNeededRessourcesLine('c', constraints);
        addNeededRessourcesLine('t', constraints);
        addNeededRessourcesLine('d', constraints);
    }

    private static Map<Character, String> setupCodeToPiece() {
        Map<Character, String> codeToPiece = new HashMap<>();
        codeToPiece.put('c', "Colonie");
        codeToPiece.put('t', "Ville");
        codeToPiece.put('r', "Route");
        codeToPiece.put('d', "Développement");
        return codeToPiece;
    }

    private void addNeededRessourcesLine(char c, GridBagConstraints constraints) {
        constraints.gridx = 0;
        constraints.gridy++;
        add(new JLabel(CODE_TO_PIECE.get(c) + " : "), constraints);
        constraints.gridx++;
        add(new CostCardLine(c), constraints);
    }

    private static class CostCardLine extends JPanel implements RessourcesRelatedGraphics {
        /**
         * Represente les ressources necessaire a la construction d'une colonie.
         *
         * @see CarteRessource
         */
        private static final Map<CarteRessource, Integer> COLONY_NEEDED_RESSOURCES = Joueur.COLONY_NEEDED_RESSOURCES;
        /**
         * Represente les ressources necessaire a la construction d'une ville.
         *
         * @see CarteRessource
         */
        private static final Map<CarteRessource, Integer> CITY_NEEDED_RESSOURCES = Joueur.CITY_NEEDED_RESSOURCES;
        /**
         * Represente les ressources necessaire a la construction d'une route.
         *
         * @see CarteRessource
         */
        private static final Map<CarteRessource, Integer> ROAD_NEEDED_RESSOURCES = Joueur.ROAD_NEEDED_RESSOURCES;
        /**
         * Represente les ressources necessaire a l'aquisition d'une carte developpement.
         *
         * @see CarteRessource
         */
        private static final Map<CarteRessource, Integer> DEV_CARDS_NEEDED_RESSOURCES = Joueur.DEV_CARDS_NEEDED_RESSOURCES;

        /**
         * Represente la configuration initiale du nombre de piece en debut de partie.
         */

        public CostCardLine(char c) {
            setLayout(new BorderLayout());
            chooseRequireRessources(c);
        }

        private void chooseRequireRessources(char c) {
            switch (c) {
                case 'c':
                    initRequireressource(COLONY_NEEDED_RESSOURCES);
                    break;
                case 't':
                    initRequireressource(CITY_NEEDED_RESSOURCES);
                    break;
                case 'r':
                    initRequireressource(ROAD_NEEDED_RESSOURCES);
                    break;
                case 'd':
                    initRequireressource(DEV_CARDS_NEEDED_RESSOURCES);
                    break;
            }
        }

        private void initRequireressource(Map<CarteRessource, Integer> neededRessources) {
            JPanel panel = new JPanel();
            neededRessources.forEach((r, i) -> panel.add(new JLabel(RESSOURCES_NAME.get(r.getRessource()) + "(" + i + ") ")));
            add(panel, BorderLayout.CENTER);
        }


    }
}
