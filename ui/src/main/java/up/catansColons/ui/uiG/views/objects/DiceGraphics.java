package up.catansColons.ui.uiG.views.objects;

import up.catansColons.ui.models.Model;

import javax.swing.*;
import java.awt.*;

public class DiceGraphics extends JPanel {
    private final JLabel dice;
    private final Model model;

    public DiceGraphics(Model model) {
        this.model = model;
        dice = new JLabel();
        add(dice);
    }

    @Override
    protected void paintComponent(Graphics g) {
        dice.setText(String.valueOf(model.getDice()));
        super.paintComponent(g);
    }
}
