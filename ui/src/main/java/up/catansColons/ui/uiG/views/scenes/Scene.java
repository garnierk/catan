package up.catansColons.ui.uiG.views.scenes;

import javax.swing.*;

public abstract class Scene extends JPanel {

    public abstract void play();

}
