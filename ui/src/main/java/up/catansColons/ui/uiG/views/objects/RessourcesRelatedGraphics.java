package up.catansColons.ui.uiG.views.objects;

import up.catansColons.cards.CarteRessource;

import java.util.Map;

public interface RessourcesRelatedGraphics {
    Map<Character, String> RESSOURCES_NAME = CarteRessource.RESSOURCE_CODE_TO_PRINT_FR;
}
