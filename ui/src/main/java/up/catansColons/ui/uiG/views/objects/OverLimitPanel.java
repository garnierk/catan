package up.catansColons.ui.uiG.views.objects;

import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.Exchange;
import up.catansColons.ui.controllers.GUIExchangeController;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class OverLimitPanel extends JPanel implements RessourcesRelatedGraphics, Exchange {

    private final GUIExchangeController controller;
    private final Joueur player;
    private final Map<Character, Integer> ratio;
    private Map<Character, Integer> out = initInOut();
    private Map<Character, Integer> in = initInOut();
    private int cota;

    public OverLimitPanel(JFrame window, Joueur player) {
        this.player = player;
        this.controller = new GUIExchangeController(player, this, window);
        ratio = initRatio();
        JPanel playerExchange = new JPanel();
        RESSOURCES_NAME.keySet().forEach((c) -> playerExchange.add(new PlayerRessources(c)));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        cota = player.getRessourcesNumber() / 2;
        JLabel label = new JLabel(player.getPseudo() + " doit donner ses ressources au voleur");
        label.setHorizontalTextPosition(SwingConstants.CENTER);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(label);
        add(new CentralPanel());
        add(playerExchange);
    }

    public Map<Character, Integer> getOut() {
        return out;
    }

    public Map<Character, Integer> getIn() {
        return in;
    }

    @Override
    public void reset() {
        cota = player.getRessourcesNumber() / 2;
        out = initInOut();
        in = initInOut();
    }

    public int getRatio(char c) {
        return ratio.get(c);
    }

    public void addCota(int n) {
        cota -= n;
    }

    public void removeCota(int n) {
        cota += n;
    }

    public Map<Character, Integer> initRatio() {
        Map<Character, Integer> map = new HashMap<>();
        out.forEach((code, ignored) -> map.put(code, 1));
        return map;
    }

    public Map<Character, Integer> initInOut() {
        Map<Character, Integer> map = new HashMap<>();
        RESSOURCES_NAME.forEach((c, ignored) -> map.put(c, 0));
        return map;
    }


    private class CentralPanel extends JPanel implements RessourcesRelatedGraphics {
        private final JButton accept;
        private final JLabel message;

        private CentralPanel() {
            accept = new JButton("Accepter");
            accept.addActionListener(e -> {
                controller.accept();
                SwingUtilities.getWindowAncestor(this).dispose();
            });
            message = new JLabel("");
            add(message);
            add(accept);
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (cota > 0) {
                message.setText("Veuillez ajouter " + Math.abs(cota) + " ressource(s)");
                accept.setVisible(false);
            } else if (cota < 0) {
                message.setText("Veuillez retirer " + Math.abs(cota) + " ressources(s)");
                accept.setVisible(false);
            } else {
                message.setText("Vous avez donné suffisament de ressource");
                accept.setVisible(true);
            }
            super.paintComponent(g);
        }
    }

    class PlayerRessources extends JPanel implements RessourcesRelatedGraphics {
        private final char code;
        private final JLabel name;
        private final JLabel pos;
        private final JLabel neg;

        PlayerRessources(char code) {
            this.code = code;
            JPanel ressource = new JPanel();
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            name = new JLabel();
            pos = new JLabel();
            neg = new JLabel();
            pos.setForeground(POS);
            neg.setForeground(NEG);
            setContent();
            ressource.setLayout(new BoxLayout(ressource, BoxLayout.LINE_AXIS));
            ressource.add(name);
            ressource.add(pos);
            ressource.add(neg);
            add(ressource);
            add(new PlayerRessourceButton());
            add(new RemoveRessourceButton());
            setVisible(true);
        }

        private void setContent() {
            int i = in.get(code);
            int o = out.get(code);
            name.setText(RESSOURCES_NAME.get(code) + "(" + (player.getRessourceNumber(code) + i - o) + ")");
            if (i > 0) {
                pos.setText("+" + i);
                pos.setVisible(true);
            } else {
                pos.setVisible(false);
            }
            if (o > 0) {
                neg.setText("-" + o);
                neg.setVisible(true);
            } else {
                neg.setVisible(false);
            }
            revalidate();
        }

        @Override
        protected void paintComponent(Graphics g) {
            setContent();
            super.paintComponent(g);
        }

        private class PlayerRessourceButton extends JButton {
            private PlayerRessourceButton() {
                super("Ajouter");
                addActionListener(e -> controller.addPlayerRessources(code));
            }
        }

        private class RemoveRessourceButton extends JButton {
            private RemoveRessourceButton() {
                super("Retirer");
                setVisible(true);
                addActionListener(e -> controller.removePlayerRessource(code));
            }
        }
    }
}
