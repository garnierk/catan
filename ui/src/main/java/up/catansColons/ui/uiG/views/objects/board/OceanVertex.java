package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

import javax.swing.*;
import java.awt.*;

public class OceanVertex extends JPanel
        implements BoardElementGraphic {

    @SuppressWarnings("all")
    public OceanVertex() {
        Dimension size = new Dimension();
        size.width = Math.min(BoardElementGraphic.WIDTH, BoardElementGraphic.HEIGHT);
        size.height = size.width;
        setPreferredSize(size);
        setBackground(OCEAN);
    }

    @Override
    protected void paintBorder(Graphics g) {
        g.setColor(Color.BLACK);
        super.paintBorder(g);
    }

    @Override
    public Tile getTile() {
        return null;
    }

    @Override
    public Vertex getVertex() {
        return null;
    }

    @Override
    public Edge getEdge() {
        return null;
    }
}
