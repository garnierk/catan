package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;

/**
 * Affichage des arretes sur le plateau
 *
 * @see JButton
 * @see BoardElementGraphic
 * @see Edge
 * @see GameController
 */
public class EdgeGraphic extends JButton
        implements BoardElementGraphic {

    private static boolean active = false;
    private final Edge edge;
    private final GameController controller;
    private Shape shape;

    public EdgeGraphic(Edge edge, GameController controller) {
        this.edge = edge;
        this.controller = controller;
        int width = (edge.isHorizontal()) ? BoardElementGraphic.WIDTH : BoardElementGraphic.HEIGHT;
        int height = (edge.isHorizontal()) ? BoardElementGraphic.WIDTH : BoardElementGraphic.HEIGHT;
        setPreferredSize(new Dimension(width, height));
        this.setFocusable(false);
        this.setBackground(EMPTY_STRUCTURE);
        setContentAreaFilled(false);
        if (controller != null) initAction();
        setUI(new MetalButtonUI() {
            @Override
            protected Color getDisabledTextColor() {
                return Color.WHITE;
            }
        });

    }

    public static void setActive(boolean active) {
        EdgeGraphic.active = active;
    }

    /**
     * Initialise l'action quand le bouton est active
     */
    private void initAction() {
        addActionListener(e -> controller.edgeAction('r', this));
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (edge != null && !edge.isFree()) {
            g.setColor(ID_TO_COLOR.get(edge.getRoad().getPlayerId()));
            setEnabled(false);
        } else {
            g.setColor(EMPTY_STRUCTURE);
            setEnabled(active);
        }
        Dimension size = getSize();
        g.fillRect(0, 0, size.width, size.height);
        super.paintComponent(g);
    }

    @Override
    protected void paintBorder(Graphics g) {
        if (active && edge.isFree()) g.setColor(Color.YELLOW);
        super.paintBorder(g);
    }

    @Override
    public boolean contains(int x, int y) {
        if (shape == null || shape.getBounds().equals(getBounds())) {
            shape = new Rectangle(0, 0, getWidth(), getHeight());
        }
        return shape.contains(x, y);
    }

    @Override
    public Tile getTile() {
        return edge.getCurrent().getTile();
    }

    @Override
    public Vertex getVertex() {
        return edge.getCurrent();
    }

    @Override
    public Edge getEdge() {
        return edge;
    }
}
