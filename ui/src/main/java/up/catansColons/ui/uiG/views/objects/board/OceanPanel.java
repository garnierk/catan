package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

import javax.swing.*;
import java.awt.*;

/**
 * Affichage de l'ocean
 *
 * @see TilePanel
 * @see BoardElementGraphic
 */
public class OceanPanel extends JPanel
        implements BoardElementGraphic {

    public OceanPanel() {
        super();
        this.setBackground(OCEAN);
        int width = TILE_SIZE.width * 3 / 4;
        int height = TILE_SIZE.height * 3 / 4;
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    protected void paintBorder(Graphics g) {
        g.setColor(Color.BLACK);
        super.paintBorder(g);
    }

    @Override
    public Tile getTile() {
        return null;
    }

    @Override
    public Vertex getVertex() {
        return null;
    }

    @Override
    public Edge getEdge() {
        return null;
    }
}
