package up.catansColons.ui.uiG.views.objects.player;

import up.catansColons.board.composant.Port;
import up.catansColons.cards.CarteRessource;
import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.Exchange;
import up.catansColons.ui.controllers.GUIExchangeController;
import up.catansColons.ui.controllers.GameController;
import up.catansColons.ui.uiG.views.objects.RessourcesRelatedGraphics;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ExchangePanel extends JPanel implements RessourcesRelatedGraphics, Exchange {

    private final GUIExchangeController controller;
    private final Joueur player;
    private final Map<Character, Integer> ratio;
    private Map<Character, Integer> out = initInOut();
    private Map<Character, Integer> in = initInOut();
    private int cota;

    public ExchangePanel(GameController controller, JFrame window) {
        this.player = controller.getModel().getCurrent();
        this.controller = new GUIExchangeController(player, this, window);
        ratio = initRatio();
        JPanel exchange = new JPanel();
        JPanel playerExchange = new JPanel();
        for (char c : RessourcesRelatedGraphics.RESSOURCES_NAME.keySet()) {
            exchange.add(new ExchangeRessource(c));
            playerExchange.add(new PlayerRessources(c));
        }
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(exchange);
        add(new CentralPanel());
        add(playerExchange);
    }

    public Map<Character, Integer> getOut() {
        return out;
    }

    public Map<Character, Integer> getIn() {
        return in;
    }

    public int getRatio(char c) {
        return ratio.get(c);
    }

    public void addCota(int n) {
        cota += n;
    }

    public void removeCota(int n) {
        cota -= n;
    }

    @Override
    public Map<Character, Integer> initRatio() {
        Map<Character, Integer> map = new HashMap<>();
        out.forEach((code, ignored) -> {
            Port port = player.getUsefulPort(new CarteRessource(code));
            int n = (port != null) ? port.getRatio() : 4;
            map.put(code, n);
        });
        return map;
    }

    public Map<Character, Integer> initInOut() {
        Map<Character, Integer> map = new HashMap<>();
        RESSOURCES_NAME.forEach((c, ignored) -> map.put(c, 0));
        return map;
    }

    public void reset() {
        cota = 0;
        in = initInOut();
        out = initInOut();
    }

    private class CentralPanel extends JPanel implements RessourcesRelatedGraphics {
        private final JButton accept;
        private final JLabel message;

        private CentralPanel() {
            accept = new JButton("Accepter");
            accept.addActionListener(e -> controller.accept());
            message = new JLabel("");
            add(message);
            add(accept);
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (cota < 0) {
                message.setText("Veuillez ajouter " + Math.abs(cota) + " ressource(s)");
                accept.setVisible(false);
            } else if (cota > 0) {
                message.setText("Vous donnez gracieusement " + cota + " ressources(s)");
                accept.setVisible(true);
            } else {
                message.setText("Echange equitable");
                accept.setVisible(true);
            }
            super.paintComponent(g);
        }
    }

    private class PlayerRessources extends JPanel implements RessourcesRelatedGraphics {
        private final char code;
        private final JLabel name;
        private final JLabel pos;
        private final JLabel neg;

        private PlayerRessources(char code) {
            this.code = code;
            JPanel ressource = new JPanel();
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            name = new JLabel();
            pos = new JLabel();
            neg = new JLabel();
            setContent();
            ressource.setLayout(new BoxLayout(ressource, BoxLayout.LINE_AXIS));
            pos.setForeground(POS);
            neg.setForeground(NEG);
            ressource.add(name);
            ressource.add(pos);
            ressource.add(neg);
            add(ressource);
            add(new PlayerRessourceButton());
            add(new RemoveRessourceButton());
        }

        private void setContent() {
            int i = in.get(code);
            int o = out.get(code);
            name.setText(RESSOURCES_NAME.get(code) + "(" + (player.getRessourceNumber(code) + i - o) + ")");
            if (i > 0) {
                pos.setText(" +" + i);
                pos.setVisible(true);
            } else {
                pos.setVisible(false);
            }
            if (o > 0) {
                neg.setText(" -" + o);
                neg.setVisible(true);
            } else {
                neg.setVisible(false);
            }
            revalidate();
        }

        @Override
        protected void paintComponent(Graphics g) {
            setContent();
            super.paintComponent(g);
        }

        private class PlayerRessourceButton extends JButton {
            private PlayerRessourceButton() {
                super("Ajouter");
                addActionListener(e -> controller.addPlayerRessources(code));
            }
        }

        private class RemoveRessourceButton extends JButton {
            private RemoveRessourceButton() {
                super("Retirer");
                addActionListener(e -> controller.removePlayerRessource(code));
            }

            @Override
            protected void paintComponent(Graphics g) {
                setVisible(true);
                super.paintComponent(g);
            }
        }
    }

    private class ExchangeRessource extends JPanel implements RessourcesRelatedGraphics {
        private final char code;

        private ExchangeRessource(char code) {
            this.code = code;
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            add(new JLabel(RESSOURCES_NAME.get(code)));
            add(new ExchangeRessourceButton());
            add(new RemoveRessourceButton());
        }

        private class ExchangeRessourceButton extends JButton {
            private ExchangeRessourceButton() {
                super("");
                setText("Ajouter (" + ratio.get(code) + " : 1)");
                addActionListener(e -> controller.addExchange(code));
            }
        }

        private class RemoveRessourceButton extends JButton {
            private RemoveRessourceButton() {
                super("Retirer");
                setVisible(true);
                addActionListener(e -> controller.removeExchange(code));
            }

            @Override
            protected void paintComponent(Graphics g) {
                setVisible(true);
                super.paintComponent(g);
            }
        }
    }


}
