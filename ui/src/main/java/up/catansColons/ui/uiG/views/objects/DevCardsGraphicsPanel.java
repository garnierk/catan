package up.catansColons.ui.uiG.views.objects;

import up.catansColons.cards.CarteDeveloppement;
import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class DevCardsGraphicsPanel extends JPanel {
    private final GameController controller;
    private final Joueur player;
    private final DevCardGraphic knight;
    private final DevCardGraphic road;
    private final DevCardGraphic discovery;
    private final JLabel vp;

    public DevCardsGraphicsPanel(Joueur player, GameController controller) {
        this.player = player;
        this.controller = controller;
        setLayout(new GridBagLayout());
        road = new DevCardGraphic('R');
        knight = new DevCardGraphic('C');
        discovery = new DevCardGraphic('D');
        vp = new JLabel("");
        addComp();
    }

    private void addComp() {
        if (player.hasCard(road.code)) {
            add(road);
        } else {
            remove(road);
        }
        if (player.hasCard(knight.code)) {
            add(knight);
        } else {
            remove(knight);
        }
        if (player.hasCard(discovery.code)) {
            add(discovery);
        } else {
            remove(discovery);
        }
        if (player.hasCard('V')) {
            vp.setText(player.getCardNumber('V') + " carte(s) victoire");
            add(vp);
        } else {
            remove(vp);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        addComp();
        super.paintComponent(g);
    }

    private class DevCardGraphic extends JPanel {
        private final char code;

        public DevCardGraphic(char c) {
            code = c;
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            Map<Character, String> TRAD_MAP = CarteDeveloppement.DEV_CARDS_CODE_TO_PRINT_FR;
            add(new JLabel(TRAD_MAP.get(c)));
            add(new useButton(c));
        }
    }

    private class useButton extends JButton {
        private useButton(char c) {
            super("Utiliser");
            addActionListener(e -> controller.useCard(c));
        }
    }
}
