package up.catansColons.ui.uiG.views.objects;

import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ActionButtonPanel extends JPanel {

    private final ActionButton diceButton;
    private final ActionButton exchangeButton;
    private final ActionButton endRoundButton;
    private final ActionButton takeDevCards;
    private final ActionButton useDevCards;
    private final ActionButton moveThief;
    private final ActionButton construction;

    public ActionButtonPanel(GameController controller) {
        setLayout(new GridBagLayout());
        diceButton = new ActionButton(0, controller);
        exchangeButton = new ActionButton(1, controller);
        takeDevCards = new ActionButton(2, controller);
        useDevCards = new ActionButton(3, controller);
        endRoundButton = new ActionButton(4, controller);
        moveThief = new ActionButton(5, controller);
        construction = new ActionButton(6, controller);
        initPanel();
    }

    private void initPanel() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        add(diceButton, constraints);
        constraints.gridx++;
        add(exchangeButton, constraints);
        constraints.gridy++;
        constraints.gridx = 0;
        add(takeDevCards, constraints);
        constraints.gridx++;
        add(useDevCards, constraints);
        constraints.gridy++;
        constraints.gridx = 0;
        add(endRoundButton, constraints);
        constraints.gridx++;
        add(moveThief, constraints);
        constraints.gridy++;
        constraints.gridx = 0;
        add(construction, constraints);
    }

    public ActionButton getDiceButton() {
        return diceButton;
    }

    public ActionButton getExchangeButton() {
        return exchangeButton;
    }

    public ActionButton getEndRoundButton() {
        return endRoundButton;
    }

    public ActionButton getTakeDevCards() {
        return takeDevCards;
    }

    public ActionButton getUseDevCards() {
        return useDevCards;
    }

    public ActionButton getMoveThief() {
        return moveThief;
    }

    public ActionButton getConstruction() {
        return construction;
    }

    public void disableAll() {
        diceButton.setEnabled(false);
        exchangeButton.setEnabled(false);
        endRoundButton.setEnabled(false);
        takeDevCards.setEnabled(false);
        useDevCards.setEnabled(false);
        moveThief.setEnabled(false);
        construction.setEnabled(false);
    }

    public static class ActionButton extends JButton {

        private static final Map<Integer, String> INT_TO_DISPLAY = setIntToDisplay();
        private final GameController controller;

        public ActionButton(int i, Icon icon, GameController controller) {
            super(INT_TO_DISPLAY.get(i), icon);
            this.controller = controller;
            initAction(i);
            setVisible(true);
        }

        public ActionButton(int i, GameController controller) {
            this(i, null, controller);
        }

        private static Map<Integer, String> setIntToDisplay() {
            Map<Integer, String> intToDisplay = new HashMap<>();
            intToDisplay.put(0, "Lancer le dé");
            intToDisplay.put(1, "Échanger");
            intToDisplay.put(2, "Prendre une carte");
            intToDisplay.put(3, "Utiliser une carte");
            intToDisplay.put(4, "Finir le tour");
            intToDisplay.put(5, "Placer le voleur");
            intToDisplay.put(6, "Construire");
            return intToDisplay;
        }

        private void initAction(int i) {
            switch (i) {
                case 0:
                    addActionListener(e -> controller.launchDice());
                    break;
                case 1:
                    addActionListener(e -> controller.exchange());
                    break;
                case 2:
                    addActionListener(e -> controller.takeCard());
                    break;
                case 3:
                    addActionListener(e -> controller.showCards());
                    break;
                case 4:
                    addActionListener(e -> controller.endRound());
                    break;
                case 5:
                    addActionListener(e -> controller.setActiveTilePanel(true));
                    break;
                case 6:
                    addActionListener(e -> controller.showConstructionMenu());
                default:
                    setVisible(false);
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (!isEnabled()) setForeground(new Color(107, 77, 55, 0));
            else setForeground(Color.BLACK);
            super.paintComponent(g);
        }
    }

}
