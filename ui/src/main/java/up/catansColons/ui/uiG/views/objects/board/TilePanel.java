package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;

/**
 * Affichage d'une tuile
 */
public class TilePanel extends JButton
        implements BoardElementGraphic {

    private static boolean active = false;
    private final Tile tile;
    private final ThiefGraphic thiefGraphic;
    private Shape shape;

    public TilePanel(Tile t, GameController controller) {
        this.tile = t;
        this.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        initTile(constraints);
        addActionListener(e -> controller.tileAction(getTile()));
        this.setBackground(RESSOURCE_TO_COLOR.get(tile.getRessource()));
        setPreferredSize(TILE_SIZE);
        thiefGraphic = new ThiefGraphic();
        add(thiefGraphic, constraints);
        setUI(new MetalButtonUI() {
            @Override
            protected Color getDisabledTextColor() {
                return Color.BLACK;
            }
        });
    }

    public static void setActive(boolean active) {
        TilePanel.active = active;
    }

    public void initTile(GridBagConstraints constraints) {
        String output = tile.getRessourceOutput();
        if (!tile.isDesert()) output += " " + tile.getNumber();
        add(new JLabel(output), constraints);
        constraints.gridy++;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Dimension size = getSize();
        g.fillRect(0, 0, size.width, size.height);
        if (!tile.isThiefPresent()) {
            thiefGraphic.setVisible(false);
            setEnabled(active);
        } else {
            thiefGraphic.setVisible(true);
            setEnabled(false);
        }
        super.paintComponent(g);
    }

    @Override
    protected void paintBorder(Graphics g) {
        Dimension size = getSize();
        g.drawRect(0, 0, size.width - 1, size.height - 1);
        if (active && !tile.isThiefPresent()) super.paintBorder(g);
    }

    @Override
    public boolean contains(int x, int y) {
        if (shape == null || !shape.getBounds().equals(getBounds())) {
            shape = new Rectangle(0, 0, getWidth(), getHeight());
        }
        return shape.contains(x, y);
    }

    public Tile getTile() {
        return tile;
    }

    @Override
    public Vertex getVertex() {
        return null;
    }

    @Override
    public Edge getEdge() {
        return null;
    }
}
