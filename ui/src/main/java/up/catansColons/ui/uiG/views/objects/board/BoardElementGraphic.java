package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.Board;
import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.cards.CarteRessource;
import up.catansColons.ui.uiG.views.objects.player.PlayerGraphicsElement;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Regroupe les informations necessaire a l'affichage du plateau
 */
public interface BoardElementGraphic {

    Dimension TILE_SIZE = new Dimension(70, 25);
    int HEIGHT = 45;
    int WIDTH = 20;

    Color EMPTY_STRUCTURE = new Color(248, 230, 205);
    Color ROCK = new Color(145, 147, 142);
    Color FIELD = new Color(198, 173, 119);
    Color DESERT = new Color(236, 196, 124);
    Color OCEAN = new Color(0, 157, 196);
    Color MEADOW = new Color(175, 206, 15);
    Color FOREST = new Color(102, 133, 87);
    Color CLAY = new Color(181, 141, 123);
    /**
     * Indique la couleur associe a l'id du joueur
     */
    Map<Integer, Color> ID_TO_COLOR = PlayerGraphicsElement.PLAYER_COLOR;
    /**
     * Indique la couleur associe a la ressource
     */
    Map<Character, Color> RESSOURCE_TO_COLOR = setRessourceToColor();

    /**
     * Initialise la liaison entre une ressource et sa couleur
     *
     * @return la valeur de RESSOURCE_TO_COLOR
     */
    private static Map<Character, Color> setRessourceToColor() {
        Map<Character, Color> ressourceToColor = new HashMap<>();
        ressourceToColor.put(CarteRessource.WHEAT, FIELD);
        ressourceToColor.put(CarteRessource.WOOD, FOREST);
        ressourceToColor.put(CarteRessource.WOOL, MEADOW);
        ressourceToColor.put(CarteRessource.CLAY, CLAY);
        ressourceToColor.put(CarteRessource.ROCK, ROCK);
        ressourceToColor.put(Board.DESERT, DESERT);
        return ressourceToColor;
    }

    Tile getTile();

    Vertex getVertex();

    Edge getEdge();

}
