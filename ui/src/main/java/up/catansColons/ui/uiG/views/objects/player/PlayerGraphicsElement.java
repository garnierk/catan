package up.catansColons.ui.uiG.views.objects.player;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public interface PlayerGraphicsElement {

    Map<Integer, Color> PLAYER_COLOR = setIdToColor();

    /**
     * Initialise la liaison entre l'id du joueur et sa couleur
     *
     * @return la valeur de ID_TO_COLOR
     */
    private static Map<Integer, Color> setIdToColor() {
        HashMap<Integer, Color> idToColor = new HashMap<>();
        idToColor.put(0, new Color(190, 39, 25));
        idToColor.put(1, new Color(26, 180, 117));
        idToColor.put(2, new Color(60, 104, 180));
        idToColor.put(3, new Color(132, 27, 208));

        return idToColor;
    }
}
