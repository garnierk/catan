package up.catansColons.ui.uiG.views.objects;

import up.catansColons.board.piece.City;
import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Piece;
import up.catansColons.board.piece.Road;
import up.catansColons.player.Joueur;
import up.catansColons.ui.uiG.views.objects.player.PlayerGraphicsElement;

import javax.swing.*;
import java.awt.*;

public class PiecesGraphics extends JPanel implements PlayerGraphicsElement {

    private final Joueur player;
    private final JLabel colonies;
    private final JLabel cities;
    private final JLabel roads;

    public PiecesGraphics(Joueur player, boolean horizontal) {
        this.player = player;
        colonies = new JLabel();
        cities = new JLabel();
        roads = new JLabel();
        if (!horizontal) {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        }
        colonies.setAlignmentX(CENTER_ALIGNMENT);
        colonies.setAlignmentY(CENTER_ALIGNMENT);
        roads.setAlignmentX(CENTER_ALIGNMENT);
        roads.setAlignmentY(CENTER_ALIGNMENT);
        cities.setAlignmentX(CENTER_ALIGNMENT);
        cities.setAlignmentY(CENTER_ALIGNMENT);
        add(Box.createGlue());
        add(colonies);
        add(roads);
        add(cities);
        add(Box.createGlue());
        setBackground(PLAYER_COLOR.get(player.getID()));
        colonies.setForeground(Color.WHITE);
        cities.setForeground(Color.WHITE);
        roads.setForeground(Color.WHITE);
        setContent();
    }

    private int getRoadsNumber() {
        int ret = 0;
        for (Piece p : player.getPieces()) {
            if (p instanceof Road) ret++;
        }
        return ret;
    }

    private int getCitiesNumber() {
        int ret = 0;
        for (Piece p : player.getPieces()) {
            if (p instanceof City) ret++;
        }
        return ret;
    }

    private int getColoniesNumber() {
        int ret = 0;
        for (Piece p : player.getPieces()) {
            if (p.getClass() == Colony.class) ret++;
        }
        return ret;
    }

    private void setContent() {
        colonies.setText(getColoniesNumber() + " colonies");
        cities.setText(getCitiesNumber() + " villes");
        roads.setText(getRoadsNumber() + " routes");
    }

    @Override
    protected void paintComponent(Graphics g) {
        setContent();
        super.paintComponent(g);
    }
}
