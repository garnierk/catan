package up.catansColons.ui.uiG.views.scenes;

import up.catansColons.game.Config;
import up.catansColons.player.Joueur;
import up.catansColons.ui.controllers.SettingsController;
import up.catansColons.ui.models.Model;

import javax.swing.*;
import java.awt.*;

public class SettingsScene extends Scene {

    private final SettingsController controller;
    private final Model model;
    private final JFrame window;
    private JTextField pseudo;
    private JButton send;

    public SettingsScene(JFrame window) {
        model = new Model();
        this.window = window;
        controller = new SettingsController(this, model, window);
        model.setController(controller);
        this.setLayout(new BorderLayout());
        initTop();
        initText();
    }

    public void play() {
        this.send.addActionListener(event -> this.controller.readPseudo());
    }

    public void isAgainstPlayer() {
        this.revalidate();
        initTop();
        JPanel Lobby = new JPanel();
        JButton b1 = new JButton("Jouer Contre 3 IA");
        JButton b2 = new JButton("Jouer Contre 2 IA");
        JButton b3 = new JButton("Jouer Contre des Joueurs");
        Lobby.add(b1);
        Lobby.add(b2);
        Lobby.add(b3);
        this.add(Lobby);
        this.printProfile();
        this.askLoadSave();
        window.pack();
        this.revalidate();
        b1.addActionListener(event -> this.controller.readOpponent1());
        b2.addActionListener(event -> this.controller.readOpponent2());
        b3.addActionListener(event -> this.controller.readOpponent3());

    }

    private void printProfile() {
        JPanel jp = new JPanel();
        jp.setLayout(new GridLayout(30, 30));
        JLabel jl = new JLabel("Profil de " + this.model.getProfil().getPseudo());
        jp.add(jl);
        for (Config c : this.model.getProfil().getConfigSave()) {
            System.out.println(c.getNameConfig());
            JLabel jl2 = new JLabel(c.getNameConfig());
            jp.add(jl2);
        }
        this.add(jp, BorderLayout.EAST);
        window.pack();
        window.setLocationRelativeTo(null);
        this.revalidate();
    }

    private void askLoadSave() {
        JPanel jp = new JPanel();
        jp.setLayout(new FlowLayout());
        JLabel jl = new JLabel("Nom de la configuration a charger");
        jp.add(jl);
        JTextField saisieNom = new JTextField(10);
        jp.add(saisieNom, BorderLayout.CENTER);
        JButton jb = new JButton("Play with config ");
        jp.add(jb);
        this.add(jp, BorderLayout.SOUTH);
        window.pack();
        window.setLocationRelativeTo(null);
        this.revalidate();
        jb.addActionListener(event -> this.controller.readLoad(saisieNom));
    }

    private int numPlayer(JPanel Config, JComboBox<String> nb, JButton jb) {
        jb.addActionListener(event -> jb.setEnabled(false));
        return this.controller.readIntFromMenu(nb);
    }

    private void initTop() {
        JPanel jp = new JPanel();
        JLabel jl = new JLabel("Colon de Catane");
        jp.add(jl);
        this.add(jp, BorderLayout.PAGE_START);
        window.pack();
        window.setLocationRelativeTo(null);
    }

    public void askConfig(int nbPlayer) {
        this.revalidate();
        initTop();
        JPanel Config = new JPanel();
        Config.setLayout(new BoxLayout(Config, BoxLayout.PAGE_AXIS));
        JLabel j = new JLabel("Combien de Joueur dans la Partie ?");
        Config.add(j);
        String[] number = {"3", "4"};
        JComboBox<String> nb = new JComboBox<String>(number);
        Config.add(nb);
        JButton jb = new JButton("Envoyer !");
        Config.add(jb);
        if (nbPlayer == 1) {
            nbPlayer = this.numPlayer(Config, nb, jb);
        }
        int i = nbPlayer - 1;
        JButton next = new JButton("Start Game ! ");
        while (i != 0) {
            JLabel lab = new JLabel("Joueur " + (nbPlayer - i));
            Config.add(lab);
            JLabel j2 = new JLabel("Quel nom ?");
            Config.add(j2);
            JTextField saisieNom = new JTextField(10);
            Config.add(saisieNom, BorderLayout.CENTER);
            JButton jb2 = new JButton("Envoyer !");
            Config.add(jb2);
            jb.addActionListener(event -> {
                int newNum = this.numPlayer(Config, nb, jb);
                this.removeAll();
                this.askConfig(newNum);
                jb.setEnabled(false);
            });
            jb2.addActionListener(event -> {
                String name = this.controller.readString(saisieNom);
                this.model.getConfig().setName(name);
                jb2.setEnabled(false);
                this.model.addPlayer(name);
            });
            if (!this.model.getConfig().estComplete()) {
                Config.add(next);
                i--;
                if (i != 0) {
                    Config.remove(next);
                }
            }
        }
        this.askSave(Config);
        JButton back = new JButton("back");
        Config.add(back);
        this.add(Config, BorderLayout.CENTER);
        window.pack();
        window.setLocationRelativeTo(null);
        this.revalidate();
        back.addActionListener(event -> {
            this.removeAll();
            this.isAgainstPlayer();
        });
        next.addActionListener(event -> this.controller.readGame());
    }

    public void afficheStart() {
        initTop();
        JPanel jp = new JPanel();
        JLabel jl = new JLabel("Debut de la Partie !");
        jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
        jp.add(jl, BorderLayout.CENTER);
        for (Config c : this.model.getProfil().getConfigSave()) {
            JLabel jl2 = new JLabel(c.getNameConfig());
            jp.add(jl2);
        }
        for (Joueur j : this.model.getConfig().getPlayerList()) {
            JLabel jl2 = new JLabel(j.getPseudo());
            jp.add(jl2);
        }
        JButton jb = new JButton("exit");
        jp.add(jb);
        this.add(jp);
        window.pack();
        window.setLocationRelativeTo(null);
        this.revalidate();
        jb.addActionListener(event -> {
            this.removeAll();
            this.isAgainstPlayer();
        });
    }

    private void askSave(JPanel Config) {
        JCheckBox save = new JCheckBox("Sauvegarder la configuration !");
        Config.add(save);
        this.revalidate();
        boolean select = save.isSelected();
        System.out.println(select);
        JButton jb = new JButton("Envoyer !");
        JTextField saisieNom = new JTextField(10);
        save.addActionListener(event -> {
            if (save.isSelected()) {
                Config.add(saisieNom, BorderLayout.CENTER);
                Config.add(jb);
            }
            save.setSelected(true);
            this.revalidate();
            window.pack();
            window.setLocationRelativeTo(null);
        });
        jb.addActionListener(event3 -> {
            this.controller.saveConfig(saisieNom);
            jb.setEnabled(false);
            JLabel jl = new JLabel("Configuration sauvegarder !");
            Config.add(jl);
            this.revalidate();
            window.pack();
            window.setLocationRelativeTo(null);
        });
    }

    private void initText() {
        JPanel Pseudo = new JPanel();
        JLabel j = new JLabel("Saisissez votre pseudo !");
        Pseudo.setLayout(new BoxLayout(Pseudo, BoxLayout.PAGE_AXIS));
        Pseudo.add(j, BorderLayout.CENTER);
        JButton b = new JButton("Envoyer !");
        b.addActionListener(e -> controller.readPseudo());
        JTextField saisieTaille = new JTextField(10);
        Pseudo.add(saisieTaille, BorderLayout.EAST);
        Pseudo.setAlignmentX(CENTER_ALIGNMENT);
        Pseudo.add(b, BorderLayout.EAST);
        this.send = b;
        this.pseudo = saisieTaille;
        this.add(Pseudo, BorderLayout.CENTER);
        window.pack();
        window.setLocationRelativeTo(null);
        this.revalidate();
    }

    public JTextField getPseudo() {
        return this.pseudo;
    }

}
