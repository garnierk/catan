package up.catansColons.ui.uiG.views.objects.board;

import up.catansColons.board.Board;
import up.catansColons.board.composant.Port;
import up.catansColons.board.composant.Tile;
import up.catansColons.ui.controllers.GameController;

import javax.swing.*;
import java.awt.*;

/**
 * Affichage du plateau de jeu sur l'interface
 */
public class BoardPanel extends JPanel {
    private static final int DEFAULT_GRIDX = 1;
    private static final int DEFAULT_GRIDY = 1;
    private final GameController controller;

    public BoardPanel(Board board, GameController gameController) {

        GridBagConstraints constraints = new GridBagConstraints();
        this.setLayout(new GridBagLayout());
        constraints.fill = GridBagConstraints.BOTH;
        this.controller = gameController;
        initBoard(board.getTiles(), constraints);

    }

    /**
     * Initialise l'affichage en fonction du plateau
     *
     * @param tiles       represente le plateau de jeu
     * @param constraints organise l'affichage
     */
    private void initBoard(Tile[][] tiles, GridBagConstraints constraints) {
        constraints.gridx = 0;
        constraints.gridy = 0;
        buildOceanTiles(tiles[0].length + 2, constraints);
        constraints.gridy = DEFAULT_GRIDY;
        for (Tile[] tileLine : tiles) {
            constraints.gridx = 0;
            add(new OceanEdge(true), constraints);
            buildLine(tileLine, constraints, 0);
            constraints.gridx++;
            add(new OceanEdge(true), constraints);
            constraints.gridy++;
            constraints.gridx = 0;
            add((new OceanPanel()), constraints);
            buildTiles(tileLine, constraints);
            constraints.gridx++;
            add((new OceanPanel()), constraints);
            constraints.gridy++;
        }
        constraints.gridx = 0;
        add(new OceanEdge(true), constraints);
        constraints.gridx++;
        buildLine(tiles[tiles.length - 1], constraints, 1);
        constraints.gridx++;
        add(new OceanEdge(true), constraints);
        constraints.gridy++;
        buildOceanTiles(tiles[tiles.length - 1].length + 2, constraints);
    }

    /**
     * Construit une ligne du plateau compose d'une arrete, d'une tuile et d'une arrete
     *
     * @param tileLine    represente la ligne a construire
     * @param constraints represente la disposition de l'affichage
     */
    private void buildTiles(Tile[] tileLine, GridBagConstraints constraints) {
        constraints.gridx = DEFAULT_GRIDX;
        for (Tile t : tileLine) {
            EdgeGraphic e = new EdgeGraphic(t.getVertex(0, 0).getEdge(t.getVertex(1, 0)), controller);
            add(e, constraints);
            constraints.gridx++;
            TilePanel tilePanel = (t instanceof Port) ? new PortPanel(t, controller) : new TilePanel(t, controller);
            add(tilePanel, constraints);
            constraints.gridx++;
        }
        Tile lastTile = tileLine[tileLine.length - 1];
        EdgeGraphic e = new EdgeGraphic(lastTile.getVertex(0, 1).getEdge(lastTile.getVertex(1, 1)), controller);
        add(e, constraints);
    }

    /**
     * Construit une ligne du plateau compose d'un coin, d'une arrete et d'un coin
     *
     * @param tileLine    represente la ligne a construire
     * @param constraints represente la disposition de l'affichage
     * @param pos         represente la position verticale du coin  a afficher
     */
    private void buildLine(Tile[] tileLine, GridBagConstraints constraints, int pos) {
        constraints.gridx = DEFAULT_GRIDX;
        for (Tile t : tileLine) {
            add(new VertexGraphic(t.getVertex(pos, 0), controller), constraints);
            constraints.gridx++;
            EdgeGraphic e = new EdgeGraphic(t.getVertex(pos, 0).getEdge(t.getVertex(pos, 1)), controller);
            add(e, constraints);
            constraints.gridx++;
        }
        VertexGraphic v = new VertexGraphic(tileLine[tileLine.length - 1].getVertex(pos, 1), controller);
        add(v, constraints);
    }

    /**
     * Constuit les tuiles oceans
     *
     * @param l           represente la quantite de tuiles a creer
     * @param constraints sert a la disposition de tuiles
     */
    private void buildOceanTiles(int l, GridBagConstraints constraints) {
        constraints.gridx = 0;
        for (int i = 0; i < l; i++) {
            add(new OceanPanel(), constraints);
            constraints.gridx++;
            if (i != l - 1) {
                add(new OceanEdge(false), constraints);
                constraints.gridx++;
            }
        }
        constraints.gridx = 0;
    }
}
