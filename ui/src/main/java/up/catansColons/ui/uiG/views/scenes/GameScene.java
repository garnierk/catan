package up.catansColons.ui.uiG.views.scenes;

import up.catansColons.ui.controllers.GameController;
import up.catansColons.ui.models.Model;
import up.catansColons.ui.uiG.views.objects.*;
import up.catansColons.ui.uiG.views.objects.board.BoardPanel;
import up.catansColons.ui.uiG.views.objects.player.AllPlayerPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Affichage de l'environnement de jeu
 */
public class GameScene extends Scene {

    private final GameController controller;
    private final Model model;

    public GameScene(JFrame window, Model model) {
        this.model = model;
        controller = new GameController(this, model, window);
        model.setController(controller);
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        initLeftSide();
        initCenter();
        initRightSide();
    }

    private void initCenter() {
        JPanel central = new JPanel();
        central.setLayout(new BoxLayout(central, BoxLayout.PAGE_AXIS));
        central.add(new AllPlayerPanel(model.getConfig().getPlayerList()));
        central.add(initBoard());
        central.add(new PlayerRessourcesPanel(model));
        add(central);
    }

    private void initRightSide() {
        ActionButtonPanel actionButtonPanel = new ActionButtonPanel(controller);
        controller.setActionButtonPanel(actionButtonPanel);
        add(actionButtonPanel);
    }

    private void initLeftSide() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        JPanel dice = new DiceGraphics(model);
        JPanel cost = new CostCard();
        panel.add(Box.createGlue());
        panel.add(dice);
        panel.add(cost);
        add(panel);
    }

    private JPanel initBoard() {
        JPanel board = new JPanel();
        board.setLayout(new BorderLayout());
        int n = model.getConfig().getNbJoueur();
        if (n > 2) {
            board.add(new PiecesGraphics(model.getConfig().getPlayerList().get(2), true), BorderLayout.PAGE_START);
        }
        if (n > 3) {
            board.add(new PiecesGraphics(model.getConfig().getPlayerList().get(3), false), BorderLayout.LINE_END);
        }
        board.add(new BoardPanel(model.getBoard(), controller), BorderLayout.CENTER);
        if (n > 1) {
            board.add(new PiecesGraphics(model.getConfig().getPlayerList().get(1), false), BorderLayout.LINE_START);
        }
        if (n > 0) {
            board.add(new PiecesGraphics(model.getConfig().getPlayerList().get(0), true), BorderLayout.PAGE_END);
        }
        return board;
    }

    public void play() {
        model.playFirstRound();
    }
}
