plugins {
    `java-library`
}
version = "0.0.1"
group = "up"

dependencies {
    implementation(project(":game"))
    implementation(project(":board"))
    implementation(project(":cards"))
    implementation(project(":player"))
    implementation("junit:junit:4.13.2")
}
