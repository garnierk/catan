package up.catansColons.cards;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CarteRessource extends Carte {
    public static final char WHEAT = 'b';
    public static final char WOOD = 'w';
    public static final char WOOL = 'l';
    public static final char CLAY = 'a';
    public static final char ROCK = 'p';
    /**
     * Repertoire de liaison entre la notation interne au code et la sortie utilisateur
     */
    public static final Map<Character, String> RESSOURCE_CODE_TO_PRINT_FR = setRessourceCodeToPrintFr();
    /**
     * Represente le code de la ressource
     */
    private final char ressource; // Ble, Pierre, Argile, Bois, Mouton

    public CarteRessource(char ressource) {
        this.ressource = ressource;
    }

    /**
     * Initailise la map de traduction entre la notation interne au code et la sortie utilisateur
     *
     * @return la valeur de CODE_TO_PRINT_FR
     */
    private static Map<Character, String> setRessourceCodeToPrintFr() {
        HashMap<Character, String> codeToPrint = new HashMap<>();
        codeToPrint.put(WOOL, "Laine");
        codeToPrint.put(WHEAT, "Ble");
        codeToPrint.put(WOOD, "Bois");
        codeToPrint.put(ROCK, "Pierre");
        codeToPrint.put(CLAY, "Argile");

        return codeToPrint;
    }

    public void affiche() {
        System.out.print(RESSOURCE_CODE_TO_PRINT_FR.get(this.ressource));
    }

    public char getRessource() {
        return ressource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarteRessource)) return false;
        CarteRessource that = (CarteRessource) o;
        return getRessource() == that.getRessource();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRessource());
    }
}