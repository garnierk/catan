package up.catansColons.cards;

import java.util.*;

public class CarteDeveloppement extends Carte {

    /**
     * Map de traduction
     */
    public static final Map<Character, String> DEV_CARDS_CODE_TO_PRINT_FR = setDevCardsCodeToPrintFr();
    /**
     * Nombre de cartes dans la pile
     */
    private static final int DEV_CARDS_NUMBER = 25;
    /**
     * Nombre de carte similaire dans le jeu
     */
    private static Map<Character, Integer> devCardSpecificNumber;
    /**
     * Pile de jeu
     */
    private static LinkedList<CarteDeveloppement> devCardStack;
    /**
     * charactere servant au code interne
     */
    private final char code;
    /**
     * Renseigne si la carte est une carte progres
     */
    private final boolean progress;

    public CarteDeveloppement(char code) {
        this.code = code;
        this.progress = (code == 'R' || code == 'D');
    }

    /**
     * Initialise le nombre de chaque type de carte
     *
     * @return la valeur de DEV_CARD_SPECIFIC_NUMBER
     */
    private static Map<Character, Integer> setDevCardSpecificNumber() {
        Map<Character, Integer> specificNumber = new HashMap<>();
        specificNumber.put('C', 14);
        specificNumber.put('V', 5);
        specificNumber.put('R', 3);
        specificNumber.put('D', 3);
        return specificNumber;
    }

    /**
     * Initialise la pile de carte
     */
    public static void initDevCardStack() {
        devCardStack = new LinkedList<>();
        devCardSpecificNumber = setDevCardSpecificNumber();
        Random rd = new Random();
        while (devCardStack.size() < DEV_CARDS_NUMBER) {
            int rand = rd.nextInt(devCardSpecificNumber.size());
            for (char c : devCardSpecificNumber.keySet()) {
                if (rand <= 0) {
                    var card = setDevCard(c);
                    if (card != null) devCardStack.add(card);
                } else rand--;
            }
        }
    }

    public static boolean isEmpty() {
        return devCardStack.isEmpty();
    }

    /**
     * Prend une carte de la pile
     *
     * @return la carte, null si la pile est vide
     */
    public static CarteDeveloppement takeCard() {
        if (devCardStack.size() == 0) return null;
        return devCardStack.removeFirst();
    }

    private static CarteDeveloppement setDevCard(char c) {
        if (devCardSpecificNumber.get(c) <= 0) return null;
        else devCardSpecificNumber.computeIfPresent(c, (d, i) -> i - 1);
        return new CarteDeveloppement(c);
    }

    /**
     * Initialise la map de traduction
     *
     * @return la valeur de DEV_CARDS_CODE_TO_PRINT_FR
     */
    private static Map<Character, String> setDevCardsCodeToPrintFr() {
        HashMap<Character, String> codeToPrintFr = new HashMap<>();
        codeToPrintFr.put('C', "Chevalier");
        codeToPrintFr.put('V', "Point de victoire");
        codeToPrintFr.put('R', "Construction de route");
        codeToPrintFr.put('D', "Decouverte");

        return codeToPrintFr;
    }

    /**
     * Replace une carte dans la pile si ce n'est pas une carte progres
     */
    public void replace() {
        if (!this.progress) {
            CarteDeveloppement.devCardStack.add(this);
        }
    }

    public boolean isVictoryPoint() {
        return code == 'V';
    }

    public void affiche() {
        System.out.print(DEV_CARDS_CODE_TO_PRINT_FR.get(code));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarteDeveloppement)) return false;
        CarteDeveloppement that = (CarteDeveloppement) o;
        return code == that.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public char getCode() {
        return code;
    }
}
