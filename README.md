## Usage

### Building the project

1. clone the repository
    ```
    git clone git@gaufre.informatique.univ-paris-diderot.fr:garnierk/colons-de-catane.git
    ```
   or
    ```
    git clone https://gaufre.informatique.univ-paris-diderot.fr/garnierk/colons-de-catane.git
    ```
2. Enter the project folder
    ```
    cd colons-de-catanne
    ```
3. Only if you are on a SCRIPT computer (in one of the TP rooms):
    ```
    source SCRIPT/envsetup
    ```
   This will set up the GRADLE_OPTS environment variable so that gradle uses the SCRIPT proxy for downloading its
   dependencies. It will also use a custom trust store (the one installed in the system is apparently broken... ).
4. run gradle wrapper (it will download all dependencies, including gradle itself)
    ```
    ./gradlew build
    ```

### Running the software

Currently, it can be run through gradle too. In order to pass program arguments, you need to pass them behind `--args`:
* launch the application with the graphic user interface
```
./gradlew run 
```
* launch the application with the textual user interface
```
./gradlew run --args='--text-Only' 
```

