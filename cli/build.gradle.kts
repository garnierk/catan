plugins {
    java
    application
}

application {
    mainClass.set("up.catansColons.cli.CLILaucher")
}

dependencies {
    implementation(project(":ui"))
    implementation(project(":board"))
}

tasks.getByName("run", JavaExec::class) {
    standardInput = System.`in`
}
