package up.catansColons.cli;

import up.catansColons.ui.uiG.CatansColonsLauncher;
import up.catansColons.ui.uiT.TextLauncher;

/**
 * Point d'entree du programme
 */
public class CLILaucher {

    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                new CatansColonsLauncher();
                return;
            }
            for (var arg : args) {
                if (arg == null) dislayHelpAndExit();
                if (arg.startsWith("--")) {
                    switch (arg) {
                        case "--h":
                        case "--HELP":
                        case "--Help":
                        case "--help":
                        case "--displayHelp":
                            dislayHelpAndExit();
                            break;
                        case "--text-Only":
                            new TextLauncher();
                            return;
                        default:
                            System.out.println("UNKNOWN PLUGIN");
                            dislayHelpAndExit();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche l'aide et quite le programme
     */
    private static void dislayHelpAndExit() {
        System.out.println("---Manual--- \n" +
                "./gradlew run - run the graphic version of The Settlers of Catan. \n" +
                "./gradlew run --args='--text-Only' - run the textual version of The Settlers of Catan");
        System.out.println("This game was made by \n" +
                "Garnier Kevin \n" +
                "Ripasarti Adriano \n \n" +
                "Thank you for playing. The game will now terminate ");
        System.exit(0);

    }
}
