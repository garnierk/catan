package up.catansColons.game;

import up.catansColons.player.Joueur;

import java.util.List;
import java.util.LinkedList;
import java.lang.Cloneable;

import up.catansColons.player.*;

import java.util.Map;

public class Config implements Cloneable {

    private final List<Joueur> playerList; /* Correspond a la liste des joueurs de la partie. */
    private int nbJoueur;
    private String nameConfig; /* nom de la configuration. */
    private Joueur Owner; // Le joueur auquel la configuration appartient.

    public Config(Joueur j, String nameConfig) {
        this.Owner = j;
        this.nameConfig = nameConfig;
        this.playerList = new LinkedList<Joueur>();
        //playerList.add(j);
        //this.IAList = new LinkedList<IA>();
    }

    public Config() {
        this(null, null);
    }

    public void ajouteJoueur(String name) { /* Ajoute un Joueur dans la configuration de la partie. */
        playerList.add(new Joueur(name));
        nbJoueur++;
    }

    public void ajouteIA(String name) {
        playerList.add(new IA(name));
        nbJoueur++;
    }

    public void addIA() {
        playerList.add(new IA());
        nbJoueur++;
    }

    public Config clone() {
        Config newConfig = new Config(null, this.nameConfig);
        for (Joueur j : this.playerList) {
            newConfig.ajouteJoueur(j.getPseudo());
        }
        return newConfig;
    }

    public void setName(String newName) { /* definit le nom de la configuration. */
        this.nameConfig = newName;
    }

    public boolean estComplete() {
        if (this.playerList.size() == 3) {
            this.nbJoueur = 3;
            return true;
        }
        if (this.playerList.size() == 4) {
            this.nbJoueur = 4;
            return true;
        }
        return false;
    }

    public void affiche() {
        for (Joueur j : this.playerList) {
            j.affiche();
        }
    }

    public List<Joueur> getPlayerList() {
        return this.playerList;
    }

    public int getNbJoueur() {
        return nbJoueur;
    }

    public String getNameConfig() {
        return this.nameConfig;
    }

    public Joueur getOwner() {
    	return this.Owner;
    }
    
    public void setnbJoueur(int nbJoueur) {
    	this.nbJoueur = nbJoueur;
    }

}
