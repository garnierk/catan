package up.catansColons.game;

import java.util.LinkedList;
import java.util.List;

public class Game {

    private List<Config> configSave; /* Configuration sauvegardee. */
    private String Pseudo;

    public Game(String Pseudo) {
        this.configSave = new LinkedList<Config>();
        this.Pseudo = Pseudo;

    }

    public List<Config> getConfigSave() {
        return configSave;
    }

    public void addSave(Config c) {
        this.configSave.add(c);
    }
    
    public String getPseudo() {
        return Pseudo;
    }

}
