package up.catansColons.game;

import up.catansColons.board.Board;
import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Road;
import up.catansColons.player.Joueur;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.board.composant.Edge;

public class Partie {

    public Joueur vainqueur;
    public Config gameConfig; /* Configuration avec laquelle la partie a ete lancee. */
    public Board board; /* Plateau sur lequel se deroulera la partie. */

    public Partie(Config gameConfig) {
        this.board = new Board(4, 4);
        this.gameConfig = gameConfig;

		/*for(Joueur j : gameConfig.playerList) {
			j.playFirstRound(this.board);
		}
		for(Joueur j : gameConfig.playerList) {
			j.playSecondRound(board);
		}
		int de = Joueur.lanceDe();
		for(Joueur j : gameConfig.playerList) {
			j.playRound(board, de);
		}*/
    }

    public void playFirstRound(Joueur j, Tile t, Vertex v, Edge e) {
        Colony c = new Colony(j.getID());
        c.place(t, v, e);
        Road r = new Road(j.getID());
        r.place(t, v, e);
    }

    public Board getBoard() {
        return board;
    }

    /*public void affichePlateau() {
		
	}*/

}
