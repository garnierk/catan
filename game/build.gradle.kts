plugins{
    `java-library`
}

dependencies {
    implementation(project(":player"))
    implementation (project(":board"))
}