package up.catansColons.player;

import org.junit.BeforeClass;
import org.junit.Test;
import up.catansColons.board.Board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe test de plusieurs IA fonctionnant en meme temps
 *
 * @see IA
 */
public class TestMultipleIA {
    private static Board board;
    private static Collection<IA> ias;

    @BeforeClass
    public static void setup() {
        board = new Board(5, 5);
        ias = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            IA ia = new IA("IA" + i);
            ia.initPiece();
            ias.add(ia);
        }
    }

    @Test
    public void stressTest() {
        AtomicInteger occ = new AtomicInteger();
        for (int i = 0; i < 2; i++) {
            ias.forEach(ia -> {
                occ.getAndIncrement();
                //ia.playFirstRound(board);
                System.out.println(occ);
            });
        }
    }
}
