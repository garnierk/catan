package up.catansColons.player;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Road;

public class TestPlayer {

    private static Joueur joueur;

    @BeforeClass
    public static void setup() {
        joueur = new Joueur("j");
        joueur.initPiece();
    }

    @AfterClass
    public static void clean() {
        joueur = null;
    }

    @Test
    public void getUnplacedColony() {
        for (int i = 0; i < 5; i++) {
            Colony c = joueur.getUnplacedColony();
            Assert.assertFalse(c.isPlaced());
            joueur.pieces.remove(c);
        }
        Assert.assertNull(joueur.getUnplacedColony());
    }

    @Test
    public void getUnplacedRoad() {
        for (int i = 0; i < 15; i++) {
            Road r = joueur.getUnplacedRoad();
            Assert.assertFalse(r.isPlaced());
            joueur.pieces.remove(r);
        }
        Assert.assertNull(joueur.getUnplacedRoad());
    }
}

