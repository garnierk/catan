package up.catansColons.player;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import up.catansColons.board.Board;
import up.catansColons.board.piece.Piece;

/**
 * Classe Test de l'IA aleatoire
 *
 * @see IA
 */
public class TestIA {

    private static Board board;
    private static IA ia;
    private static int piecesNumber;

    @BeforeClass
    public static void setup() {
        board = new Board(5, 5);
        ia = new IA("ia");
        ia.initPiece();
        piecesNumber = ia.getPieces().size();
    }

    @AfterClass
    public static void clean() {
        board = null;
        ia = null;
    }

    /**
     * Teste la fonction random
     */
    @Test
    public void random() {
        for (int i = 0; i < 100; i++) {
            IA ia = new IA("IA");
            int rd = ia.random(2, 13);
            Assert.assertTrue(2 <= rd && rd <= 12);
        }
    }

    /**
     * Teste les fonctions playFirstRound et ploaySecondRound
     */
    @Test
    public void placeThings() {
        playFirstRound();
        playSecondRound();
        playRoundWithoutRessources();
        for (int i = 0; i < 15; i++) {
            tryToBuildRoad();
            tryToBuildColony();
            tryToBuildCity();
        }
        Assert.assertEquals(0, ia.getPieces().size());
    }

    private void playFirstRound() {
        ia.playFirstRound(board);
        Assert.assertEquals(0, getModifiedPieceNumber());
        Assert.assertEquals(piecesNumber - 2, ia.getPieces().size());
        piecesNumber -= 2;

    }

    private void playSecondRound() {
        ia.playSecondRound(board);
        Assert.assertEquals(0, getModifiedPieceNumber());
        Assert.assertFalse(ia.carteRessources.isEmpty());
        Assert.assertEquals(piecesNumber - 2, ia.getPieces().size());
        piecesNumber -= 2;
    }

    private void tryToBuildColony() {
        ia.clearRessources();
        ia.prendreRessource('a', 1);
        ia.prendreRessource('w', 1);
        ia.prendreRessource('b', 1);
        ia.prendreRessource('l', 1);
        ia.constructRandomThings();
        Assert.assertEquals(0, getModifiedPieceNumber());
    }

    private void tryToBuildRoad() {
        ia.clearRessources();
        ia.prendreRessource('a', 1);
        ia.prendreRessource('w', 1);
        ia.constructRandomThings();
        Assert.assertEquals(0, getModifiedPieceNumber());
    }

    private void tryToBuildCity() {
        ia.clearRessources();
        ia.prendreRessource('b', 2);
        ia.prendreRessource('p', 2);
        ia.constructRandomThings();
        Assert.assertEquals(0, getModifiedPieceNumber());
    }

    private void playRoundWithoutRessources() {
        ia.playRound(board, 0);
        Assert.assertEquals(0, getModifiedPieceNumber());
    }

    /**
     * Donne le nombre de pieces modifies
     *
     * @return le nombre de pieces modifiees
     */
    private int getModifiedPieceNumber() {
        int val = 0;
        for (Piece p : ia.pieces) {
            if (p.isPlaced()) val++;
        }
        return val;
    }
}
