package up.catansColons.player;

import up.catansColons.board.Board;
import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Port;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Placeable;
import up.catansColons.cards.CarteDeveloppement;
import up.catansColons.cards.CarteRessource;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Classe represantant une IA basee sur l'aleatoire. L'IA est capable de jouer des coups legaux sans se bloquer.
 *
 * @see up.catansColons.player.Joueur
 */

public class IA extends Joueur {

    /**
     * Represente les pieces l'ensemblle des pieces du debut de la partie
     */
    private int originalPiecesNumber;

    public IA(String pseudo) {
        super(pseudo);
    }

    public IA() {
        this("IA");
        pseudo += getID();
    }

    /**
     * Genere un nombre aleatoire compris entre start (inclus) et bound (exclus).
     *
     * @param start Represente la borne inferieure fermee de l'intervalle des valeurs possibles.
     * @param bound Represente la borne superieure ouverte de l'intervalle des valeurs possibles.
     * @return Un entier aleatoire strictement compris dans l'intervalle [start,bound[.
     */
    int random(int start, int bound) {
        return (int) (Math.random() * (bound - start)) + start;
    }

    @Override
    public void initPiece() {
        super.initPiece();
        this.originalPiecesNumber = this.pieces.size();

    }

    /**
     * Joue le premier tour selon les regles de jeu. Ici l'IA decide aleatoirement sur quel coin et arrete, elle peut
     * poser ses pieces.
     *
     * @param board Represente le tableau de jeu.
     */
    @Override
    public void playFirstRound(Board board) { //Joue le premier Tour
        int tilesNumber = (board.getTiles().length * board.getTiles()[0].length) * 2;
        boolean isPlaceable = false;
        int occ = 0;
        do {
            occ++;
            if (occ == tilesNumber) {
                return;
            }
            Tile t = getRandomTile(board);
            if (t != null) {
                Vertex v = getFreeVertex(t);
                if (placeFirstColony(t, v)) {
                    Edge e = getRandomFriendlyEdge(v);
                    if (placeFirstRoad(t, v, e)) {
                        this.pieces.remove(e.getRoad());
                        this.placedPieces.add(e.getRoad());
                        isPlaceable = true;
                    } else {
                        remove(v);
                    }
                }
            }

        } while (!isPlaceable);
    }


    /**
     * Joue le premier tour selon les regles de jeu. Ici l'IA decide aleatoirement sur quel coin et arrete, elle peut
     * poser ses pieces.
     *
     * @param board Represente le tableau de jeu.
     */
    @Override
    public void playSecondRound(Board board) {
        int tilesNumber = (board.getTiles().length * board.getTiles()[0].length) * 2;
        boolean isPlaceable = false;
        int occ = 0;
        do {
            occ++;
            if (occ >= tilesNumber) {
                return;
            }
            Tile t = getRandomTile(board);
            if (t != null) {
                Vertex v = getFreeVertex(t);
                if (placeFirstColony(t, v)) {
                    Edge e = getRandomFriendlyEdge(v);
                    if (placeFirstRoad(t, v, e)) {
                        harvest(v.getNextTiles());
                        isPlaceable = true;
                    } else {
                        remove(v);
                    }
                }
            }
        } while (!isPlaceable);
    }

    /**
     * Choisit une tuile aleatoirement sur le plateau.
     *
     * @param board represente le plateau de jeu.
     * @return une tuile aleatoire du plateau qui n'est pas le desert.
     */

    private Tile getRandomTile(Board board) {
        Tile tile;
        int occ = 0;
        int tilesNumber = (board.getTiles().length * board.getTiles()[0].length) * 2;
        do {
            occ++;
            if (occ >= tilesNumber) return null;
            int x = random(0, board.getTiles().length);
            int y = random(0, board.getTiles()[0].length);
            tile = board.getTiles()[x][y];
        } while (tile == null || tile.isDesert());
        return tile;
    }

    /**
     * Choisit un coin aleatoire sur le plateau.
     *
     * @param t represente la tuile auquel le coin appartiendra.
     * @return un coin aleatoirement appartenant a la tuile t.
     */
    private Vertex getRandomVertex(Tile t) {
        int x = random(0, 2);
        int y = random(0, 2);
        return t.getVertex(x, y);
    }

    /**
     * Choisit un coin libre du plateau
     *
     * @param t represente la tuile auquel le coin appartiendra.
     * @return un coin libre aleatoirement appartenant a la tuile t.
     */
    private Vertex getFreeVertex(Tile t) {
        if (t == null) return null;
        for (int i = 0; i < 10; i++) {
            Vertex v = getRandomVertex(t);
            if (v.isFreeToPlace()) return v;
        }
        return null;
    }

    /**
     * Choisit un coin libre mais aussi proche d'une installation alliee aleatoirement sur le plateau.
     *
     * @param t represente la tuile auquel le coin appartiendra.
     * @return un coin appartenant a la tuile t.
     */
    private Vertex getRandomFriendlyVertex(Tile t) {
        if (t == null) return null;
        for (int i = 0; i < 10; i++) {
            Vertex v = getRandomVertex(t);
            if (v.isFreeToPlace() && v.nextToFriendlyRoad(this.ID))
                return v;
        }
        return null;
    }

    /**
     * Chosit une arrete libre et a proximite d'une installation alliee aleatoirement sur le plateau.
     *
     * @param colony Represente la colonie a proximite.
     * @return une telle arrete, null sinon.
     */
    private Edge getRandomFriendlyEdge(Colony colony) {
        if (colony == null) return null;
        return getRandomFriendlyEdge(colony.getVertex());
    }

    /**
     * Chosit une arrete libre et a proximite d'une installation alliee aleatoirement sur le plateau.
     *
     * @param vertex Represente le coin a proximite.
     * @return une telle arrete, null sinon.
     */
    private Edge getRandomFriendlyEdge(Vertex vertex) {
        if (vertex == null) return null;
        for (Edge edge : vertex.getEdges()) {
            if ((edge.isFreeToPlace(this.ID))) return edge;
        }
        return null;
    }


    /**
     * Construit aleatoirement une piece.
     */
    void constructRandomThings() {
        Set<Character> possibleConstruction = isAbleToConstruct();
        for (int i = 0; i < possibleConstruction.size() * 2; i++) {
            int n = random(0, possibleConstruction.size() * 2) % possibleConstruction.size();
            for (char c : possibleConstruction) {
                if (n <= 0) {
                    tryToBuild(c);
                    return;
                }
                n--;
            }
        }
    }

    /**
     * Essaye de creer la piece choisit aleatoirement.
     *
     * @param c represente la piece choisie.
     */
    private void tryToBuild(char c) {
        if (c == 't') {
            tryToBuildCity(c, 0);
            return;
        }
        tryToBuildPiece(c, 0);
    }

    /**
     * Essaye de creer la ville choisit aleatoirement.
     *
     * @param c   represente la piece choisie.
     * @param occ reoresente l'occurrence actuelle.
     * @return true si la piece est posee, false sinon.
     */
    private boolean tryToBuildCity(char c, int occ) {
        if (occ >= this.originalPiecesNumber * 2) {
            return false;
        }
        int n = random(0, placedPieces.size());
        for (Placeable p : placedPieces) {
            if (n <= 0) {
                if ((construirePiece(c, p.getTile(), p.getVertex(), null))) {
                    return true;
                }
                return tryToBuildCity(c, ++occ);
            }
            n--;
        }
        return false;
    }

    /**
     * Essaye de creer la piece choisit aleatoirement.
     *
     * @param c   represente la piece choisie (route ou colonie).
     * @param occ reoresente l'occurrence actuelle.
     * @return true si la piece est posee, false sinon.
     */

    private boolean tryToBuildPiece(char c, int occ) {
        if (occ >= this.originalPiecesNumber * 2) {
            return false;
        }
        int n = random(0, placedPieces.size());
        for (Placeable p : placedPieces) {
            if (n <= 0) {
                if (construirePiece(c, p.getTile(), p.getFreeVertex(), p.getFreeEdge())) {
                    return true;
                }
                return tryToBuildPiece(c, ++occ);
            }
            n--;
        }
        return false;
    }


    /**
     * Joue automatiquement son tour base sur l'aleatoire.
     *
     * @param b    represente le plateau de jeu.
     * @param dice represente la valeur du de.
     */
    @Override
    public void playRound(Board b, int dice) {
        if (random(0, 10) < 6) {
            constructRandomThings();
        }
        if (random(0, 10) < 8) {
            for (int i = 0; i < random(0, 3); i++) makeExchages();
        }
        if (random(0, 10) > 60) prendreDeveloppement();
        if (random(0, 3) > 80) useDevCard(b);
        if (dice == 7) {
            moveThief(getRandomTile(b), b.getThief());
        }
    }

    @Override
    public void overLimitRessources() {
        int ressourcesNumber = getRessourcesNumber();
        if (ressourcesNumber <= 7) return;
        List<CarteRessource> highRessources = getTooHighRessources();
        int size = highRessources.size();
        ressourcesNumber = ressourcesNumber / 2;
        while (getRessourcesNumber() > ressourcesNumber) {
            CarteRessource r = highRessources.get(random(0, size));
            random(0, 0);
            enleveRessource(r, 1);
        }
    }

    /**
     * Cherche les ressources les plus eleve des ressources de l'IA
     *
     * @return la collection de ces ressources
     */
    private List<CarteRessource> getTooHighRessources() {
        List<CarteRessource> highRessources = new ArrayList<>();
        for (CarteRessource ressource : getCarteRessources().keySet()) {
            int val = getCarteRessources().get(ressource);
            if (val >= 2) highRessources.add(ressource);
        }
        return highRessources;
    }

    /**
     * Utilise une carte developpement
     *
     * @param b represente le plateau de jeu
     */
    private void useDevCard(Board b) {
        boolean isUsed = false;
        int occ = 0;
        int maxOcc = getCarteDeveloppements().size() * 2;
        while (isUsed || occ < maxOcc) {
            isUsed = tryUseDevCard(b);
            occ++;
        }
    }

    /**
     * Essaye d'utilise une carte de developpement
     *
     * @param b represente le plateau de jeu
     */
    private boolean tryUseDevCard(Board b) {
        int rd = random(0, carteDeveloppements.size());
        boolean isUsed = false;
        for (CarteDeveloppement card : carteDeveloppements.keySet()) {
            if (rd <= 0 && !card.isVictoryPoint()) {
                isUsed = use(card, b);
            } else {
                rd--;
            }
        }
        return isUsed;
    }

    /**
     * Utilise la carte developpement
     *
     * @param card represente la carte a utiliser
     * @param b    represente le plateau de jeu
     * @return true si la carte a ete utilisee, false sinon
     */
    private boolean use(CarteDeveloppement card, Board b) {
        if (carteDeveloppements.get(card) <= 0) return false;
        switch (card.getCode()) {
            case 'C':
                moveThief(getRandomTile(b), b.getThief());
                useCard('C');
                return true;
            case 'R':
                return buildCard();
            case 'D':
                return discoveryCard(b);
            default:
                return false;
        }
    }

    /**
     * Utilise une carte decouverte
     *
     * @param b represente le plateau de jeu
     * @return true si la carte a ete utilisee, false sinon
     */
    private boolean discoveryCard(Board b) {
        for (int i = 0; i < 2; i++) {
            var tile = getRandomTile(b);
            if (tile == null) i--;
            else harvest(tile);
        }
        useCard('D');
        return true;
    }

    /**
     * Utilise la carte construction de route
     *
     * @return true si la carte a ete utilisee, false sinon
     */
    private boolean buildCard() {
        boolean used = false;
        for (int i = 0; i < 2; i++) {
            if (tryToBuildPiece('r', 0)) {
                ROAD_NEEDED_RESSOURCES.forEach(this::prendreRessource);
                used = true;
            }
        }
        if (used) useCard('R');
        return used;
    }

    /**
     * Realise les echanges.
     */
    private void makeExchages() {
        CarteRessource minRessource = getSmallRessource();
        CarteRessource maxRessource = getHighRessource();
        if (maxRessource == null || minRessource == null) return;
        if (getCarteRessources().get(maxRessource) < 4) return;
        if (getCarteRessources().get(minRessource) > 3) return;
        chooseExchangeType(minRessource, maxRessource);

    }

    /**
     * Chosit le meileur echange a proceder entre un echange depouis le port ou avec la banque.
     *
     * @param minRessource represente la ressource la plus rare a obtenir par l'echange.
     * @param maxRessource represente la ressource la plus commune a echanger.
     */
    private void chooseExchangeType(CarteRessource minRessource, CarteRessource maxRessource) {
        Port port = getUsefulPort(minRessource);
        if (port == null) echanger(maxRessource, minRessource);
        else echanger(port, maxRessource);
    }

    /**
     * Cherche la ressource la plus rare.
     *
     * @return la ressource en question.
     */
    private CarteRessource getSmallRessource() {
        int min = -1;
        CarteRessource carteRessource = null;
        for (CarteRessource ressource : getCarteRessources().keySet()) {
            int val = getCarteRessources().get(ressource);
            if (val < min || min == -1) {
                min = val;
                carteRessource = ressource;
            }
        }
        return carteRessource;
    }

    /**
     * Cherche la ressource la plus commmune.
     *
     * @return la ressource en question.
     */
    private CarteRessource getHighRessource() {
        int max = -1;
        CarteRessource carteRessource = null;
        for (CarteRessource ressource : getCarteRessources().keySet()) {
            int val = getCarteRessources().get(ressource);
            if (val > max) {
                max = val;
                carteRessource = ressource;
            }
        }
        return carteRessource;
    }


}
