package up.catansColons.player;

import up.catansColons.board.Board;
import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Port;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;
import up.catansColons.board.piece.*;
import up.catansColons.cards.CarteDeveloppement;
import up.catansColons.cards.CarteRessource;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represente un joueur
 */
public class Joueur {

    /**
     * Represente les ressources necessaire a la construction d'une colonie.
     *
     * @see CarteRessource
     */
    public static final Map<CarteRessource, Integer> COLONY_NEEDED_RESSOURCES = setColonyNeededRessources();
    /**
     * Represente les ressources necessaire a la construction d'une ville.
     *
     * @see CarteRessource
     */
    public static final Map<CarteRessource, Integer> CITY_NEEDED_RESSOURCES = setCityNeededRessources();
    /**
     * Represente les ressources necessaire a la construction d'une route.
     *
     * @see CarteRessource
     */
    public static final Map<CarteRessource, Integer> ROAD_NEEDED_RESSOURCES = setRoadNeededRessources();
    /**
     * Represente les ressources necessaire a l'aquisition d'une carte developpement.
     *
     * @see CarteRessource
     */
    public static final Map<CarteRessource, Integer> DEV_CARDS_NEEDED_RESSOURCES = setDevCardNeededRessources();
    /**
     * Represente la configuration initiale du nombre de piece en debut de partie.
     */
    private static final Map<Class<?>, Integer> PIECES_SETUP = setPiecesSetup();

    public static int nextId = 0;
    /**
     * Represente la liste des ressources.
     */
    Map<CarteRessource, Integer> carteRessources;
    /**
     * Represente la liste des cartes developpement
     */
    Map<CarteDeveloppement, Integer> carteDeveloppements;
    String pseudo;
    String color;
    int nbPoints;
    int ID;
    /**
     * Represente les pieces du joueur
     */
    List<Piece> pieces;
    /**
     * Represente les pieces placees du joueur
     */
    List<Piece> placedPieces;
    /**
     * Indique le nombre de carte chevalier utilisee
     */
    private int usedKnightCards;
    /**
     * Indique si le joueur possede la route la plus longue
     */
    private boolean longestRoad;
    /**
     * Indique si le joueur possede la plus grande armee
     */
    private boolean highestKnight;

    public Joueur(String pseudo) {
        this.pseudo = pseudo;
        this.ID = nextId % 4;
        nextId++;
        this.usedKnightCards = 0;
        this.carteRessources = setRessourcesCards();
        this.carteDeveloppements = setDevCards();
        this.pieces = new LinkedList<>();
        this.placedPieces = new LinkedList<>();
    }

    /**
     * Initialise les prerequis de la construction d'une colonie.
     *
     * @return la valeur de COLONY_NEEDED_RESSOURCES.
     * @see CarteRessource
     */
    private static Map<CarteRessource, Integer> setColonyNeededRessources() {
        HashMap<CarteRessource, Integer> neededRessources = new HashMap<>();
        neededRessources.put(new CarteRessource('a'), 1);
        neededRessources.put(new CarteRessource('w'), 1);
        neededRessources.put(new CarteRessource('b'), 1);
        neededRessources.put(new CarteRessource('l'), 1);
        return neededRessources;
    }

    /**
     * Initialise les prerequis de la construction d'une ville.
     *
     * @return la valeur de CITY_NEEDED_RESSOURCES.
     * @see CarteRessource
     */
    private static Map<CarteRessource, Integer> setCityNeededRessources() {
        HashMap<CarteRessource, Integer> neededRessources = new HashMap<>();
        neededRessources.put(new CarteRessource('b'), 2);
        neededRessources.put(new CarteRessource('p'), 2);
        return neededRessources;
    }

    /**
     * Initialise les prerequis de la construction d'une route.
     *
     * @return la valeur de ROAD_NEEDED_RESSOURCES.
     * @see CarteRessource
     */
    private static Map<CarteRessource, Integer> setRoadNeededRessources() {
        HashMap<CarteRessource, Integer> neededRessources = new HashMap<>();
        neededRessources.put(new CarteRessource('a'), 1);
        neededRessources.put(new CarteRessource('w'), 1);
        return neededRessources;
    }

    /**
     * Initialise les prerequis de l'aquisition d'une carte developpement.
     *
     * @return la valeur de DEVELOPMENT_CARDS_NEEDED_RESSOURCES.
     * @see CarteRessource
     */
    private static Map<CarteRessource, Integer> setDevCardNeededRessources() {
        HashMap<CarteRessource, Integer> neededRessources = new HashMap<>();
        neededRessources.put(new CarteRessource('p'), 1);
        neededRessources.put(new CarteRessource('l'), 1);
        neededRessources.put(new CarteRessource('b'), 1);
        return neededRessources;
    }

    /**
     * Initialise la configuration initiale de pieces de jeu
     *
     * @return la valeur de PIECES_SETUP
     */
    private static Map<Class<?>, Integer> setPiecesSetup() {
        String packageName = "up.catansColons.board.piece.";
        HashMap<Class<?>, Integer> piecesSetup = new HashMap<>();
        try {
            piecesSetup.put(Class.forName(packageName + "Colony"), 5);
            piecesSetup.put(Class.forName(packageName + "City"), 4);
            piecesSetup.put(Class.forName(packageName + "Road"), 15);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return piecesSetup;
    }

    /**
     * Initailise la map de traduction des pieces
     */
    public static Map<Character, String> setPieceCodeToPrintFr() {
        HashMap<Character, String> codeToPrintFr = new HashMap<>();
        codeToPrintFr.put('r', "Route");
        codeToPrintFr.put('c', "Colonie");
        codeToPrintFr.put('t', "Ville");
        return codeToPrintFr;
    }

    public static int lanceDe() {
        Random rd = new Random();
        return rd.nextInt(11) + 2;
    }

    /**
     * Initialise la map de traduction des carte de developpement
     */
    private Map<CarteDeveloppement, Integer> setDevCards() {
        Map<CarteDeveloppement, Integer> devCards = new HashMap<>();
        for (char code : CarteDeveloppement.DEV_CARDS_CODE_TO_PRINT_FR.keySet()) {
            devCards.put(new CarteDeveloppement(code), 0);
        }
        return devCards;
    }

    /**
     * Initialise la liste des ressources
     *
     * @return la valeur de carteRessources
     */
    private Map<CarteRessource, Integer> setRessourcesCards() {
        HashMap<CarteRessource, Integer> ressources = new HashMap<>();
        for (char code : CarteRessource.RESSOURCE_CODE_TO_PRINT_FR.keySet()) {
            ressources.put(new CarteRessource(code), 0);
        }
        return ressources;
    }

    /**
     * Affiche sur le terminal les informations importante pour le joueur
     */
    public void affiche() {
        System.out.println("Joueur : " + this.pseudo);
        System.out.print("Carte de Ressource : [ ");
        carteRessources.forEach((r, i) -> {
            r.affiche();
            System.out.print("(" + i + ") ");
        });
        System.out.println("]");
        System.out.print("Carte de Developpement : [");
        carteDeveloppements.forEach((r, i) -> {
            r.affiche();
            System.out.print("(" + i + ") ");
        });
        System.out.println("]");
    }

    /**
     * Donne le nombre de piece initiale au joueur selon les PIECES_SETUP.
     */
    public void initPiece() {
        PIECES_SETUP.forEach((c, val) -> {
            try {
                var constructor = c.getConstructor(int.class);
                for (int i = 0; i < val; i++) {
                    this.pieces.add((Piece) constructor.newInstance(this.ID));
                }
            } catch (ReflectiveOperationException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Verifie si le joueur a assez de ressource.
     *
     * @param ressource    indique la ressource a chercher
     * @param nbNecessaire indique le nombre necessaire
     * @return true si c'est le cas, false sinon
     */
    public boolean aRessource(CarteRessource ressource, int nbNecessaire) {
        return this.carteRessources.get(ressource) >= nbNecessaire;
    }

    public boolean aRessource(char ressource, int nbNecessaire) {
        return aRessource(new CarteRessource(ressource), nbNecessaire);
    }

    /**
     * Enleves des ressources au joueur
     *
     * @param ressource   la ressource a retirer
     * @param nbRessource le nombre a retirer
     */
    public void enleveRessource(CarteRessource ressource, int nbRessource) {
        this.carteRessources.computeIfPresent(ressource, (r, i) -> {
            int tmp = i - nbRessource;
            return (tmp < 0) ? i : tmp;
        });
    }

    /**
     * Enleves des ressources au joueur
     *
     * @param ressource   la ressource a retirer
     * @param nbRessource le nombre a retirer
     */
    public void enleveRessource(char ressource, int nbRessource) {
        enleveRessource(new CarteRessource(ressource), nbRessource);
    }


    public void enlevePiece(Piece p) { /* Enleve une route des pieces. */
        this.pieces.removeIf(piece -> piece == p);
        if (!placedPieces.contains(p)) placedPieces.add(p);
    }

    public boolean construirePiece(char n, Tile t, Vertex v, Edge e) { /* Construit une piece (route (r), colonie(c), ville(t)) a un certain endroit du plateau
															 precise en parametre (Tile, Vertex, Edge). */
        switch (n) {
            case ('r'):
                return foundRoad(t, v, e);
            case ('c'):
                return foundColony(t, v);
            case ('t'):
                return cityUpgrade(v);

            default:
                return false;

        }
    }

    /**
     * Place une colonie du jeu de piece sur le plateau
     *
     * @param t represente la tuile ou placer la colonie
     * @param v represente le coin ou placer la colonie.
     * @return true si la construction s'est deroule sans probleme, false sinon
     */
    private boolean foundColony(Tile t, Vertex v) {
        if (!this.construireColonie()) return false;
        Colony colony = getUnplacedColony();
        if (colony == null) return false;
        if (!colony.foundNewColony(t, v)) return false;
        this.removeRessources(COLONY_NEEDED_RESSOURCES);
        this.enlevePiece(colony);
        return true;
    }

    /**
     * Place une colonie du jeu de piece sur le plateau
     *
     * @param t represente la tuile ou placer la route
     * @param v represente le coin ou placer la route
     * @param e represente l'arrete ou placer la route
     * @return true si la construction s'est deroule sans probleme, false sinon
     */
    private boolean foundRoad(Tile t, Vertex v, Edge e) {
        if (!this.construireRoute()) return false;
        Road road = this.getUnplacedRoad();
        if (road == null) return false;
        if (!road.place(t, v, e)) return false;
        this.removeRessources(ROAD_NEEDED_RESSOURCES);
        this.enlevePiece(road);
        return true;
    }

    /**
     * Ameliore une colonie en ville
     *
     * @param v represente le coin ou placer la ville
     * @return true si l'amelioration s'est deroule sans probleme, false sinon
     */
    private boolean cityUpgrade(Vertex v) {
        if (v == null) return false;
        Colony colony = v.getColony();
        if (colony == null) return false;
        if (!this.construireVille(colony)) return false;
        this.enlevePiece(colony);
        this.placedPieces.remove(colony);
        return true;
    }

    public boolean construireRoute() { /* Construit une route. */
        return this.checkNeededRessources(ROAD_NEEDED_RESSOURCES);
    }

    /**
     * Cherche une route non construite parmis les pieces libres.
     *
     * @return la route a construire, null si elle n'existe pas.
     */
    public Road getUnplacedRoad() {
        for (Piece p : this.pieces) {
            if ((p instanceof Road) && !p.isPlaced()) {
                return (Road) p;
            }
        }
        return null;
    }

    public boolean construireColonie() { /* Construit une colonie. */
        return this.checkNeededRessources(COLONY_NEEDED_RESSOURCES);
    }

    /**
     * Verifie que le joueur possede bien les ressources requise pour la construction demande.
     *
     * @param neededRessources represente les prerequis pour la construction.
     * @return true, si la construction peut avoir lieu, false sinon.
     */
    boolean checkNeededRessources(Map<CarteRessource, Integer> neededRessources) {
        for (CarteRessource ressource : neededRessources.keySet()) {
            if (!this.aRessource(ressource, neededRessources.get(ressource))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Verifie si le joueur a assez de ressources pour  prendre une carte
     *
     * @return true si c'est le cas, false sinon
     */
    public boolean canTakeCard() {
        return this.checkNeededRessources(DEV_CARDS_NEEDED_RESSOURCES);
    }

    /**
     * Verifie si le joueur a assez de ressources pour constuire une ville
     *
     * @return true si c'est le cas, false sinon
     */
    public boolean canBuildCity() {
        return this.checkNeededRessources(CITY_NEEDED_RESSOURCES) && isCityAvailable();
    }

    /**
     * Cherche une colonie non construite parmis les pieces libres.
     *
     * @return la colonie a construire, null si elle n'existe pas.
     */
    public Colony getUnplacedColony() {
        for (Piece p : this.pieces) {
            if ((p.getClass() == Colony.class) && !p.isPlaced()) {
                return (Colony) p;
            }
        }
        return null;
    }


    private boolean construireVille(Colony co) { /* Construit une ville a partir d'une colonie. */
        if (!co.isOwner(getID()) || !this.checkNeededRessources(CITY_NEEDED_RESSOURCES)) return false;
        if (!isCityAvailable()) return false;
        City city = co.amelioration();
        if (city == null) return false;
        placedPieces.add(city);
        removeCity();
        this.removeRessources(CITY_NEEDED_RESSOURCES);
        return true;
    }

    /**
     * Retire une ville des pieces du joueur
     */
    private void removeCity() {
        for (Piece piece : pieces) {
            if (piece instanceof City) {
                pieces.remove(piece);
                return;
            }
        }
    }

    /**
     * Indique si le joueur possedes des villes libres
     *
     * @return true si c'est le cas, false sinon
     */
    private boolean isCityAvailable() {
        for (Piece p : this.pieces) {
            if ((p instanceof City) && (!p.isPlaced())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prend le nombre donne de ressource
     *
     * @param ressource   indique la ressource a prendre
     * @param nbRessource indique le nombre a prendre
     */
    public void prendreRessource(CarteRessource ressource, int nbRessource) {
        this.carteRessources.computeIfPresent(ressource, ((r, i) -> i += nbRessource));
    }

    /**
     * Prend le nombre donne de ressource
     *
     * @param ressource   indique la ressource a prendre
     * @param nbRessource indique le nombre a prendre
     */
    public void prendreRessource(char ressource, int nbRessource) {
        prendreRessource(new CarteRessource(ressource), nbRessource);
    }

    /**
     * Place une route selon les regles du jeu. La route est placee aleatoirement.
     *
     * @param t Represente la tuile a laquelle le coin appartient.
     * @param v Represente le coin auquel l'arrete est relie.
     * @param e Represente l'arrete sur laquelle la route est posee.
     * @return true si le placement s'est deroule sans probleme, false sinon.
     */
    public boolean placeFirstRoad(Tile t, Vertex v, Edge e) {
        Road road = getUnplacedRoad();
        if (road == null) return false;
        if (road.place(t, v, e)) {
            this.pieces.remove(e.getRoad());
            placedPieces.add(e.getRoad());
            return true;
        }
        return false;
    }

    /**
     * Place la premiere Colony selon les regles du jeu. Cette colonie est placee aleatoirement.
     *
     * @param t Represente la tuile a laquelle le coin appartient.
     * @param v Represente le coin auquel la colonie est reliee.
     * @return true si le placement s'est deroule sans probleme, false sinon.
     */
    public boolean placeFirstColony(Tile t, Vertex v) {
        Colony colony = getUnplacedColony();
        if (colony == null) return false;
        if (colony.place(t, v)) {
            this.pieces.remove(colony);
            this.placedPieces.add(colony);
            return true;
        }
        return false;
    }

    /**
     * Prend une carte developpement
     */
    public boolean prendreDeveloppement() {
        if (!this.checkNeededRessources(DEV_CARDS_NEEDED_RESSOURCES)) return false;
        var card = CarteDeveloppement.takeCard();
        if (card == null) return false;
        else carteDeveloppements.computeIfPresent(card, (c, i) -> i + 1);
        this.removeRessources(DEV_CARDS_NEEDED_RESSOURCES);
        return true;
    }

    /**
     * Retire toutes les ressources requise
     *
     * @param neededRessources represente les prerequis pour la construction
     */
    private void removeRessources(Map<CarteRessource, Integer> neededRessources) {
        neededRessources.forEach(this::enleveRessource);
    }

    public boolean echanger(Port p, CarteRessource RessourceDonner) { /* echange des ressources aux port en fonction du ratio de celui-ci. */
        CarteRessource RessourcePort = new CarteRessource(p.getPortRessource());
        int ratio = p.getRatio();

        if (!this.aRessource(RessourceDonner, ratio)) return false;
        this.enleveRessource(RessourceDonner, ratio);
        this.prendreRessource(RessourcePort, 1);
        return true;
    }

    public void echanger(CarteRessource ressource, CarteRessource RessourceDemander) { /* Echange 3 ressources identique contre une ressource demandee. */
        if (this.aRessource(ressource, 3)) {
            this.enleveRessource(ressource, 3);
            this.prendreRessource(RessourceDemander, 1);
        }
    }

    public void echanger(char ressource, char askedRessource) {
        echanger(new CarteRessource(ressource), new CarteRessource(askedRessource));
    }

    public void playFirstRound(Board b) {
    }

    public void playSecondRound(Board b) {
    }

    public void playRound(Board b, int dice) {
    }

    /**
     * Deplace le voleur
     *
     * @param tile  la tuile ou deplacer le voleur
     * @param thief represente le voleur du plateau
     */
    public void moveThief(Tile tile, Thief thief) {
        thief.move(tile);
    }


    /**
     * Recolte les ressources de toutes les colonies
     *
     * @param dice represente la valeur du de lors de ce tour
     */
    public void harvestRessources(int dice) {
        placedPieces.forEach(piece -> {
            if (piece instanceof Colony) {
                boolean isCity = piece instanceof City;
                Collection<Tile> tiles = ((Colony) piece).getNextTiles();
                if (tiles == null) return;
                tiles.forEach(t -> harvest(t, dice, isCity));
            }
        });
    }

    /**
     * Recolte les ressources des tuiles dans tiles
     *
     * @param tiles represente les tuile a recolter
     */
    public void harvest(Collection<Tile> tiles) {
        tiles.forEach(tile -> prendreRessource(new CarteRessource(tile.getRessource()), 1));
    }

    /**
     * Recolte la ressource de la tuile
     *
     * @param tile represente la tuile ou recolter la ressource
     */
    public void harvest(Tile tile) {
        prendreRessource(new CarteRessource(tile.getRessource()), 1);
    }

    /**
     * //Effectue la recolte de ressources.
     *
     * @param t      represnete la tuile a recolter
     * @param dice   represente la valeur du de lors de ce tour
     * @param isCity indique si la colonie est une ville
     */
    private void harvest(Tile t, int dice, boolean isCity) { //Effectue la recolte de ressources
        if (t.getNumber() != dice || t.isThiefPresent()) return;
        if (isCity) {
            prendreRessource(new CarteRessource(t.getRessource()), 2);
        } else {
            prendreRessource(new CarteRessource(t.getRessource()), 1);
        }
    }

    public Map<CarteRessource, Integer> getCarteRessources() {
        return carteRessources;
    }

    /**
     * Vide la liste en la rendant son etat d'origine.
     */
    public void clearRessources() {
        this.carteRessources.forEach((r, i) -> carteRessources.replace(r, i, 0));
    }

    public Map<CarteDeveloppement, Integer> getCarteDeveloppements() {
        return carteDeveloppements;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getColor() {
        return color;
    }

    /**
     * Indique le score du joueur
     *
     * @return le score
     */
    public int getScore() {
        int score = carteDeveloppements.get(new CarteDeveloppement('V'));
        if (highestKnight) score += 2;
        if (longestRoad) score += 2;
        for (Piece piece : this.placedPieces) {
            score += piece.getScore();
        }
        return score;
    }

    /**
     * Indique le nombre de ressources possede par le joueur
     */
    public int getRessourcesNumber() {
        int n = 0;
        for (int r : carteRessources.values()) n += r;
        return n;
    }

    /**
     * Indique le nombre de carte developpement possede par le joueur
     */
    public int getDevCardsNumber() {
        int n = 0;
        for (int r : carteDeveloppements.values()) n += r;
        return n;
    }

    /**
     * Indique si le joueur possede une carte
     *
     * @param c represente le code de la carte a chercher
     * @return true si c'est le cas, false sinon
     */
    public boolean hasCard(char c) {
        return hasCard(new CarteDeveloppement(c));
    }

    /**
     * Indique si le joueur possede une carte
     *
     * @param d represente la carte a chercher
     * @return true si c'est le cas, false sinon
     */
    public boolean hasCard(CarteDeveloppement d) {
        return carteDeveloppements.get(d) > 0;
    }

    /**
     * Supprime la moitie des ressources choisies par le joueur
     */
    public void overLimitRessources() {
    }

    /**
     * Calcule la longueur de la route la plus longue
     *
     * @return cette valeur
     */
    public int getLongestRoadSize() {
        AtomicInteger n = new AtomicInteger();
        placedPieces.forEach(piece -> {
            int current = piece.getLongestRoadSize();
            if (n.get() < current) n.set(current);
        });
        return n.get();
    }

    public int getID() {
        return ID;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public List<Piece> getPlacedPieces() {
        return placedPieces;
    }

    public int getRessourceNumber(char c) {
        return carteRessources.get(new CarteRessource(c));
    }

    /**
     * Indique si le joueur a des cartes develoopements
     *
     * @return true si c'est le cas
     */
    public boolean hasDevCard() {
        for (int v : carteDeveloppements.values()) {
            if (v > 0) return true;
        }
        return false;
    }

    /**
     * Indique quelle piece le joueur est capable de construire
     *
     * @return une collection contenant ler code des pieces constructibles
     */
    public Set<Character> isAbleToConstruct() {
        HashSet<Character> construction = new HashSet<>();
        if (this.checkNeededRessources(COLONY_NEEDED_RESSOURCES) && isColonyAvailable()) {
            construction.add('c');
        }
        if (this.checkNeededRessources(CITY_NEEDED_RESSOURCES) && isCityAvailable()) {
            construction.add('t');
        }

        if (this.checkNeededRessources(ROAD_NEEDED_RESSOURCES) && isRoadAvailable()) {
            construction.add('r');
        }
        return construction;
    }

    /**
     * Cherche le port le plus rentable des ports possedes par le joueur
     *
     * @param minRessource represente la ressource la plus rare a obtenir par l'echange.
     * @return le port si il existe, null sinon.
     */
    public Port getUsefulPort(CarteRessource minRessource) {
        Set<Port> ports = getAllPorts();
        Port port = null;
        for (Port p : ports) {
            if (p.getPortRessource() == minRessource.getRessource()) {
                if (port == null) port = p;
                port = (port.getRatio() < p.getRatio()) ? port : p;
            }
        }
        return port;
    }

    /**
     * Cherche tous les ports possedes par le joueur
     *
     * @return l'ensemble des ports
     */
    private Set<Port> getAllPorts() {
        Set<Port> ports = new HashSet<>();
        this.placedPieces.forEach(piece -> {
            if (!(piece instanceof Colony)) return;
            Colony colony = (Colony) piece;
            Collection<? extends Port> nextPorts = colony.getNextPort();
            if (nextPorts == null) return;
            ports.addAll(nextPorts);
        });
        return ports;
    }

    /**
     * Indique s'il reste une route libre dans les pieces
     */
    public boolean isRoadAvailable() {
        return getUnplacedRoad() != null;
    }

    /**
     * Indique s'il reste une colonie libre dans les pieces
     */
    public boolean isColonyAvailable() {
        return getUnplacedColony() != null;
    }

    public int getUsedKnightCards() {
        return usedKnightCards;
    }

    public void setHighestKnight(boolean highestKnight) {
        this.highestKnight = highestKnight;
    }

    public void setLongestRoad(boolean longestRoad) {
        this.longestRoad = longestRoad;
    }

    /**
     * Utilise une carte construction
     *
     * @param edge represente l'arrete ou constuire la route
     * @return true si la route est construite
     */
    public boolean useConstructionCard(Edge edge) {
        Road road = this.getUnplacedRoad();
        if (road == null) return false;
        return road.place(edge.getCurrent().getTile(), edge.getCurrent(), edge);
    }

    /**
     * Retire la carte utilise de la collection
     *
     * @param code represente le code de la carte a retirer
     */
    public void useCard(char code) {
        carteDeveloppements.computeIfPresent(new CarteDeveloppement(code), (c, i) -> {
            c.replace();
            return i - 1;
        });
        if (code == 'C') usedKnightCards++;
    }

    public int getCardNumber(char code) {
        return carteDeveloppements.get(new CarteDeveloppement(code));
    }

    /**
     * Retire une colonie pose sur le plateau
     *
     * @param v represente le coin ou retier la carte
     */
    public void remove(Vertex v) {
        this.placedPieces.remove(v.getColony());
        if (v.getColony().isOwner(getID())) this.pieces.add(v.getColony());
        v.getColony().remove();
    }
}
