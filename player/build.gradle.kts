plugins {
    `java-library`
}

dependencies {
    implementation(project(":cards"))
    implementation(project(":board"))
    implementation("junit:junit:4.13.2")
}