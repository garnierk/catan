package up.catansColons.board.composant;


import up.catansColons.board.Board;
import up.catansColons.board.piece.Thief;

import java.util.HashSet;
import java.util.Map;
import java.util.Random;

/**
 * Correspond a une tuile du jeu
 */

public class Tile {
    /**
     * Sert a la traduction entre le code interne et la sortie utilisateur en francais
     */
    public static final Map<Character, String> CODE_TO_PRINT_FR = Board.CODE_TO_PRINT_FR;
    /**
     * Represente les coins de la tuile
     */
    private final Vertex[][] vertices;
    /**
     * represnete la ressource associe a la tuile
     */
    private final Character ressources;
    /**
     * Indique toutes les tuiles adjacente a cette tuile
     */
    private final HashSet<Tile> nextTiles;
    /**
     * Indique le voleur est sur la tuile, null si il n'y est pas
     */
    private Thief thief;
    /**
     * Indique le nombre associe a la tuile
     */
    private int number;
    /**
     * Represente les differentes informations internes a la tuile pour la sortie utilisateur
     */
    private String tilePrint;

    public Tile(Character ressources) {
        this.ressources = ressources;
        vertices = new Vertex[2][2];
        thief = null;
        nextTiles = new HashSet<>();
        initialize();
        if (!this.ressources.equals(Board.DESERT)) {
            Random rd = new Random();
            do {
                this.number = rd.nextInt(10) + 2;
            } while (this.number == 7);
        }
        tilePrint = CODE_TO_PRINT_FR.get(ressources);
        if (!this.ressources.equals(Board.DESERT)) tilePrint += " " + number;
    }

    public void addNextTiles(Tile t) {
        this.nextTiles.add(t);
        t.addNextTiles(this);
    }

    public boolean isThiefPresent() {
        return this.thief != null;
    }

    public Thief getThief() {
        return thief;
    }

    public void setThief(Thief t) {
        thief = t;

    }

    /**
     * Initialise la tuile, ses coins et ses arretes internes
     */
    private void initialize() {
        for (int i = 0; i < vertices.length; i++) {
            vertices[0][i] = new Vertex(this);
            vertices[1][i] = new Vertex(this);
        }
    }

    /**
     * Renseigne si le coin est un coin de la tuile
     *
     * @param c represnet el coin a verifier
     * @return true si c'est le cas, false sinon
     */
    public boolean isVertexPresent(Vertex c) {
        for (Vertex[] vertex : vertices) {
            for (Vertex value : vertex) {
                if (c == value) return true;
            }
        }
        return false;
    }

    public String getTilePrint() {
        return tilePrint;
    }

    public void setTilePrint(String tilePrint) {
        this.tilePrint = tilePrint;
    }

    /**
     * Initialise les arretes de la tuile
     */
    public void initEdge() {
        for (int i = 0; i < vertices.length; i++) {
            Vertex current = vertices[i][0];
            Vertex next = vertices[i][1];
            Vertex current2 = vertices[0][i];
            Vertex next2 = vertices[1][i];
            current.getNextTiles().add(this);
            next.getNextTiles().add(this);
            current2.getNextTiles().add(this);
            next2.getNextTiles().add(this);
            new Edge(current, next, true);
            new Edge(current2, next2, false);
        }
    }


    public Vertex getVertex(int[] pos) {
        if (pos.length != 2) return null;
        return getVertex(pos[0], pos[1]);
    }

    public Vertex getVertex(int x, int y) {
        x = Math.max(0, Math.min(x, vertices.length - 1));
        y = Math.max(0, Math.min(y, vertices[0].length - 1));
        return vertices[x][y];
    }

    public Character getRessource() {
        return this.ressources;
    }

    public String getRessourceOutput() {
        return CODE_TO_PRINT_FR.get(ressources);
    }

    /**
     * Relie deux tuiles par la droite
     *
     * @param t represente la tuile a relier a this
     */
    public void linkRight(Tile t) {
        if (t == null || this == t) return;
        t.vertices[0][0] = vertices[0][1];
        t.vertices[1][0] = vertices[1][1];

    }

    /**
     * Relie deux tuiles par le bas
     *
     * @param t represente la tuile a relier a this
     */
    public void linkBottom(Tile t) {
        if (t == null || this == t) return;
        t.vertices[0] = vertices[1];

    }

    public boolean isDesert() {
        return this.ressources.equals(Board.DESERT);
    }

    public int getNumber() {
        return number;
    }

    public Vertex[][] getVertices() {
        return vertices;
    }

    @Override
    public String toString() {
        int blankLength = Board.LINE_LENGTH + 2 - tilePrint.length();
        StringBuilder builder = new StringBuilder();
        builder.append(vertices[0][0].getEdge(vertices[1][0]));
        for (int i = 0; i < blankLength / 2; i++) builder.append(' ');
        builder.append(tilePrint).append((isThiefPresent()) ? "(T)" : "");
        while (builder.length() < Board.LINE_LENGTH + 1) builder.append(' ');
        return builder.toString();
    }

    /**
     * Cherche un coin libre parmis ses coins
     *
     * @return le coin en question s'il existe, null sinon
     */
    public Vertex getFreeVertex() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; i++) {
                if (vertices[i][j].isFree()) return vertices[i][j];
            }
        }
        return null;
    }
}
