package up.catansColons.board.composant;

import up.catansColons.board.Board;

/**
 * Represente une tuile avec un port
 *
 * @see Tile
 */
public class Port extends Tile {
    /**
     * Represente le ratio du port pour le commerce
     */
    private final int ratio;
    /**
     * Represente la ressource echangeable du port
     */
    private final char portRessource;

    public Port(char ressources, int ratio) {
        super(ressources);
        this.ratio = ratio;
        portRessource = Board.getRandomRessource();
        setTilePrint(getTilePrint() + "(" + (CODE_TO_PRINT_FR.get(portRessource) + " " + ratio + ":1)"));
    }

    public int getRatio() {
        return ratio;
    }

    public char getPortRessource() {
        return portRessource;
    }

    public String getPortRessourceOutput() {
        return CODE_TO_PRINT_FR.get(portRessource);
    }
}
