package up.catansColons.board.composant;

import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Road;

import java.util.*;

/**
 * Represente les coins de la tuile,ou seront pose les colonies.
 *
 * @see Tile
 */

public class Vertex {

    /**
     * Represente le caractere des coins vide
     */
    private static final char VERTEX_CHAR = '+';
    /**
     * Represnete l'ensemble des arretes du coins
     */
    private final ArrayList<Edge> edges;
    /**
     * Represente la tuile auquel le coin appartient
     */
    private final Tile tile;
    /**
     * Represente toutes les tuiles adjacente au coin
     */
    private final HashSet<Tile> nextTiles;
    /**
     * Represente la colonie posee sur le coins
     */
    private Colony colony;
    /**
     * Represente le caractere affichable du coin
     */
    private char vertexPrint;

    public Vertex(Tile t) {
        edges = new ArrayList<>();
        nextTiles = new HashSet<>();
        if (t != null) nextTiles.add(t);
        tile = t;
        vertexPrint = VERTEX_CHAR;
    }

    public Vertex() {
        this(null);
    }

    public Tile getTile() {
        return tile;
    }

    public HashSet<Tile> getNextTiles() {
        return nextTiles;
    }

    /**
     * Ajoute une arrete sans doublons
     *
     * @param e represente l'arrete ajouter
     */
    public void addEdge(Edge e) {
        if (!edges.contains(e))
            edges.add(e);
    }

    /**
     * Cherche l'arrete entre deux coins (this et v)
     *
     * @param v represente le coin relie a this par l'arrete
     * @return l'arrete en question si elle existe, null sinon
     */
    public Edge getEdge(Vertex v) { //
        for (Edge e : edges) {
            if ((v == e.getNext(this)) && (this == e.getNext(v))) {
                return e;
            }
        }
        return null;
    }

    /**
     * Supprime l'arrete
     *
     * @param a represente l'arrete a supprime
     */
    public void removeEdge(Edge a) {
        edges.remove(a);
    }

    /**
     * Indique si l'arrete est relie au coin
     *
     * @param a represente l'arrete a chercher
     * @return true si elle est relie, false sinon
     */
    public boolean isPresent(Edge a) {
        for (Edge arrete : this.edges) {
            if (arrete == a) return true;
        }
        return false;
    }

    /**
     * Indique le nombre d'arrete relie au coin
     *
     * @return le nombre en question
     */
    public int nbEdge() {
        return edges.size();
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    /**
     * Cherche toutes les coins relies a this
     *
     * @return une collection contenant tout les coins
     */
    public Set<Vertex> getVertices() {
        HashSet<Vertex> ret = new HashSet<>();
        for (Edge a : edges) {
            Vertex previous = a.getNext(this);
            ret.add(previous);
        }
        return ret;
    }

    public Colony getColony() {
        return colony;
    }


    public void setColony(Colony colony) {
        this.colony = colony;

    }

    /**
     * Renseigne si le coin est proche d'une route alliée
     *
     * @param id represente l'id du joueur possedant la route
     * @return true si c'est le cas, false sinon
     */
    public boolean nextToFriendlyRoad(int id) {
        for (Edge edge : edges) {
            if (!edge.isFree() && edge.getRoad().getPlayerId() == id)
                return true;
        }
        return false;
    }

    /**
     * Indique si le coin est libre
     *
     * @return true si c'est le cas, false sinon
     */
    public boolean isFree() {
        return colony == null;
    }

    /**
     * Renseigne si on peut placer la colonie
     *
     * @return true si le cas, false sinon
     */
    public boolean isFreeToPlace() { //
        return isFree() && noCloseColony();
    }

    /**
     * Renseigne si il n'y a aucune autre colonie au voisinage proche de this
     *
     * @return true si le cas, false sinon
     */
    private boolean noCloseColony() {
        for (Edge a : edges) {
            Vertex c = a.getNext(this);
            if (c.colony != null) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        vertexPrint = (isFree()) ? VERTEX_CHAR : colony.getPlayerChar();
        return String.valueOf(vertexPrint);
    }


    //Retourne la longueur de la plus longue route
    public int getLongestRoadSize(int playerID, List<Vertex> vertexList, List<Road> roadList) {
        if (isEnemy(playerID) || vertexList.contains(this)) return 0;
        vertexList.add(this);
        int max = 0;
        for (Edge e : edges) {
            if (!e.isFree() && !roadList.contains(e.getRoad())) {
                int current =
                        e.getLongestRoadSize(
                                playerID, new ArrayList<>(vertexList), new ArrayList<>(roadList), this
                        );
                max = Math.max(max, current);
            }
        }
        return max;
    }

    private boolean isEnemy(int playerId) {
        return (!isFree() && !colony.isOwner(playerId));
    }

    /**
     * Cherche les arretes lire reliee au coin
     *
     * @return l'arrete si elle existe, null sinon
     */
    public Edge getFreeEdge() {
        for (Edge e : this.getEdges()) {
            if (e.isFree()) return e;
        }
        return null;
    }

    /**
     * Cherche tous les ports adjacents au coin
     *
     * @return une collection contenant ces ports
     */
    public Collection<? extends Port> getNextPorts() {
        Collection<Port> ports = new HashSet<>();
        nextTiles.forEach(tile -> {
            if (tile instanceof Port) ports.add((Port) tile);
        });
        return ports;
    }

    public int[] getPos() {
        Tile t = this.getTile();
        for (int i = 0; i < t.getVertices().length; i++) {
            for (int j = 0; j < t.getVertices()[i].length; j++) {
                if (t.getVertices()[i][j] == this) return new int[]{i, j};
            }
        }
        return null;
    }

    public Vertex getVertex(Vertex vertex, char c) {
        Tile t = vertex.getTile();
        int[] pos = getPos();
        if (pos == null) return null;
        switch (c) {
            case 'h':
                return t.getVertex(pos[0] - 1, pos[1]);
            case 'g':
                return t.getVertex(pos[0], pos[1] - 1);
            case 'd':
                return t.getVertex(pos[0], pos[1] + 1);
            case 'b':
                return t.getVertex(pos[0] + 1, pos[1]);
            default:
                return null;
        }
    }
}
