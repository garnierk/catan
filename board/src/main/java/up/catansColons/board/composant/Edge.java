package up.catansColons.board.composant;

import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Road;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Represente les arretes des Tuiles
 *
 * @see Tile
 */
public class Edge {
    /**
     * Represente le caractere d'une arrete vide horizontale
     */
    private static final char VERTICAL_CHAR = '|';
    /**
     * Represente le caractere d'une arrete vide vertical
     */
    private static final char HORIZONTAL_CHAR = '-';
    /**
     * Indique si l'arete est horizontale ou verticale
     */
    private final boolean horizontal;
    /**
     * Represente le lien entre chaque coin
     */

    private Vertex current, next;
    /**
     * Represente la route posee sur l'arrete
     */
    private Road road;
    /**
     * Represente le caractere affichable de l'arrete
     */
    private char edgePrint;

    public Edge(Vertex current, Vertex next) {
        this(current, next, false);
    }

    public Edge(Vertex current, Vertex next, boolean horizontal) {
        this.current = current;
        this.next = next;
        current.addEdge(this);
        next.addEdge(this);
        road = null;
        this.horizontal = horizontal;
        if (horizontal) edgePrint = HORIZONTAL_CHAR;
        else edgePrint = VERTICAL_CHAR;
    }

    public Vertex getCurrent() {
        return current;
    }

    public void setCurrent(Vertex current) {
        this.current = current;
    }

    public Vertex getNext() {
        return next;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public Vertex getNext(Vertex vertex) {
        return (this.current == vertex) ? next : current;
    }

    public Road getRoad() {
        return road;
    }

    public void setRoad(Road road) {
        this.road = road;
        edgePrint = (road != null) ? road.getPlayerChar() : getInitChar();
    }

    /**
     * Cherche le charactere a afficher sur le terminal en fonction de la verticalite
     *
     * @return le charactere affichable
     */
    private char getInitChar() {
        return (horizontal) ? HORIZONTAL_CHAR : VERTICAL_CHAR;
    }

    public void setEdgePrint(char edgePrint) {
        this.edgePrint = edgePrint;
    }


    public boolean isFree() {
        return road == null;
    }

    /**
     * Indique si l'arrete est proche d'une colonie alliee du playerId.
     *
     * @param id repesente l'Id du joueur la colonie
     * @return true si l'arrete est acollee a une colonie, false sinon
     */
    private boolean nextToFriendlyColony(int id) {
        if (this.current != null) {
            Colony colony = this.current.getColony();
            if (colony != null && colony.isOwner(id)) return true;
        }
        if (this.next != null) {
            Colony colony = this.next.getColony();
            return colony != null && colony.isOwner(id);
        }
        return false;
    }

    /**
     * Indique si l'arrete est proche d'une route alliee du playerId
     *
     * @param id repesente l'Id du joueur la route
     * @return true si l'arrete est acollee a une route, false sinon
     */
    private boolean nextToFriendlyRoad(int id) {
        for (Edge e : this.current.getEdges()) {
            if (!e.isFree() && e.getRoad().isOwner(id)) return true;
        }

        for (Edge e : this.next.getEdges()) {
            if (!e.isFree() && e.getRoad().isOwner(id)) return true;
        }
        return false;
    }

    /**
     * Indique si l'arrete est proche d'une infrastructure alliee
     *
     * @param id repesente l'Id du joueur des pieces
     * @return true si c'est le cas, false sinon
     */
    public boolean nextToFriendly(int id) {
        return nextToFriendlyColony(id) || nextToFriendlyRoad(id);
    }

    /**
     * Indique si l'arrete est libre et proche d'une infrastructure alliee
     *
     * @param id repesente l'Id du joueur des pieces
     * @return true si c'est le cas, false sinon
     */
    public boolean isFreeToPlace(int id) {
        return isFree() && nextToFriendly(id) && noEnemyColony(id);
    }

    private boolean noEnemyColony(int id) {
        Colony cn = next.getColony();
        Colony cc = current.getColony();
        if (cn != null && !cn.isOwner(id)) return false;
        return cc == null || cc.isOwner(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return (
                (
                        (Objects.equals(current, edge.current) && Objects.equals(road, edge.road))
                )
                        && Objects.equals(next, edge.next)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(current, next, road);
    }

    /**
     * Cherche une arrete libre parmis celles de ses deux extremites
     *
     * @return coin possedant une arrete libre s'il existe, null sinon
     */
    public Vertex getNotFullVertex() {
        for (Edge e : getCurrent().getEdges()) {
            if (e.isFree()) return getCurrent();
        }
        for (Edge e : getNext().getEdges()) {
            if (e.isFree()) return getNext();
        }
        return null;
    }

    /**
     * Cherche un coin libre parmis ses deux extrimites
     *
     * @return le coin en question
     */
    public Vertex getFreeVertex() {
        return (current.isFree()) ? current : next;
    }

    /**
     * Cherche un coin occupe parmis ses deux extremites
     *
     * @return le coin en question
     */
    public Vertex getOccupiedVertex() {
        return (current.isFree()) ? next : current;
    }

    @Override
    public String toString() {
        return String.valueOf(edgePrint);
    }

    /**
     * Indique si l'arrete est horizontale sur le plateau
     *
     * @return true si c'est le cas, false sinon
     */
    public boolean isHorizontal() {
        return horizontal;
    }

    /**
     * Parcours les pieces du joueur pour trouver la route la plus longue en passant au prochian coin non parcouru
     *
     * @param playerID   represente l'ID du joueur
     * @param vertices   represente l'ensemble des coins deja parcourus
     * @param roads      represente l'ensemble des routes parcourues
     * @param lastVertex represente le dernier vertex parcouru
     * @return 0 si la route est en bout de chaine, la valeur de getLongestRoadSize du prochain coin sinon
     */
    public int getLongestRoadSize(int playerID, ArrayList<Vertex> vertices, ArrayList<Road> roads, Vertex lastVertex) {
        if (isFreeOrEnemy(playerID) || roads.contains(this.road)) return 0;
        roads.add(road);
        Vertex v = getNext(lastVertex);
        return v.getLongestRoadSize(playerID, new ArrayList<>(vertices), new ArrayList<>(roads)) + 1;

    }

    /**
     * Indique si l'arrete est libre ou ennemie
     *
     * @param playerId represente l'ID du joueur
     * @return true si l'arrete est libre ou si la route posée dessus est ennemie, false sinon
     */
    private boolean isFreeOrEnemy(int playerId) {
        return isFree() || (!isFree() && !road.isOwner(playerId));
    }
}
