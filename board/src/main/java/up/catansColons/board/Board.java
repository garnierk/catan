package up.catansColons.board;

import up.catansColons.board.composant.Port;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.piece.Thief;
import up.catansColons.cards.CarteRessource;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Board {

    /**
     * Represente la longueur de l'affichage d'une tuile
     */
    public static final int LINE_LENGTH = 25;
    /**
     * Represente le nombre de port totaux par longueur de cote
     */
    public static final double PORT_COEFF = 1.5;
    /**
     * Represente la case desert
     */
    public static final char DESERT = 'd';

    /**
     * //Represente les ressources possibles
     */
    public static final Map<Character, String> CODE_TO_PRINT_FR = setCodeToPrintFr();
    /**
     * represente le centre du plateau
     */
    private final int[] center;
    /**
     * Represente les tuiles du plateau
     */
    private final Tile[][] tiles;
    /**
     * Represente le voleur
     */
    private Thief thief;
    /**
     * Nombre de port maximal par cote
     */
    private int nbPortTop, nbPortBottom, nbPortLeft, nbPortRight;

    public Board(int n, int m) {
        tiles = new Tile[n][m];
        center = new int[]{n / 2, m / 2};
        nbPortTop = (int) (m * PORT_COEFF);
        nbPortLeft = (int) (n * PORT_COEFF) - 2;
        nbPortRight = nbPortLeft;
        nbPortBottom = nbPortTop;
        initialize();
    }

    /**
     * Initailise la map de traduction entre la notation interne au code et la sortie utilisateur
     *
     * @return la valeur de CODE_TO_PRINT_FR
     */
    private static Map<Character, String> setCodeToPrintFr() {
        Map<Character, String> codeToPrintFr = new HashMap<>(CarteRessource.RESSOURCE_CODE_TO_PRINT_FR);
        codeToPrintFr.put(DESERT, "Desert");
        return codeToPrintFr;
    }

    /**
     * Donne un entier aleatoire pour la selection de ressources
     *
     * @return l'entier aleatoire
     */
    private static int rd() {
        Random random = new Random();
        return random.nextInt(CODE_TO_PRINT_FR.size() - 1);
    }

    /**
     * Donne une ressource aleatoire
     *
     * @return la ressource differente de DESERT
     */
    public static char getRandomRessource() {
        int n = rd();
        for (char c : CODE_TO_PRINT_FR.keySet()) {
            if ((n <= 0) && (c != DESERT)) return c;
            n--;
        }
        return 'd';
    }

    /**
     * Initialise le plateau
     */
    private void initialize() {
        initializeTopBottom();
        initializeRightLeft();
        initializeMiddle();
        for (Tile[] tile : tiles) {
            for (Tile t : tile) {
                t.initEdge();
            }
        }
    }

    /**
     * Relie les tuiles entre-elles
     *
     * @param i est une coordonnee
     * @param j est une coordonnee
     */
    private void linkTuiles(int i, int j) {
        if (isAbleToLink(tiles[i][j], tiles[i][j + 1])) tiles[i][j].linkRight(tiles[i][j + 1]);
        if (isAbleToLink(tiles[i][j], tiles[i + 1][j])) tiles[i][j].linkBottom(tiles[i + 1][j]);
    }

    private boolean isAbleToLink(Tile origin, Tile linker) {
        return origin != null && linker != null;
    }

    /**
     * est une coordonee
     */
    private void initializeMiddle() {
        for (int i = 0; i < tiles.length - 1; i++) {
            for (int j = 0; j < tiles[0].length - 1; j++) {
                initMiddleTile(i, j);
                linkTuiles(i, j);
            }
        }
    }

    private void initMiddleTile(int i, int j) {
        tiles[i][j] = tileOrDesert(i, j);
        tiles[i][j + 1] = tileOrDesert(i, j + 1);
        tiles[i + 1][j] = tileOrDesert(1 + i, j);
    }

    /**
     * Definie selon l'emplacement, si la tuile possede une ressource classique ou si c'est le desert
     *
     * @param i est une coordonnee
     * @param j est une coordonee
     * @return la tuile cree
     */

    private Tile tileOrDesert(int i, int j) {
        if (tiles[i][j] != null) return tiles[i][j];
        if (i == center[0] && j == center[1]) {
            return initDesert();
        }
        return new Tile(getRandomRessource());
    }

    private Tile initDesert() {
        Tile ret = new Tile(DESERT);
        this.thief = new Thief(ret);
        ret.setThief(thief);
        return ret;
    }

    /**
     * Definit par l'aleatoire si c'est un port ou une tuile classique
     *
     * @param position represente le cote du plateau
     * @return la tuile cree
     */
    private Tile tileOrPort(char position) {

        switch (position) {
            case 't':
                nbPortTop--;
                return initTileOrPort(nbPortTop);
            case 'b':
                nbPortBottom--;
                return initTileOrPort(nbPortBottom);

            case 'l':
                nbPortLeft--;
                return initTileOrPort(nbPortLeft);

            case 'r':
                nbPortRight--;
                return initTileOrPort(nbPortRight);
            default:
                return new Tile(getRandomRessource());
        }
    }

    private Tile initTileOrPort(int n) {
        int portRatio = (int) (Math.random() * 10) % 2 + 2;
        return (Math.random() > 0.5 && n >= 0) ?
                new Port(getRandomRessource(), portRatio) : new Tile(getRandomRessource());
    }

    /**
     * Initialise les bords superieur et inferieur du plateau
     */
    private void initializeTopBottom() {
        for (int i = 0; i < tiles[0].length - 1; i++) {
            initializeTop(i);
            initializeBottom(i);
        }
    }

    private void initializeTop(int i) {
        if (tiles[0][i] == null) tiles[0][i] = tileOrPort('t');
        if (tiles[0][i + 1] == null) tiles[0][i + 1] = tileOrPort('t');
        tiles[0][i].linkRight(tiles[0][i + 1]);
    }

    private void initializeBottom(int i) {
        int n = tiles.length - 1;
        if (tiles[n][i] == null) tiles[n][i] = tileOrPort('b');
        if (tiles[n][i + 1] == null) tiles[n][i + 1] = tileOrPort('b');
        tiles[n][i].linkRight(tiles[n][i + 1]);
    }

    /**
     * Initialise les bords lateraux du plateau
     */
    private void initializeRightLeft() { //
        for (int i = 0; i < tiles.length - 1; i++) {
            initializeRight(i);
            initializeLeft(i);
        }
    }

    /**
     * Initialise les bords lateraux du plateau
     */
    private void initializeRight(int i) {
        int n = tiles.length - 1;
        if (tiles[i][n] == null) tiles[i][n] = tileOrPort('r');
        if (tiles[i + 1][n] == null) tiles[i + 1][n] = tileOrPort('r');
        tiles[i][n].linkBottom(tiles[i + 1][n]);
    }

    /**
     * Initialise les bords lateraux du plateau
     */
    private void initializeLeft(int i) {
        if (tiles[i][0] == null) tiles[i][0] = tileOrPort('l');
        if (tiles[i + 1][0] == null) tiles[i + 1][0] = tileOrPort('l');
        tiles[i][0].linkBottom(tiles[i + 1][0]);
    }

    public Thief getThief() {
        return thief;
    }

    @Override
    public String toString() {
        StringBuilder tab = new StringBuilder();
        for (Tile[] tileLine : tiles) {
            tab.append(buildLine(tileLine, 0));
            tab.append(buildTiles(tileLine));
        }
        tab.append(buildLine(tiles[tiles.length - 1], 1));
        return tab.toString();
    }

    /**
     * Construit l'affichage String du contenu texte des tuiles ainsi que les arretes laterales
     *
     * @param tiles represente le tableau de tuiles a afficher
     * @return le builder qui a construit l'affichage
     */
    private StringBuilder buildTiles(Tile[] tiles) {
        StringBuilder tab = new StringBuilder();
        for (Tile t : tiles) {
            tab.append(t);
        }
        Tile lastTile = tiles[tiles.length - 1];
        tab.append(lastTile.getVertices()[0][1].getEdge(lastTile.getVertices()[1][1]));
        return tab.append('\n');
    }

    /**
     * Construit l'affichage String des coin et arretes
     *
     * @param tiles represente le tableau de tuiles a afficher
     * @return le builder qui a construit l'affichage
     */
    private StringBuilder buildLine(Tile[] tiles, int pos) {
        StringBuilder tab = new StringBuilder();
        for (Tile t : tiles) {
            StringBuilder builder = new StringBuilder();
            builder.append(t.getVertices()[pos][0]);
            for (int i = 0; i < LINE_LENGTH; i++)
                builder.append(t.getVertices()[pos][0].getEdge(t.getVertices()[pos][1]));
            tab.append(builder);
        }
        return tab.append(tiles[tiles.length - 1].getVertices()[pos][1]).append('\n');
    }

    /**
     * Affiche le plateau avec des coordonnees
     */
    public void print() {
        String blank = "  ";
        System.out.println(letterLine(blank));
        int charCode = 65;

        for (Tile[] tileLine : tiles) {
            System.out.print(blank + " " + buildLine(tileLine, 0));
            System.out.print(Character.toChars(charCode));
            System.out.print(blank + buildTiles(tileLine));
            charCode++;
        }
        System.out.println(blank + " " + buildLine(tiles[tiles.length - 1], 1));
    }

    /**
     * Affiche les coordonees en lettre
     *
     * @param blank represente le blanc a disposer au debut de chaque ligne
     * @return le builder contenant la ligne du plateau
     */
    private StringBuilder letterLine(String blank) {
        int blankLength = (Board.LINE_LENGTH + 3) / 2;
        StringBuilder builder = new StringBuilder();
        builder.append(blank);
        int charCode = 49;
        for (Tile ignored : tiles[0]) {
            for (int i = 0; i < blankLength; i++) builder.append(' ');
            builder.append(Character.toChars(charCode));
            for (int i = blankLength; i < Board.LINE_LENGTH; i++) builder.append(' ');

            charCode++;
        }
        return builder;
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    public Tile getTile(int x, int y) {
        x = Math.max(0, Math.min(x, tiles.length - 1));
        y = Math.max(0, Math.min(y, tiles[0].length - 1));
        return tiles[x][y];
    }

    public Tile getTile(int[] pos) {
        if (pos.length != 2) return null;
        return getTile(pos[0], pos[1]);
    }

    public Tile getTile(Tile t, char c) {
        int[] pos = getPos(t);
        if (pos == null) return null;
        switch (c) {
            case 'h':
                return getTile(pos[0] - 1, pos[1]);
            case 'g':
                return getTile(pos[0], pos[1] - 1);
            case 'd':
                return getTile(pos[0], pos[1] + 1);
            case 'b':
                return getTile(pos[0] + 1, pos[1]);
            default:
                return null;
        }
    }

    /**
     * Donne la position d'une tuise dans tiles
     *
     * @param t represente la tuile a chercher
     * @return la position en question
     */
    private int[] getPos(Tile t) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                if (t == tiles[i][j])
                    return new int[]{i, j};
            }
        }
        return null;
    }
}
