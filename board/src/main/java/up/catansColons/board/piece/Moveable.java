package up.catansColons.board.piece;

import up.catansColons.board.composant.Tile;


/**
 * Interface indiquant si l'element se deplace sur le plateau
 */

public interface Moveable extends Placeable {

    /**
     * Deplace la piece sur la plateau aux coordonees donnees
     *
     * @param t represente la tuile ou placer la piece
     */
    void move(Tile t);
}
