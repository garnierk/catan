package up.catansColons.board.piece;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

/**
 * Interface indiquant si l'element est placeable sur le plateau
 */
public interface Placeable {

    Tile getTile();

    Edge getEdge();

    Vertex getVertex();

    /**
     * Indique une arrete libre relie a l'element
     *
     * @return l'arrete en question, null si elle n'existe pas
     */
    Edge getFreeEdge();

    /**
     * Indique un coin libre relie a l'element
     *
     * @return le coin en question, null s'il n'existe pas
     */
    Vertex getFreeVertex();

    /**
     * Indique si l'element est place
     *
     * @return true si c'est le cas, false sinon
     */
    boolean isPlaced();

    /**
     * Place un element Placeable sur le plateau selon les coordonees
     *
     * @param t represente la tuile ou placer l'element
     * @return true si le placement a eu lieu, false sinon
     */
    default boolean place(Tile t) {
        return place(t, null);
    }

    /**
     * Place un element Placeable sur le plateau selon les coordonees
     *
     * @param t represente la tuile ou placer l'element
     * @param c represente le coin ou placer l'element
     * @return true si le placement a eu lieu, false sinon
     */
    default boolean place(Tile t, Vertex c) {
        return place(t, c, null);
    }

    /**
     * Place un element Placeable sur le plateau selon les coordonees
     *
     * @param t represente la tuile ou placer l'element
     * @param c represente le coin ou placer l'element
     * @param e represente l'arrete ou placer l'element
     * @return true si le placement a eu lieu, false sinon
     */
    boolean place(Tile t, Vertex c, Edge e);

}
