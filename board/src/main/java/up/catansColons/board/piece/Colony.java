package up.catansColons.board.piece;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Port;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represente la piece Colonie du jeu
 *
 * @see Piece
 * @see Placeable
 */
public class Colony extends Piece {
    /**
     * Represente l'id du joeur possedant la colonie
     */
    private final int playerId;
    /**
     * represenet le caractere affichable de la Colonie
     */
    char playerChar;
    /**
     * Represente ele coin sur lequel la colonie est placee
     */
    private Vertex vertex;

    public Colony(Tile tuile, Vertex vertex, int playerId) {
        super(tuile);
        this.vertex = vertex;
        this.playerId = playerId;
        playerChar = setPlayerChar(playerId);
    }

    public Colony(int playerId) {
        this(null, null, playerId);
    }

    /**
     * Ameliore une colonie en ville
     *
     * @return la Ville creee
     */
    public City amelioration() {
        if (getTile() == null || vertex == null) return null;
        City ret = new City(getTile(), vertex, playerId);
        this.vertex.setColony(ret);
        this.vertex = null;
        setTile(null);
        return ret;
    }

    /**
     * Indique toutes les tuiles adjacente a la colonie
     *
     * @return une collection contenant ces tuiles
     */
    public Collection<Tile> getNextTiles() {
        return (vertex != null) ? this.vertex.getNextTiles() : null;
    }

    @Override
    public boolean place(Tile t, Vertex v, Edge e) {
        if (t == null || v == null) return false;//Place une colonie (utile pour le debut de partie)
        if (!t.isVertexPresent(v) || !v.isFreeToPlace()) return false;
        setTile(t);
        vertex = v;
        v.setColony(this);
        return true;
    }


    /**
     * Retire une colonie d'un coin
     */
    public void remove() {
        setTile(null);
        vertex.setColony(null);
        vertex = null;
    }

    /**
     * Fonde une nouvelle colonie en cours de partie aux coordonees donnees
     *
     * @param t represente la tuile ou fonder la colonie
     * @param v represente la tuile ou fonder la colonie
     * @param e represente la tuile ou fonder la colonie
     * @return true si la fondation a reussie, false sinon
     */
    public boolean foundNewColony(Tile t, Vertex v, Edge e) {
        return foundNewColony(t, v);
    }

    /**
     * Fonde une nouvelle colonie en cours de partie aux coordonees donnees
     *
     * @param t represente la tuile ou fonder la colonie
     * @param v represente la tuile ou fonder la colonie
     * @return true si la fondation a reussie, false sinon
     */
    public boolean foundNewColony(Tile t, Vertex v) { //Fonde une nouvelle colonie en cours de partie
        if (t == null || v == null) return false;
        if (!v.nextToFriendlyRoad(this.playerId)) return false;
        return place(t, v);
    }

    public boolean isPlaced() {
        return vertex != null;
    }

    public int getScore() {
        return (isPlaced()) ? 1 : 0;
    }

    @Override
    public String getName() {
        return "Colonie";
    }

    public int getPlayerId() {
        return playerId;
    }

    @Override
    public Edge getEdge() {
        return null;
    }

    public Vertex getVertex() {
        return vertex;
    }

    @Override
    public Edge getFreeEdge() {
        Vertex v = getFreeVertex();
        if (v != null) {
            v.getFreeEdge();
        }
        return null;
    }

    public char getPlayerChar() {
        return playerChar;
    }

    @Override
    public Vertex getFreeVertex() {
        Tile t = getTile();
        return (t != null) ? t.getFreeVertex() : null;
    }

    /**
     * Retourne la longueur de la plus longue route
     *
     * @param playerId indique l'id du joueur possedant la colonie
     * @return le nombre de route a la chaine
     */
    public int getLongestRoadSize(int playerId) {
        if (!isPlaced()) return 0;
        return this.vertex.getLongestRoadSize(playerId, new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Retourne la longueur de la plus longue route
     *
     * @return le nombre de route a la chaine
     */
    public int getLongestRoadSize() {
        return getLongestRoadSize(this.playerId);
    }

    /**
     * Indique si le joueur donne en argument possede la colonie
     *
     * @param playerId represente l'id du joueur
     * @return true c'et le cas, false sinon
     */
    public boolean isOwner(int playerId) {
        return this.playerId == playerId;
    }

    /**
     * Cherche tous les ports adjacents au coin
     *
     * @return une collection contenant ces ports
     */
    public Collection<? extends Port> getNextPort() {
        if (vertex == null) return null;
        return this.vertex.getNextPorts();
    }
}