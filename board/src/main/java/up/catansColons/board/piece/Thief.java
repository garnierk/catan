package up.catansColons.board.piece;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

/**
 * Represente la piece Voleur du jeu
 *
 * @see Piece
 * @see Moveable
 * @see Placeable
 */
public class Thief extends Piece
        implements Placeable, Moveable {

    public Thief(Tile tuile) {
        super(tuile);
    }


    @Override
    public String getName() {
        return "Voleur";
    }

    @Override
    public Tile getTile() {
        return super.getTile();
    }

    @Override
    public Edge getEdge() {
        return null;
    }

    @Override
    public Vertex getVertex() {
        return null;
    }

    @Override
    public Edge getFreeEdge() {
        return null;
    }

    @Override
    public Vertex getFreeVertex() {
        return null;
    }

    @Override
    public boolean isPlaced() {
        return true;
    }

    @Override
    public void move(Tile t) {
        getTile().setThief(null);
        this.place(t);

    }

    @Override
    public boolean place(Tile t, Vertex c, Edge e) {
        t.setThief(this);
        this.setTile(t);
        return true;
    }

    @Override
    public int getLongestRoadSize() {
        return 0;
    }

}
