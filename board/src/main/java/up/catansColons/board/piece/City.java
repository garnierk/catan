package up.catansColons.board.piece;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

/**
 * Represente la piece Ville du jeu
 *
 * @see Colony
 * @see Piece
 * @see Placeable
 */
public class City extends Colony
        implements Placeable {

    public City(Tile tile, Vertex vertex, int playerId) {
        super(tile, vertex, playerId);
    }

    public City(int playerId) {
        this(null, null, playerId);
    }

    @Override
    public int getScore() {
        return (isPlaced()) ? 2 : 0;
    }

    @Override
    public String getName() {
        return "Ville";
    }

    @Override
    public Edge getEdge() {
        return null;
    }

    @Override
    public Edge getFreeEdge() {
        Vertex v = this.getVertex();
        return (v != null) ? getVertex().getFreeEdge() : null;
    }

    @Override
    public City amelioration() {
        return null;
    }

    @Override
    public Vertex getFreeVertex() {
        return null;
    }


    @Override
    public boolean place(Tile t, Vertex v, Edge e) {
        return false;
    }

    /**
     * Fonde une nouvelle colonie en cours de partie aux coordonees donnees
     *
     * @param t represente la tuile ou fonder la colonie
     * @param v represente la tuile ou fonder la colonie
     * @param e represente la tuile ou fonder la colonie
     * @return true si la fondation a reussie, false sinon
     */
    @Override
    public boolean foundNewColony(Tile t, Vertex v, Edge e) {
        return false;
    }

    /**
     * Fonde une nouvelle colonie en cours de partie aux coordonees donnees
     *
     * @param t represente la tuile ou fonder la colonie
     * @param v represente la tuile ou fonder la colonie
     * @return true si la fondation a reussie, false sinon
     */
    @Override
    public boolean foundNewColony(Tile t, Vertex v) {
        return false;
    }
}
