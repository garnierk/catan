package up.catansColons.board.piece;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

public abstract class Piece
        implements Placeable {
    /**
     * Indique le caractere relie au joueur 1 sur la plateau
     */
    public static final char PLAYER_1 = 'a';
    /**
     * Indique le caractere relie au joueur 2 sur la plateau
     */
    public static final char PLAYER_2 = '2';
    /**
     * Indique le caractere relie au joueur 3 sur la plateau
     */
    public static final char PLAYER_3 = '#';
    /**
     * Indique le caractere relie au joueur 4 sur la plateau
     */
    public static final char PLAYER_4 = '$';
    /**
     * Represente la tuile sur laquelle la piece est posee
     */
    private Tile tile;


    public Piece(Tile tile) {
        this.tile = tile;
    }

    /**
     * Choisit le caractere de la piece en fonction de la valeur de l'Id du joueur
     *
     * @param playerId represente l'Id du joueur
     * @return le caractere selectione, e pour une erreur
     */
    protected char setPlayerChar(int playerId) {
        switch (playerId) {
            case 0:
                return Piece.PLAYER_1;
            case 1:
                return Piece.PLAYER_2;
            case 2:
                return Piece.PLAYER_3;
            case 3:
                return Piece.PLAYER_4;
            default:
                return 'e';
        }
    }

    @Override
    public String toString() {
        return getName();
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public String getName() {
        return "";
    }

    /**
     * Donne le score de la piece
     *
     * @return le score en question
     */
    public int getScore() {
        return 0;
    }

    @Override
    public boolean place(Tile t, Vertex v, Edge e) {
        return false;
    }

    /**
     * Donne la longueur de la route la plus longue depuis la piece
     *
     * @return la valeur de cette route
     */
    public abstract int getLongestRoadSize();
}
