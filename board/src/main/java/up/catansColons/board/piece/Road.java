package up.catansColons.board.piece;

import up.catansColons.board.composant.Edge;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.composant.Vertex;

import java.util.ArrayList;

/**
 * Represente la piece Route du jeu
 *
 * @see Piece
 * @see Placeable
 */
public class Road extends Piece
        implements Placeable {

    /**
     * Represente le caractere affichable de la route
     */
    private final char playerChar;
    /**
     * Represente l'arrete sur laquelle est pose la route
     */
    private Edge edge;
    /**
     * Represente l'id du joueur possedant la route
     */
    private int playerId;

    public Road(Tile tile, Edge edge, int playerId) {
        super(tile);
        this.edge = edge;
        this.playerId = playerId;
        playerChar = setPlayerChar(playerId);
    }


    public Road(int playerId) {
        this(null, null, playerId);
    }

    @Override
    public boolean place(Tile t, Vertex v, Edge e) {
        if (t == null || v == null || e == null) return false;
        if (isFreeToPlace(t, v, e)) return false;
        e.setRoad(this);
        this.edge = e;
        setTile(t);
        return true;
    }


    /**
     * Indique si l'arrete est libre pour poser une route et proche d'une construction alliee
     *
     * @param t represente la tuile de l'arrete
     * @param v represente le coin relie a l'arrete
     * @param e represente l'arrete ou poser la route
     * @return true si c'est le cas, false sinon
     */
    private boolean isFreeToPlace(Tile t, Vertex v, Edge e) {
        return (!e.isFree() || !v.isPresent(e) || !t.isVertexPresent(v) || !e.nextToFriendly(this.playerId));
    }

    @Override
    public Tile getTile() {
        Vertex v = getFreeVertex();
        return (v == null) ? super.getTile() : v.getTile();
    }

    public Edge getEdge() {
        return edge;
    }

    public void setEdge(Edge edge) {
        this.edge = edge;
    }

    @Override
    public Vertex getVertex() {
        return edge.getOccupiedVertex();
    }

    @Override
    public Edge getFreeEdge() {
        Vertex v = edge.getNotFullVertex();
        if (v != null) {
            return v.getFreeEdge();
        }
        return null;
    }

    @Override
    public Vertex getFreeVertex() {
        return edge.getNotFullVertex();
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public boolean isPlaced() {
        return this.edge != null;
    }

    /**
     * Indique si le joueur donne en argument possede la colonie
     *
     * @param playerId represente l'id du joueur
     * @return true c'et le cas, false sinon
     */
    public boolean isOwner(int playerId) {
        return this.playerId == playerId;
    }

    public char getPlayerChar() {
        return playerChar;
    }


    /**
     * Retourne la longueur de la plus longue route
     *
     * @return le nombre de route a la chaine
     */
    public int getLongestRoadSize() {
        if (!isPlaced()) return 0;
        int fromCurrent = this.edge.getCurrent().getLongestRoadSize(this.playerId, new ArrayList<>(), new ArrayList<>());
        int fromNext = this.edge.getNext().getLongestRoadSize(this.playerId, new ArrayList<>(), new ArrayList<>());
        return Math.max(fromCurrent, fromNext);
    }
}
