package up.catansColons.board;

/**
 * Bibliotheque de generation du plateau et des pieces necessaire au jeu
 * <p>
 * Architecture du plateau
 * Board -> Tile -> Vertex <-> Edge
 * <p>
 * Ou les fleches indique la visibilite des elements
 * <p>
 * Disposition des pieces sur le plateau
 * <p>
 * Thief -> Tile
 * Colonie -> vertex
 * City -> Vertex
 * Road -> Edge
 */