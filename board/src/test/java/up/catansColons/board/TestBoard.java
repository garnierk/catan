package up.catansColons.board;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import up.catansColons.board.composant.Vertex;
import up.catansColons.board.composant.Tile;
import up.catansColons.board.piece.Colony;
import up.catansColons.board.piece.Road;

import java.util.Arrays;
import java.util.LinkedList;

public class TestBoard {
    private static Board board = null;
    private static Tile tC1;
    private static Tile tR3;
    private Colony c1;
    private Colony c2;
    private Road r1;
    private Road r2;
    private Road r3;

    @BeforeClass
    public static void setup() {
        board = new Board(4, 4);
        tC1 = board.getTiles()[0][3];
        tR3 = board.getTiles()[3][2];
    }

    @AfterClass
    public static void clean() {
        board = null;
        tC1 = null;
        tR3 = null;
    }

    @Test
    public void checkConstruction() {
        System.out.println(board);
        board.print();
        /*for (Tile[] tab : board.getTiles()) {
            for (Tile t : tab) {
                System.out.print("NextTiles : (");
                System.out.print(t.getVertex(0, 0).getNextTiles().size());
                System.out.print(t.getVertex(0, 1).getNextTiles().size());
                System.out.print(t.getVertex(1, 0).getNextTiles().size());
                System.out.print(t.getVertex(1, 1).getNextTiles().size());
                System.out.println(")");
                System.out.print("Edges : (");
                System.out.print(t.getVertex(0, 0).getEdges().size());
                System.out.print(t.getVertex(0, 1).getEdges().size());
                System.out.print(t.getVertex(1, 0).getEdges().size());
                System.out.print(t.getVertex(1, 1).getEdges().size());
                System.out.println(")");
            }
        }*/
    }

    public void placeColony() {
        c1 = new Colony(0);
        c2 = new Colony(0);
        boolean c1Placed = c1.place(tC1, tC1.getVertex(0, 0));
        boolean c2Placed = c2.place(tC1, tC1.getVertex(0, 0));
        Assert.assertTrue(c1Placed);
        Assert.assertFalse(c2Placed);
    }

    public void placeRoad() {
        placeColony();
        r1 = new Road(0);
        r2 = new Road(0);
        r3 = new Road(0);
        System.out.println(Arrays.deepToString(tC1.getVertices()));
        Assert.assertTrue(r1.place(tC1, c1.getVertex(), c1.getVertex().getEdge(tC1.getVertex(0, 1))));
        Assert.assertTrue(r2.place(tC1, tC1.getVertex(0, 1), tC1.getVertex(0, 1).getEdge(tC1.getVertex(1, 1))));
        Assert.assertTrue(c2.foundNewColony(tC1, tC1.getVertex(1, 1)));
        Assert.assertFalse(r1.place(tR3, tR3.getVertex(1, 1), tR3.getVertex(1, 1).getEdges().get(0)));
        Assert.assertTrue(r3.place(tC1, c1.getVertex(), c1.getVertex().getEdge(tC1.getVertex(1, 0))));
    }

    @Test
    public void test() {
        placeRoad();
        board.getThief().move(tR3);
        Assert.assertSame(board.getThief().getTile(), tR3);
        Assert.assertSame(c2.getTile(), c1.getTile());
        Assert.assertEquals(2, c1.getLongestRoadSize(0));
        Assert.assertEquals(0, c1.getLongestRoadSize(1));
    }
}
