plugins {
   `java-library`
}

dependencies {
    implementation("junit:junit:4.13.2")
    implementation(project(":cards"))
}