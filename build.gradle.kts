plugins {
    java
}

version = "0.0.1"
group = "up"

allprojects {
    repositories {
        mavenCentral()
    }
    tasks.withType<JavaCompile> {
        options.encoding = "UTF-8"
    }
    plugins.apply("java")

    java.sourceCompatibility = JavaVersion.VERSION_1_10

}

